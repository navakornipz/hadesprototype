﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class BubbleController : MonoBehaviour
{
    private Image image;

    private float amplitude;
    private float angleSpeed;
    private Vector2 wind;
    private float limitY;
    private float limitLeft;
    private float limitRight;
    private float currentAngle;
    private Vector2 tempPos;
    private Vector2 originalScale;

    [HideInInspector] public bool IsAlive;

    private void Awake()
    {
        image = GetComponent<Image>();

        originalScale = image.rectTransform.sizeDelta;
    }

    private void Update()
    {
        if (!IsAlive)
        {
            return;
        }

        tempPos += wind * Time.deltaTime;
        currentAngle += angleSpeed * Time.deltaTime;
        tempPos.x += amplitude * Mathf.Sin(currentAngle);
        image.rectTransform.anchoredPosition = tempPos;

        IsAlive = tempPos.y <= limitY && tempPos.x >= limitLeft && tempPos.x <= limitRight;
    }

    public void Init(float minScale, float maxScale)
    {
        var scale = Random.Range(minScale, maxScale);
        image.rectTransform.sizeDelta = originalScale * scale;

        var tempY = -image.rectTransform.sizeDelta.y * 0.5f - 10f;
        var tempX = Random.Range(0, Screen.width);
        image.rectTransform.anchoredPosition = new Vector2(tempX, tempY);
        tempPos = image.rectTransform.anchoredPosition;

        angleSpeed = Random.Range(0.1f, 2f);
        amplitude = Random.Range(0.1f, 5f);

        wind = new Vector2();
        wind.x = Random.Range(-50f, 50f);
        wind.y = Random.Range(50f, 200f);

        limitY = Screen.height + image.rectTransform.sizeDelta.y * 0.5f;
        limitLeft = -image.rectTransform.sizeDelta.x;
        limitRight = Screen.width - limitLeft;

        IsAlive = true;
    }
}
