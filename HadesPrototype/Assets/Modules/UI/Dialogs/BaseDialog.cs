﻿using System;
using UnityEngine;
using System.Collections;

public abstract class BaseDialog : MonoBehaviour
{
    #region Define

	private const string prefabsPath = "UI/Dialogs/{0}";

    #endregion

    #region SerializeField

    [SerializeField] private GameObject transparentObject;

    #endregion

    #region Members

    //This action call after Dialog is closed.
    protected Action onComplete;

    protected DialogState dialogState;

    private Animator animator;
	private int dialogStateHash;

    [HideInInspector] public bool IsCompleted;

    #endregion

    #region Properties

    public bool IsClosing
    {
        get
        {
            return dialogState >= DialogState.FadeOut;
        }
    }

    #endregion

    #region Initialize

	public static T Create<T>(Transform parent) where T : BaseDialog
    {
		var type = typeof(T);
        var prefabPath = string.Format(prefabsPath, type.Name);
		var prefab = Resources.Load<GameObject>(prefabPath);

		if (prefab == null)
		{
			Debug.LogErrorFormat("prefab: {0} not found.", prefabPath);
			return default(T);
		}
		else
		{
			var gameObject = Instantiate(prefab, parent, false);
			return gameObject.GetComponent<T>();
		}
	}

    protected virtual void OnAwake()
    {
        animator = gameObject.GetComponent<Animator>();

        if (animator != null)
        {
            dialogStateHash = Animator.StringToHash(typeof(DialogState).Name);
        }
    }

    #endregion

    #region Terminate

    protected virtual void OnClose()
    {
        if (onComplete != null)
        {
            onComplete.Invoke();
        }
        gameObject.SetActive(false);
    }

    #endregion

    #region Unity Callback

    private void Awake()
    {
        dialogState = DialogState.Initialize;
        OnAwake();
    }

    private void Update()
    {
        switch (dialogState)
        {
            case DialogState.FadeIn:
            {
                FadeInProcess();
                break;
            }
            case DialogState.Idle:
            {
                IdleProcess();
                break;
            }
            case DialogState.FadeOut:
            {
                FadeOutProcess();
                break;
            }
        }
    }

    private void OnEnable()
    {
        ChangeState(DialogState.Open);
    }

    #endregion

    #region Process

    protected virtual void FadeInProcess()
    {
            
    }

    protected virtual void IdleProcess()
    {
            
    }

    protected virtual void FadeOutProcess()
    {
            
    }

    #endregion

    #region Command

	public void ChangeState(DialogState dialogState)
	{
		if (this.dialogState == dialogState)
		{
		    return;
		}
        //TODO: leave old event prop
        //TODO: set new event prop
        this.dialogState = dialogState;

		if (gameObject.activeInHierarchy)
		{
			changeAnimation((int)dialogState);
		}

		switch (dialogState)
        {
            case DialogState.Open:
            {
                OnOpen();
                break;
            }
            case DialogState.FadeIn:
            {
                OnFadeIn();
                break;
            }
            case DialogState.Idle:
            {
                OnIdle();
                break;
            }
            case DialogState.FadeOut:
            {
                OnFadeOut();
                break;
            }
            case DialogState.Close:
            {
                OnClose();
                break;
            }
        }
	}

    public void ActiveTransparentBackground(bool active)
    {
        if (transparentObject == null)
        {
            return;
        }

        transparentObject.SetActive(active);
    }

    public virtual void Close()
    {
        ChangeState(DialogState.FadeOut);
    }

    protected virtual void OnOpen()
    {
        if (gameObject.activeInHierarchy == false)
        {
            gameObject.SetActive(true);
        }

        ChangeState(DialogState.FadeIn);
    }

    protected virtual void OnIdle()
    {
            
    }

    protected virtual void OnFadeIn()
    {
        if (animator != null)
        {
            return;
        }

        ChangeState(DialogState.Idle);
    }

    protected virtual void OnFadeOut()
    {
        if (animator != null)
        {
            return;
        }

        ChangeState(DialogState.Close);
    }

	private void changeAnimation(int id)
	{
		if (animator == null)
		{
			return;
		}
		StartCoroutine(changeAnimationProcess(id));
	}

	private IEnumerator changeAnimationProcess(int id)
	{
		while (!animator.isInitialized)
		{
			yield return null;
		}

		animator.SetInteger(dialogStateHash, id);
	}

    #endregion

    #region Property

	public virtual void Show(bool show = true)
	{
        ChangeState(show ? DialogState.Open : DialogState.FadeOut);
	}

    public void SetOnComplete(Action onComplete)
    {
        this.onComplete = onComplete;
    }

    #endregion
}
