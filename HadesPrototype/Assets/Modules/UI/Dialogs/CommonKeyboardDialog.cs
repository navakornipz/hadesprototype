﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommonKeyboardDialog : BaseKeyboardDialog
{
    #region Define

    private const string filePath = @"Data/CommonKeyboardData";
    private const int partDataCount = 5;
    private const int totalLanguage = 2;
    private const int english = 0;
    private const int thai = 1;

    private class OriginalData
    {
        public Vector2 Position;
        public Vector2 Size;
        public int FontSize;
    }

    #endregion

    #region Members

    private Dictionary<string, OriginalData> originalList = new Dictionary<string, OriginalData>();

    #endregion

    #region Command

    protected override void SetButtonData(Button button)
    {
        var commandName = GetCommand(button.name);
        var text = button.GetComponentInChildren<Text>();

        var originalData = new OriginalData();
        if (originalList.TryGetValue(text.text, out originalData))
        {
            text.rectTransform.anchoredPosition = originalData.Position;
            text.rectTransform.sizeDelta = originalData.Size;
            text.fontSize = originalData.FontSize;
            text.resizeTextMaxSize = originalData.FontSize;
        }

        switch (commandName)
        {
            case "ุ":
            case "ู":
                AddOriginalData(text, commandName);
                text.rectTransform.anchoredPosition = new Vector2(10, 15);
                text.rectTransform.sizeDelta = new Vector2(60, 60);
                text.fontSize = 48;
                text.resizeTextMaxSize = 48;
                break;
            case "ึ":
            case "ั":
            case "ี":
            case "็":
            case "ิ":
            case "ื":
                AddOriginalData(text, commandName);
                text.rectTransform.anchoredPosition = new Vector2(14, -10);
                text.rectTransform.sizeDelta = new Vector2(60, 60);
                text.fontSize = 48;
                text.resizeTextMaxSize = 48;
                break;
            case "่":
            case "้":
            case "๊":
            case "๋":
            case "์":
                AddOriginalData(text, commandName);
                text.rectTransform.anchoredPosition = new Vector2(6, -16);
                text.rectTransform.sizeDelta = new Vector2(60, 60);
                text.fontSize = 48;
                text.resizeTextMaxSize = 48;
                break;
        }

        text.text = GetCommand(button.name);
    }

    public void AddOriginalData(Text text, string commandName)
    {
        if (originalList.ContainsKey(commandName))
        {
            return;
        }

        var originalData = new OriginalData();
        originalData.Position = text.rectTransform.anchoredPosition;
        originalData.Size = text.rectTransform.sizeDelta;
        originalData.FontSize = text.fontSize;
        originalList.Add(commandName, originalData);
    }

    #endregion

    #region Event

    protected override void OnClick(string buttonName)
    {
        var commandName = GetCommand(buttonName);
        if (string.IsNullOrEmpty(commandName))
        {
            return;
        }

        switch (commandName)
        {
            case "Shift":
                OnShiftHit();
                return;
            case "TH":
            case "EN":
                OnSwapLanguage();
                return;
            case "Enter":
                return;
        }

        if (InputManager.Instance == null)
        {
            Debug.LogError("Input Manager is not initialize!");
            return;
        }
        InputManager.Instance.OnVirtualKeyboard(commandName);
    }

    #endregion

    #region Get/Set

    protected override string GetFileData()
    {
        return filePath;
    }

    protected override int GetPartDataCount()
    {
        return partDataCount;
    }

    protected override int GetTotalLanguage()
    {
        return totalLanguage;
    }

    #endregion
}
