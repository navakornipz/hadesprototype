﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MessageDialog : BaseDialog
{
    public enum ButtonPosition
    {
        Middle = 0,
        Left,
        Right
    }

    [SerializeField] private Text titleText;
    [SerializeField] private Text descriptionText;
    [SerializeField] private Button positiveButton;
    [SerializeField] private Text positiveButtonText;
    [SerializeField] private Button midButton;
    [SerializeField] private Text midButtonText;
    [SerializeField] private Button negativeButton;
    [SerializeField] private Text negativeButtonText;

    protected override void OnAwake()
    {
        base.OnAwake();

        positiveButton.gameObject.SetActive(false);
        midButton.gameObject.SetActive(false);
        positiveButton.onClick.AddListener(Close);
        midButton.onClick.AddListener(Close);
        negativeButton.onClick.AddListener(Close);
    }

    public void SetMessage(string title, string description)
    {
        titleText.text = title;
        descriptionText.text = description;
    }

    public void ResetAllButtons()
    {
        positiveButton.gameObject.SetActive(false);
        positiveButtonText.text = string.Empty;
        positiveButton.onClick.RemoveAllListeners();

        midButton.gameObject.SetActive(false);
        midButtonText.text = string.Empty;
        midButton.onClick.RemoveAllListeners();

        negativeButton.gameObject.SetActive(false);
        negativeButtonText.text = string.Empty;
        negativeButton.onClick.RemoveAllListeners();

        onComplete = null;
    }

    public void SetGreenButton(string name, UnityAction OnClick)
    {
        positiveButton.gameObject.SetActive(true);
        positiveButtonText.text = name;
        positiveButton.onClick.AddListener(OnClick);
    }

    public void SetRedButton(string name, UnityAction OnClick)
    {
        negativeButton.gameObject.SetActive(true);
        negativeButtonText.text = name;
        negativeButton.onClick.AddListener(OnClick);
    }

    public void SetYellowButton(string name, UnityAction OnClick)
    {
        midButton.gameObject.SetActive(true);
        midButtonText.text = name;
        midButton.onClick.AddListener(OnClick);
    }

    protected override void OnClose()
    {
        base.OnClose();
        Destroy(gameObject);
    }
}