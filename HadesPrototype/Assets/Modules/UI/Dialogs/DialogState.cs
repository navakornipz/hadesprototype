﻿public enum DialogState
{
    Initialize = -1,
	Open = 0,
	FadeIn,
	Idle,
	FadeOut,
	Close
}
