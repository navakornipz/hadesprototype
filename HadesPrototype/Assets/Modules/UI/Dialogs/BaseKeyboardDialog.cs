﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BaseKeyboardDialog : BaseDialog
{
    #region Members

    protected Dictionary<string, string[]> data;
    protected Button[] buttons;

    protected int languageIndex;
    protected bool isShifting;

    #endregion

    #region Command

    protected virtual void InitButton(Button button)
    {
        if (button.name == "unused")
        {
            Destroy(button.gameObject);
            return;
        }
        SetButtonData(button);
        var buttonName = button.name;
        button.onClick.AddListener(() => { OnClick(buttonName); });
    }

    protected virtual void InitData()
    {
        data = new Dictionary<string, string[]>();

        var file = GetFileData();
        var textAsset = Resources.Load<TextAsset>(file);

        if (textAsset == null)
        {
            Debug.LogErrorFormat("file not found: {0}", file);
            return;
        }

        var lines = textAsset.text.Split('\r', '\n');
        for (var i = 0; i < lines.Length; i++)
        {
            var line = lines[i];

            if (line.Length < 1 ||
                line[0] == '#')
            {
                continue;
            }

            var parts = line.Split(',');
            var partDataCount = GetPartDataCount();
            if (parts.Length < partDataCount)
            {
                continue;
            }

            var value = new string[partDataCount - 1];
            for (var j = 0; j < value.Length; j++)
            {
                value[j] = parts[j + 1];
            }
            data.Add(parts[0], value);
        }
    }

    protected virtual void SetButtonData(Button button)
    {
        button.GetComponentInChildren<Text>().text = GetCommand(button.name);
    }

    #endregion

    #region Event

    protected override void OnAwake()
    {
        base.OnAwake();

        InitData();

        languageIndex = 0;
        isShifting = false;

        buttons = GetComponentsInChildren<Button>();
        for (var i = 0; i < buttons.Length; i++)
        {
            InitButton(buttons[i]);
        }
    }

    protected virtual void OnClick(string buttonName)
    {
        var commandName = GetCommand(buttonName);
        if (InputManager.Instance == null)
        {
            Debug.LogError("Input Manager is not initialize!");
            return;
        }
        InputManager.Instance.OnVirtualKeyboard(commandName);
    }

    protected virtual void OnShiftHit()
    {
        isShifting = !isShifting;
        for (var i = 0; i < buttons.Length; i++)
        {
            SetButtonData(buttons[i]);
        }
    }

    protected virtual void OnSwapLanguage()
    {
        isShifting = false;
        languageIndex++;
        if (languageIndex >= GetTotalLanguage())
        {
            languageIndex = 0;
        }
        for (var i = 0; i < buttons.Length; i++)
        {
            SetButtonData(buttons[i]);
        }
    }

    #endregion

    #region Get/Set

    protected abstract string GetFileData();
    protected abstract int GetPartDataCount();
    protected abstract int GetTotalLanguage();

    protected string GetCommand(string name)
    {
        var value = new string[GetPartDataCount() - 1];
        if (!data.TryGetValue(name, out value))
        {
            Debug.LogErrorFormat("button {0} not found.");
            return string.Empty;
        }
        var index = languageIndex * 2;
        if (isShifting)
        {
            index++;
        }
        return value[index];
    }

    #endregion
}
