﻿using System.Collections.Generic;
using UnityEngine;

public class CircleLayout : MonoBehaviour
{
    private RectTransform rectTransform;
    private RectTransform[] children;
    
    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        var tempList = new List<RectTransform>(transform.GetComponentsInChildren<RectTransform>(true));
        tempList.Remove(rectTransform);
        children = tempList.ToArray();
    }

    public void Init(int count, Vector2 start, float radius, float space)
    {
        var leftSpace = -0.5f * space * (count - 1);

        for (var i = 0; i < count; i++)
        {
            var degree = leftSpace + space * i;
            var v = Rotate(start, degree);
            children[i].gameObject.SetActive(true);
            children[i].anchoredPosition = v * radius;
        }
    }

    private Vector2 Rotate(Vector2 v, float degree)
    {
        var rad = degree * Mathf.Deg2Rad;
        var s = Mathf.Sin(rad);
        var c = Mathf.Cos(rad);
        return new Vector2(
         v.x * c - v.y * s,
         v.y * c + v.x * s
        );
    }
}
