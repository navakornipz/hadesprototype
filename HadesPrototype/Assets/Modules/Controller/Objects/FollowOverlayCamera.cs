﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class FollowOverlayCamera : MonoBehaviour
{
    private new Camera camera;
    private Transform target;
    private Vector3 offsetPosition;
    private Vector3 offsetFocus;

    public RenderTexture Texture { get; private set; }

    private void Start()
    {
        camera = GetComponent<Camera>();

        Texture = new RenderTexture(Screen.width, Screen.height, 0);

        camera.targetTexture = Texture;
    }

    public void Init(Transform target, Vector3 offsetPosition, Vector3 offsetFocus)
    {
        this.target = target;
        this.offsetPosition = offsetPosition;
        this.offsetFocus = offsetFocus;
    }
}
