﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class AreaTrigger : MonoBehaviour
{
    [SerializeField] private TriggerType type;
    [SerializeField] private TriggerBehaviour behaviour;
    [SerializeField] private GameObject target;
    [SerializeField] private int interestLayer = -1;
    [SerializeField] private string interestTag;
    [SerializeField] private bool isOnTarget;
    [SerializeField] private bool destroyWhenOutOfArea;
    [SerializeField] private string keyName;
    [SerializeField] private int animationState = -1;

    private void PullTrigger(Collider other)
    {
        if (other.gameObject.layer != interestLayer &&
            !other.CompareTag(interestTag))
        {
            return;
        }

        var interestObject = isOnTarget ? target : other.gameObject;

        if (interestObject == null)
        {
            Debug.LogError("area trigger not found interest object.");
            Destroy(this);
            return;
        }

        switch (type)
        {
            case TriggerType.ActiveObject:
                interestObject.SetActive(true);
                break;
            case TriggerType.InActiveObject:
                interestObject.SetActive(false);
                break;
            case TriggerType.PlayAnimation:
                {
                    if (string.IsNullOrEmpty(keyName))
                    {
                        Debug.LogError("animation state name should not be null.");
                        Destroy(this);
                        return;
                    }
                    var animators = interestObject.GetComponentsInChildren<Animator>(true);
                    if (animators == null || animators.Length < 1)
                    {
                        Debug.LogError("area trigger not found animator.");
                        Destroy(this);
                        return;
                    }

                    for (var i = 0; i < animators.Length; i++)
                    {
                        var animator = animators[i];
                        if (!animator.gameObject.activeInHierarchy)
                        {
                            animator.gameObject.SetActive(true);
                        }

                        animator.Play(keyName);
                    }
                }
                break;
            case TriggerType.ChangeAnimationState:
                {
                    if (string.IsNullOrEmpty(keyName))
                    {
                        Debug.LogError("animation state name should not be null.");
                        Destroy(this);
                        return;
                    }
                    if (animationState < 0)
                    {
                        Debug.LogError("animation state should not less than 0");
                        Destroy(this);
                        return;
                    }
                    var animators = interestObject.GetComponentsInChildren<Animator>(true);
                    if (animators == null || animators.Length < 1)
                    {
                        Debug.LogError("area trigger not found animator.");
                        Destroy(this);
                        return;
                    }

                    for (var i = 0; i < animators.Length; i++)
                    {
                        var animator = animators[i];
                        if (!animator.gameObject.activeInHierarchy)
                        {
                            animator.gameObject.SetActive(true);
                        }

                        animator.SetInteger(keyName, animationState);
                    }
                }
                break;
            case TriggerType.CallFunction:
                {
                    if (string.IsNullOrEmpty(keyName))
                    {
                        Debug.LogError("function name should not be null.");
                        Destroy(this);
                        return;
                    }

                    interestObject.SendMessage(keyName);
                }
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (behaviour != TriggerBehaviour.WhenEnter)
        {
            return;
        }

        PullTrigger(other);
    }

    private void OnTriggerStay(Collider other)
    {
        if (behaviour != TriggerBehaviour.WhenInTheArea)
        {
            return;
        }

        PullTrigger(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != interestLayer &&
                !other.CompareTag(interestTag))
        {
            return;
        }

        if (behaviour == TriggerBehaviour.WhenOutOfArea)
        {
            PullTrigger(other);
        }

        if (!destroyWhenOutOfArea)
        {
            return;
        }

        var interestObject = isOnTarget ? target : other.gameObject;
        if (interestObject == null)
        {
            return;
        }

        Destroy(interestObject);
    }
}
