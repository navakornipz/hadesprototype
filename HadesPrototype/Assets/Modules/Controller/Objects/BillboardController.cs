﻿using UnityEngine;

public class BillboardController : MonoBehaviour
{
    private void Update()
    {
        FacingCamera(Camera.main, transform);
    }

    public static void FacingCamera(Camera camera, Transform objectTransform)
    {
        objectTransform.LookAt(objectTransform.position + camera.transform.rotation * Vector3.forward,
            camera.transform.rotation * Vector3.up);
    }
}
