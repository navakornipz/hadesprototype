﻿using System.Collections.Generic;
using UnityEngine;

public class SplitPlane : MonoBehaviour
{
    public const int TotalColumn = 152;
    public const int TotalRow = 84;

    private const float width = 1920;
    private const float height = 1080;
    private const float fraction = 0.0009f;
    private const float startVelocity = 0.5f;
    private const float acceratorRate = 2f;

    public Vector2[][] MarkPositions;
    public int[][] MarkIndices;

    private Mesh mesh;
    private List<Vector3> vertices;
    private List<Vector3> lastVertices;

    private bool[][] isMoving;
    private Vector3[][] accerators;
    private Vector3[][] velocities;

    private void Awake()
    {
        CreateGeometry();
    }

    private void Update()
    {
        var point = Input.mousePosition;
        var i = 0;
        var newVertices = new List<Vector3>(lastVertices);
        var distanceSqr = 360000;
        for (var y = 0; y < TotalRow; y++)
        {
            for (var x = 0; x < TotalColumn; x++)
            {
                var pos = MarkPositions[y][x];
                var delta = new Vector3(pos.x, pos.y) - point;
                var n = delta.normalized;

                var lastPoint = lastVertices[i];
                //var deltaPos = vertices[i] - lastPoint;
                var force = (distanceSqr - delta.sqrMagnitude) * 0.0000001f;

                if (force < 0)
                {
                    force = 0;
                }

                //var f = deltaPos.magnitude < float.Epsilon ? 0 : fraction;

                var accerator = accerators[y][x] + force * n; // + f * deltaPos.normalized;
                var velocity = velocities[y][x] + acceratorRate * accerators[y][x] * Time.deltaTime;
                var distance = (startVelocity * n + velocity) * Time.deltaTime;
                accerators[y][x] = accerator;
                velocities[y][x] = velocity;

                if (delta.sqrMagnitude > distanceSqr * 2)
                {
                    var speed = Time.deltaTime * 2f;
                    //newVertices[i] = Vector3.Slerp(lastVertices[i], vertices[i], speed);
                    newVertices[i] = lastPoint + (vertices[i] - lastPoint) * speed;
                    var newDelta = newVertices[i] - lastPoint;
                    newVertices[i + 1] = lastVertices[i + 1] + newDelta;
                    newVertices[i + 2] = lastVertices[i + 2] + newDelta;
                    newVertices[i + 3] = lastVertices[i + 3] + newDelta;
                    /*newVertices[i + 1] = Vector3.Slerp(lastVertices[i + 1], vertices[i + 1], speed);
                    newVertices[i + 2] = Vector3.Slerp(lastVertices[i + 2], vertices[i + 2], speed);
                    newVertices[i + 3] = Vector3.Slerp(lastVertices[i + 3], vertices[i + 3], speed);*/
                    accerators[y][x] = Vector3.zero;
                    velocities[y][x] = Vector3.zero;
                }
                else
                {
                    newVertices[i] = lastPoint + distance;
                    newVertices[i + 1] = lastVertices[i + 1] + distance;
                    newVertices[i + 2] = lastVertices[i + 2] + distance;
                    newVertices[i + 3] = lastVertices[i + 3] + distance;
                }
                
                i += 4;
            }
        }

        lastVertices = new List<Vector3>(newVertices);
        mesh.SetVertices(newVertices);
    }

    private void CreateGeometry()
    {
        MarkPositions = new Vector2[TotalRow][];
        MarkIndices = new int[TotalRow][];

        var spaceX = Screen.width / TotalColumn;
        var spaceY = Screen.height / TotalRow;
        var edgeColumn = spaceX * 0.5f;
        var edgeRow = spaceY * 0.5f;
        accerators = new Vector3[TotalRow][];
        velocities = new Vector3[TotalRow][];
        for (var i = 0; i < TotalRow; i++)
        {
            var markPos = new Vector2[TotalColumn];
            var indexRow = new int[TotalColumn];
            accerators[i] = new Vector3[TotalColumn];
            velocities[i] = new Vector3[TotalColumn];
            for (var j = 0; j < TotalColumn; j++)
            {
                var newPos = new Vector2(edgeColumn + spaceX * j + spaceX / 2, edgeRow + spaceY * i + spaceY / 2);
                markPos[j] = newPos;
                var x = Screen.width - (int)newPos.x;
                var y = Screen.height - (int)newPos.y;
                indexRow[j] = y * Screen.width + x;
                accerators[i][j] = Vector3.zero;
                velocities[i][j] = Vector3.zero;
            }

            MarkPositions[i] = markPos;
            MarkIndices[i] = indexRow;
        }

        var unitWidth = 1f / TotalColumn;
        var unitHeight = 1f / TotalRow;
        var texUnit = new Vector2(width * unitWidth, height * unitHeight);
        var total = TotalColumn * TotalRow;

        vertices = new List<Vector3>();
        var uvList = new List<Vector2>();
        for (var i = 0; i < total; i++)
        {
            var vertexTL = Vector3.zero;
            vertexTL.x = (i % TotalColumn) * unitWidth;
            vertexTL.y = (i / TotalColumn) * unitHeight;
            uvList.Add(vertexTL);
            vertices.Add(vertexTL - new Vector3(0.5f, 0.5f, 0));

            var vertexTR = Vector3.zero;
            vertexTR.x = vertexTL.x + unitWidth;
            vertexTR.y = vertexTL.y;
            uvList.Add(vertexTR);
            vertices.Add(vertexTR - new Vector3(0.5f, 0.5f, 0));

            var vertexBL = Vector3.zero;
            vertexBL.x = vertexTL.x;
            vertexBL.y = vertexTL.y + unitHeight;
            uvList.Add(vertexBL);
            vertices.Add(vertexBL - new Vector3(0.5f, 0.5f, 0));

            var vertexBR = Vector3.zero;
            vertexBR.x = vertexTR.x;
            vertexBR.y = vertexBL.y;
            uvList.Add(vertexBR);
            vertices.Add(vertexBR - new Vector3(0.5f, 0.5f, 0));
        }

        var triangles = new List<int>();
        for (var i = 0; i < total; i++)
        {
            var a = i * 4;
            var b = a + 1;
            var c = b + 1;
            triangles.Add(a);
            triangles.Add(c);
            triangles.Add(b);

            a = c + 1;
            triangles.Add(c);
            triangles.Add(a);
            triangles.Add(b);
        }

        /*for (var y = 0; y < TotalRow; y++)
        {
            for (var x = 0; x < TotalColumn; x++)
            {
                var row = y * pointPerLineColumn;
                var a = x + row;
                var b = 1 + x + row;
                var c = x + row + pointPerLineRow;
                triangles.Add(a);
                triangles.Add(c);
                triangles.Add(b);

                a = c + 1;
                triangles.Add(a);
                triangles.Add(b);
                triangles.Add(c);
            }
        }*/

        lastVertices = new List<Vector3>(vertices);
        mesh = gameObject.GetComponent<MeshFilter>().mesh;
        mesh.SetVertices(vertices);
        mesh.SetTriangles(triangles, 0);
        mesh.SetUVs(0, uvList);
    }
}
