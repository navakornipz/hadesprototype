﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public abstract class BaseMain : MonoBehaviour
{
    #region SerializeField

    [SerializeField] private Transform canvas;

    #endregion SerializeField

    #region Members

    protected StateManager<AppState> state;

    protected RectTransform backgroundTransform;
    protected RectTransform dialogTransform;
    protected RectTransform foregroundTransform;
    private Image foregroundImage;

    private bool isSceneLoading;

    #endregion Members

    #region Properties

    public AppState CurrentState
    {
        get
        {
            return state.CurrentState;
        }
    }

    public bool IsFading { get; private set; }

    #endregion Properties

    #region Command

    public virtual void ChangeState(AppState newState)
    {
        state.Change(newState);
    }

    public virtual void GoToState(AppState newState)
    {
        if (state.IsInTransition)
        {
            return;
        }

        state.IsInTransition = true;
        StartFadeOut(() =>
        {
            ChangeState(newState);
        });
    }

    public void GoPreviousState()
    {
        GoToState(state.PreviousState);
    }

    public void GoNextState()
    {
        GoToState(state.NextState);
    }

    public virtual void StartFadeIn(Action onCompleted = null)
    {
        if (IsFading)
        {
            Debug.LogErrorFormat("fading right now. Can't fade again!");
            return;
        }

        StartCoroutine(FadeIn(onCompleted));
    }

    public virtual void StartFadeOut(Action onCompleted = null)
    {
        if (IsFading)
        {
            Debug.LogErrorFormat("fading right now. Can't fade again!");
            return;
        }

        StartCoroutine(FadeOut(onCompleted));
    }

    public void StartLoadScene(string sceneName, Action OnLoadSceneCompleted = null)
    {
        if (isSceneLoading)
        {
            Debug.LogErrorFormat("scene is loading right now. Can't load again!");
            return;
        }

        var scene = SceneManager.GetSceneByName(sceneName);

        if (scene == null)
        {
            return;
        }

        if (scene.isLoaded)
        {
            SceneManager.SetActiveScene(scene);
            if (OnLoadSceneCompleted != null)
            {
                OnLoadSceneCompleted.Invoke();
            }
            return;
        }

        StartCoroutine(LoadSceneProcess(sceneName, OnLoadSceneCompleted));
    }

    public void UnloadScene(string sceneName)
    {
        var scene = SceneManager.GetSceneByName(sceneName);

        if (scene == null)
        {
            return;
        }

        if (!scene.isLoaded)
        {
            return;
        }

        SceneManager.UnloadSceneAsync(sceneName);
    }

    #endregion

    #region Event

    protected virtual void StartEnter()
    {
        var backgroundObject = new GameObject("Background", typeof(RectTransform));
        backgroundTransform = backgroundObject.GetComponent<RectTransform>();
        backgroundTransform.SetParent(canvas);
        backgroundTransform.sizeDelta = new Vector2(Screen.width, Screen.height);
        backgroundTransform.anchoredPosition = Vector2.zero;

        var dialogObject = new GameObject("Dialogs", typeof(RectTransform));
        dialogTransform = dialogObject.GetComponent<RectTransform>();
        dialogTransform.SetParent(canvas);
        dialogTransform.sizeDelta = new Vector2(Screen.width, Screen.height);
        dialogTransform.anchoredPosition = Vector2.zero;

        var foregroundObject = new GameObject("Foreground", typeof(RectTransform));
        foregroundTransform = foregroundObject.GetComponent<RectTransform>();
        foregroundTransform.SetParent(canvas);
        foregroundTransform.sizeDelta = new Vector2(Screen.width, Screen.height);
        foregroundTransform.anchoredPosition = Vector2.zero;
        var fCanvas = foregroundObject.AddComponent<Canvas>();
        fCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
        fCanvas.sortingOrder = 100;

        var foregroundImageObject = new GameObject("ForegroundImage", typeof(RectTransform));
        foregroundImage = foregroundImageObject.AddComponent<Image>();
        foregroundImage.transform.SetParent(foregroundTransform);
        foregroundImage.rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);
        foregroundImage.rectTransform.anchoredPosition = Vector2.zero;

        foregroundImage.color = Color.black;
    }

    #endregion Event

    #region IEnumerator

    private IEnumerator LoadSceneProcess(string sceneName, Action OnLoadSceneCompleted)
    {
        isSceneLoading = true;

        var async = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

        yield return new WaitUntil(() => async.isDone);

        var scene = SceneManager.GetSceneByName(sceneName);

        yield return new WaitUntil(() => scene.isLoaded);

        SceneManager.SetActiveScene(scene);

        if (OnLoadSceneCompleted != null)
        {
            OnLoadSceneCompleted.Invoke();
        }

        isSceneLoading = false;
    }

    private IEnumerator FadeIn(Action onCompleted)
    {
        var duration = 1f;
        var fadeSpeed = 1f / duration;
        var color = foregroundImage.color;
        color.a = 1f;

        IsFading = true;
        while (color.a > 0)
        {
            color.a -= Time.deltaTime * fadeSpeed;
            foregroundImage.color = color;
            yield return null;
        }

        color.a = 0;
        foregroundImage.color = color;
        IsFading = false;
        foregroundImage.raycastTarget = false;

        if (onCompleted == null)
        {
            yield break;
        }

        onCompleted.Invoke();
    }

    private IEnumerator FadeOut(Action onCompleted)
    {
        var duration = 0.5f;
        var fadeSpeed = 1f / duration;
        var color = foregroundImage.color;
        color.a = 0f;
        foregroundImage.raycastTarget = true;

        IsFading = true;

        while (color.a < 1f)
        {
            color.a += Time.deltaTime * fadeSpeed;
            foregroundImage.color = color;
            yield return null;
        }

        color.a = 1f;
        foregroundImage.color = color;
        IsFading = false;

        if (onCompleted == null)
        {
            yield break;
        }

        onCompleted.Invoke();
    }

    #endregion IEnumerator
}
