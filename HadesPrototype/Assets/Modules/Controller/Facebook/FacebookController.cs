﻿#if USING_FACEBOOK

using System.Collections;
using UnityEngine;
using Facebook.Unity;

public class FacebookController : MonoBehaviour
{
    private void Awake()
    {
        FB.Init(FBInitCallback);
    }

    private void FBInitCallback()
    {
        if (!FB.IsInitialized)
        {
            return;
        }

        FB.ActivateApp();
    }

    public void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            return;
        }

        if (!FB.IsInitialized)
        {
            return;
        }

        FB.ActivateApp();
    }
}

#endif