﻿#if USING_AR_FOUNDATION

using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class PlaceObjectOnArPlane
{
    [SerializeField] private ARPlaneManager arPlaneManager;

    public void Init(ARPlaneManager arManager)
    {
        arPlaneManager = arManager;
        arPlaneManager.planesChanged += PlaneChanged;
    }

    private void PlaneChanged(ARPlanesChangedEventArgs args)
    {
        var planes = args.added;
        if (planes.Count > 1)
        {
            for (var i = 1; i < planes.Count; i++)
            {
                planes[i].gameObject.SetActive(false);
            }
        }
    }
}

#endif //USING_AR_FOUNDATION