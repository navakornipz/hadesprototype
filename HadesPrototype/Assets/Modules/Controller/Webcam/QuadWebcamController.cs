﻿#if USING_CAMERA

using System;
using UnityEngine;

public class QuadWebcamController : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] Material[] mats;

    private WebCamTexture webCamTexture;
    private MeshRenderer meshRenderer;

    public int CurrentMaterial { get; private set; }

    public bool IsInitialized { get; private set; }

    public bool IsPlaying
    {
        get
        {
            return webCamTexture.isPlaying;
        }
    }

    private void Start()
    {
        if (cam == null)
        {
            cam = Camera.main;
        }

        meshRenderer = GetComponent<MeshRenderer>();

        var json = PlayerPrefs.GetString(PlayerPrefKey.WebcamDeviceFormat, string.Empty);
        var webcamDeviceData = JsonUtility.FromJson<WebcamDeviceData>(json);

        if (webcamDeviceData == null)
        {
            webcamDeviceData = new WebcamDeviceData(WebCamTexture.devices[0].name);
        }

        Init(webcamDeviceData.Name, webcamDeviceData.Width, webcamDeviceData.Height, webcamDeviceData.Fps, null);

        //SetActive(false);
    }

    public void Init(string deviceName, int width, int height, int fps, Action onCompleted = null)
    {
        webCamTexture = new WebCamTexture(deviceName, width, height, fps);
        meshRenderer.material.mainTexture = webCamTexture;
        webCamTexture.Play();

        var message = string.Format("webcam {0}: {1}x{2} : {3} fps", webCamTexture.graphicsFormat, webCamTexture.width, webCamTexture.height, webCamTexture.requestedFPS);
        Debug.Log(message);
        ServicesLog.Save(message);

        var sdp = (float)webCamTexture.width / webCamTexture.height;
        var pos = (cam.farClipPlane - 0.01f);
        transform.position = cam.transform.position + cam.transform.forward * pos;
        var z = 1155f;
        transform.localScale = new Vector3(z * sdp, z, 1.0f);

        IsInitialized = true;

        if (onCompleted == null)
        {
            return;
        }

        onCompleted.Invoke();
    }

    public void Dispose()
    {
        webCamTexture.Stop();
        Destroy(webCamTexture);
    }

    public void Stop()
    {
        webCamTexture.Stop();
    }

    public Texture GetTexture()
    {
        return webCamTexture;
    }

    public void SwapMaterial()
    {
        if (mats == null ||
            mats.Length < 2)
        {
            return;
        }

        CurrentMaterial++;

        if (CurrentMaterial >= mats.Length)
        {
            CurrentMaterial = 0;
        }

        meshRenderer.material = mats[CurrentMaterial];
        meshRenderer.material.mainTexture = webCamTexture;
    }

    public void SetMaterial(int matIndex)
    {
        CurrentMaterial = matIndex;
        meshRenderer.material = mats[CurrentMaterial];
        meshRenderer.material.mainTexture = webCamTexture;
    }

    public void SetX(float x)
    {
        meshRenderer.material.SetFloat("_X", x);
    }

    public void SetY(float y)
    {
        meshRenderer.material.SetFloat("_Y", y);
    }

    public void SetLeft(float value)
    {
        meshRenderer.material.SetFloat("_Left", value);
    }

    public void SetRight(float value)
    {
        meshRenderer.material.SetFloat("_Right", value);
    }

    public void SetTop(float value)
    {
        meshRenderer.material.SetFloat("_Top", value);
    }

    public void SetBottom(float value)
    {
        meshRenderer.material.SetFloat("_Bottom", value);
    }

    public void SetShaderScale(float value)
    {
        meshRenderer.material.SetFloat("_QuadScale", value);
    }

    public void SetThresholdSensitivity(float value)
    {
        meshRenderer.material.SetFloat("_Sensitivity", value);
    }

    public void SetSmoothing(float value)
    {
        meshRenderer.material.SetFloat("_Smooth", value);
    }

    public void SetActive(bool active)
    {
        if (active)
        {
            if (!webCamTexture.isPlaying)
            {
                webCamTexture.Play();
            }

            if (!gameObject.activeInHierarchy)
            {
                gameObject.SetActive(true);
            }

            return;
        }

        if (webCamTexture.isPlaying)
        {
            webCamTexture.Stop();
        }

        gameObject.SetActive(false);
    }
}

#endif