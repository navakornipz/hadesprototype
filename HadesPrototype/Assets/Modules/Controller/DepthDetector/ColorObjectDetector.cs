﻿using System;
using UnityEngine;

public class ColorObjectDetector
{
    public Color[] ColorBuffer;
    public Texture2D ColorTexture;

    public int Width;
    public int Height;

    public static ColorObjectDetector Instance;

    public void Init(int width, int height)
    {
        Instance = this;

        var bufferLength = width * height;
        ColorBuffer = new Color[bufferLength];
        ColorTexture = new Texture2D(width, height, TextureFormat.RGB24, false);

        Width = width;
        Height = height;
    }

    public void LoadRawTextureData(IntPtr dataPtr, int size)
    {
        ColorTexture.LoadRawTextureData(dataPtr, size);
        ColorTexture.Apply();
        ColorBuffer = ColorTexture.GetPixels();
    }

    public void EnsureBuffers(int width, int height)
    {
        if (Width == width && Height == height)
        {
            return;
        }

        Width = width;
        Height = height;

        ColorTexture.Resize(width, height);
        ColorBuffer = new Color[width * height];
    }
}
