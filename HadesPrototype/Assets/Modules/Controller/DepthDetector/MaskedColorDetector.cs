﻿using System;
using UnityEngine;

public class MaskedColorDetector
{
    private const TextureFormat Format = TextureFormat.RGBA32;

    //public Color[] ColorBuffer;
    public Texture2D BodyTexture;
    
    private static MaskedColorDetector instance;
    public static MaskedColorDetector Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new MaskedColorDetector();
            }

            return instance;
        }
    }

    public void Init(int width, int height)
    {
        //var bufferLength = width * height;
        //ColorBuffer = new Color[bufferLength];
        BodyTexture = new Texture2D(width, height, Format, false);
    }

    public void LoadRawTextureData(IntPtr dataPtr, int size)
    {
        BodyTexture.LoadRawTextureData(dataPtr, size);
        BodyTexture.Apply();
        //ColorBuffer = BodyTexture.GetPixels();
    }

    public void EnsureBuffers(int width, int height)
    {
        if (BodyTexture.width == width && BodyTexture.height == height)
        {
            return;
        }

        BodyTexture.Resize(width, height);
        //ColorBuffer = new Color[width * height];
    }
}
