﻿#if USING_DEPTH_SENSOR
using UnityEngine;

public class DepthSwipe
{
    private const float swipeTime = 0.5f;

    public StateManager<DepthSwipeState> State;
    public Vector2[][] MarkPositions;
    public int[][] MarkIndices;
    public Vector2 StartPosition;
    public Vector2 EndPosition;
    public float DistanceSq;

    public int TotalColumn;
    public int TotalRow;
    private int edgeColumn;
    private int edgeRow;

    private float timer;

    public void Init(int totalColumn, int totalRow, int edgeColumn, int edgeRow)
    {
        TotalColumn = totalColumn;
        TotalRow = totalRow;
        this.edgeColumn = edgeColumn;
        this.edgeRow = edgeRow;

        MarkPositions = new Vector2[TotalRow][];
        MarkIndices = new int[TotalRow][];

        var spaceX = (Screen.width - 2 * edgeColumn) / TotalColumn;
        var spaceY = (Screen.height - 2 * edgeRow) / TotalRow;
        for (var i = 0; i < TotalRow; i++)
        {
            var markPos = new Vector2[TotalColumn];
            var indexRow = new int[TotalColumn];
            for (var j = 0; j < TotalColumn; j++)
            {
                var newPos = new Vector2(edgeColumn + spaceX * j + spaceX / 2, edgeRow + spaceY * i + spaceY / 2);
                markPos[j] = newPos;
                var x = Screen.width - (int)newPos.x;
                var y = Screen.height - (int)newPos.y;
                indexRow[j] = y * Screen.width + x;
            }

            MarkPositions[i] = markPos;
            MarkIndices[i] = indexRow;
        }

        State = new StateManager<DepthSwipeState>();
        State.AddProcess(DepthSwipeState.Idle, IdleProcess);
        State.AddProcess(DepthSwipeState.Begin, BeginProcess);
        State.AddProcess(DepthSwipeState.Moving, MovingProcess);
        State.AddProcess(DepthSwipeState.End, EndProcess);
    }

    public void Process()
    {
        State.Process();
    }

    public void Reset()
    {
        State.Change(DepthSwipeState.Idle);
    }

    public bool IsHitPoint(int screenIndex) //screen index is screen_point.y * screen.width + screen_point.x
    {
        var index = DepthObjectDetector.Instance.DistortData.GetDistortIndex(screenIndex);
        var depth = DepthObjectDetector.Instance.DepthBuffer[index];

        if (DepthObjectDetector.Instance.WallDepthBuffer == null)
        {
            return depth > AppData.MinDistance && depth < AppData.MaxDistance;
        }

        var keyDepth = DepthObjectDetector.Instance.WallDepthBuffer[index];
        var delta = keyDepth - depth;
        return delta > DepthObjectDetector.Instance.MinDistanceFromWall && delta < DepthObjectDetector.Instance.MaxDistanceFromWall;
    }

    public int GetDepth(int screenIndex)
    {
        var index = DepthObjectDetector.Instance.DistortData.GetDistortIndex(screenIndex);
        return DepthObjectDetector.Instance.DepthBuffer[index];
    }

    private void IdleProcess()
    {
        if (!DepthObjectDetector.Instance.HasBlob)
        {
            return;
        }

        if (DepthObjectDetector.Instance.CoordinateList.Count < 500)
        {
            return;
        }

        DistanceSq = 0;
        StartPosition = DepthObjectDetector.Instance.Center;
        EndPosition = DepthObjectDetector.Instance.Center;
        State.Change(DepthSwipeState.Begin);
    }

    private void BeginProcess()
    {
        timer = 0;
        State.Change(DepthSwipeState.Moving);
    }

    private void MovingProcess()
    {
        if (!DepthObjectDetector.Instance.HasBlob)
        {
            State.Change(DepthSwipeState.End);
            return;
        }

        if (timer < swipeTime)
        {
            timer += Time.deltaTime;
            var newPosition = DepthObjectDetector.Instance.Center;
            var newDistance = Vector2.SqrMagnitude(newPosition - StartPosition);

            if (DistanceSq < newDistance)
            {
                DistanceSq = newDistance;
                EndPosition = newPosition;
            }
            return;
        }

        State.Change(DepthSwipeState.End);
    }

    private void EndProcess()
    {
        State.Change(DepthSwipeState.Idle);
    }
}
#endif