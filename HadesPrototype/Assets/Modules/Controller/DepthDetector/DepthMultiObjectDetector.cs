﻿using System.Collections.Generic;
using UnityEngine;

public class DepthMultiObjectDetector
{
    private const int limitLength = 10;

    public short[] DepthFrameData;
    public RectInt DepthRect;

    public List<Vector2Int> CoordinateList;

    public int Width;
    public int Height;
    public int CurrentDepth;

    public Vector2 Center;

    public static DepthMultiObjectDetector Instance;

    public void Init(int width, int height)
    {
        Instance = this;

        var depthBufferLength = width * height;
        DepthFrameData = new short[depthBufferLength];

        CoordinateList = new List<Vector2Int>();

        Width = width;
        Height = height;
    }

    public bool BobAreaUpdate(int minRange, int maxRange)
    {
        var minX = int.MaxValue;
        var minY = int.MaxValue;
        var maxX = int.MinValue;
        var maxY = int.MinValue;
        var minDepth = int.MaxValue;
        var length = DepthFrameData.Length;
        var depthRange = 50;

        CoordinateList.Clear();
        for (var i = 0; i < length; i++)
        {
            var depth = DepthFrameData[i];

            if (depth <= minRange ||
                depth > maxRange)
            {
                continue;
            }

            var x = i % Width;
            var y = i / Width;

            if (minDepth > depth)
            {
                if (minDepth > depth + depthRange)
                {
                    minX = x;
                    minY = y;
                    maxX = x;
                    maxY = y;

                    CoordinateList.Clear();
                    CoordinateList.Add(new Vector2Int(x, y));
                }

                minDepth = depth;
            }
            else if (depth - minDepth > depthRange)
            {
                continue;
            }

            if (minX > x)
            {
                if (minX - x > limitLength)
                {
                    continue;
                }

                minX = x;
            }

            if (maxX < x)
            {
                if (x - maxX > limitLength)
                {
                    continue;
                }

                maxX = x;
            }

            if (minY > y)
            {
                if (minY - y > limitLength)
                {
                    continue;
                }

                minY = y;
            }

            if (maxY < y)
            {
                if (y - maxY > limitLength)
                {
                    continue;
                }

                maxY = y;
            }

            CoordinateList.Add(new Vector2Int(x, y));
        }

        if (maxX < 0)
        {
            return false;
        }

        Center = new Vector2((maxX + minX) / 2, (maxY + minY) / 2);
        DepthRect = new RectInt(minX, minY, maxX - minX, maxY - minY);

        CurrentDepth = minDepth;

        return true;
    }

    public void BobSphereUpdate(short[] depthPixels, int minRange, int maxRange)
    {
        var minX = int.MaxValue;
        var minY = int.MaxValue;
        var maxX = int.MinValue;
        var maxY = int.MinValue;
        var minDepth = int.MaxValue;
        var length = depthPixels.Length;
        var depthRange = 30;
        for (var i = 0; i < length; i++)
        {
            var depth = depthPixels[i];

            if (depth <= minRange ||
                depth > maxRange)
            {
                continue;
            }

            var x = i % Width;
            var y = i / Width;

            if (minDepth > depth)
            {
                if (minDepth > depth + depthRange)
                {
                    minX = x;
                    minY = y;
                    maxX = x;
                    maxY = y;
                }

                minDepth = depth;
            }
            else if (depth - minDepth > depthRange)
            {
                continue;
            }

            if (minX > x)
            {
                if (minX - x > limitLength)
                {
                    continue;
                }

                minX = x;
            }

            if (maxX < x)
            {
                if (x - maxX > limitLength)
                {
                    continue;
                }

                maxX = x;
            }

            if (minY > y)
            {
                if (minY - y > limitLength)
                {
                    continue;
                }

                minY = y;
            }

            if (maxY < y)
            {
                if (y - maxY > limitLength)
                {
                    continue;
                }

                maxY = y;
            }
        }

        if (maxX < 0)
        {
            return;
        }

        /*var newX = ((maxX + minX) / 2) * sdp.x;
        var newY = (Height - (maxY + minY) / 2) * sdp.y;

        //bobImage.rectTransform.anchoredPosition = new Vector2(newX, newY);
        var deltaPos = (new Vector2(newX, newY) - bobImage.rectTransform.anchoredPosition) * Time.deltaTime * 8f;
        bobImage.rectTransform.anchoredPosition += deltaPos;*/

        CurrentDepth = minDepth;
    }

    public void EnsureBuffers(int width, int height)
    {
        if (Width == width && Height == height)
        {
            return;
        }

        Width = width;
        Height = height;

        //ObjectDetectorData.Instance.ChangeDepthSdp(width, height);

        DepthFrameData = new short[width * height];
    }
}
