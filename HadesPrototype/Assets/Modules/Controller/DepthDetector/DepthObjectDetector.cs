﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class DepthObjectDetector
{
    public readonly Color WallColor = new Color(0.5f, 1f, 0.5f, 0.5f);
    public readonly Color IgnoreAreaColor = new Color(1f, 0.5f, 0.5f, 0.5f);
    public const ushort ignoreAreaRange = 50;

    public ushort[] DepthBuffer;
    public ushort[] WallDepthBuffer;
    public RectInt DepthRect;
    public Texture2D DepthTexture;
    private Color[] depthTextureBuffer;

    public DistortionData DistortData;
    public List<Vector2Int> CoordinateList;
    public HashSet<int> TheIndices;
    public Vector2 Center;

    public int Width;
    public int Height;
    public int CurrentDepth;
    public ushort NearestDepthWall;
    public ushort FarestDepthWall;
    public ushort FocusWallDepth;
    public bool UseFocusWall;
    public bool HasBlob;

    private DepthDetectionType type;
    private float depthRange;
    private int limitLength;
    public ushort MinDistanceFromWall;
    public ushort MaxDistanceFromWall;

    public static DepthObjectDetector Instance;

    public void Init(int width, int height, DepthDetectionType type, float depthRange = 50f, int limitLength = 10)
    {
        Instance = this;

        var depthBufferLength = width * height;
        DepthBuffer = new ushort[depthBufferLength];

        CoordinateList = new List<Vector2Int>();
        TheIndices = new HashSet<int>();

        Width = width;
        Height = height;

        this.type = type;
        this.limitLength = limitLength;
        this.depthRange = depthRange;
    }

    public void OnDepthUpdate(ushort[] depthBuffer, int width, int height)
    {
        EnsureBuffers(width, height);
        DepthBuffer = depthBuffer;
    }

    public void InitWall(ushort[] depthBuffer, ushort maxDistanceFromWall = 1000, ushort minDistanceFromWall = 50)
    {
        WallDepthBuffer = new ushort[depthBuffer.Length];
        Array.Copy(depthBuffer, WallDepthBuffer, depthBuffer.Length);
        MaxDistanceFromWall = maxDistanceFromWall;
        MinDistanceFromWall = minDistanceFromWall;

        NearestDepthWall = ushort.MaxValue;
        FarestDepthWall = 0;
        for (var i = 0; i < depthBuffer.Length; i++)
        {
            var depth = depthBuffer[i];

            if (depth < 1000)
            {
                continue;
            }

            if (depth < NearestDepthWall)
            {
                NearestDepthWall = depth;
            }
            if (depth > FarestDepthWall)
            {
                FarestDepthWall = depth;
            }
        }

        if (DistortData != null)
        {
            var x = Screen.width / 2;
            var y = Screen.height / 2;
            var sIndex = y * Screen.width + x;
            var index = DistortData.GetDistortIndex(sIndex);
            FocusWallDepth = WallDepthBuffer[index];
            return;
        }

        FocusWallDepth = NearestDepthWall;
    }

    public bool BobAreaUpdate(int minRange, int maxRange)
    {
        HasBlob = false;
        switch (type)
        {
            case DepthDetectionType.EveryThing:
                return false;
            case DepthDetectionType.Nearest:
            case DepthDetectionType.Farest:
                return DetectRangeProcess(minRange, maxRange);
        }

        return false;
    }

    private bool DetectRangeProcess(int minRange, int maxRange)
    {
        var minX = int.MaxValue;
        var minY = int.MaxValue;
        var maxX = int.MinValue;
        var maxY = int.MinValue;
        var minDepth = int.MaxValue;
        var length = DepthBuffer.Length;

        CoordinateList.Clear();
        TheIndices.Clear();
        for (var i = 0; i < length; i++)
        {
            var depth = DepthBuffer[i];

            if (depth <= minRange ||
                depth > maxRange)
            {
                continue;
            }

            if (WallDepthBuffer != null)
            {
                var wallDepth = WallDepthBuffer[i];
                if (UseFocusWall && 
                    IsIgnoreArea(wallDepth))
                {
                    continue;
                }

                var delta = wallDepth - depth;
                if (delta > MaxDistanceFromWall)
                {
                    continue;
                }

                if (delta <= MinDistanceFromWall)
                {
                    continue;
                }
            }

            if (DistortData != null && !DistortData.AreaOfInterest.Contains(i))
            {
                continue;
            }

            if (type == DepthDetectionType.Farest)
            {
                var wallDepth = WallDepthBuffer != null ? WallDepthBuffer[i] - MinDistanceFromWall : 10000;

                if (depth > wallDepth)
                {
                    continue;
                }

                depth = (ushort)(wallDepth - depth);
            }

            var x = i % Width;
            var y = i / Width;

            if (minDepth > depth)
            {
                if (minDepth > depth + depthRange)
                {
                    minX = x;
                    minY = y;
                    maxX = x;
                    maxY = y;

                    CoordinateList.Clear();
                    TheIndices.Clear();
                    CoordinateList.Add(new Vector2Int(x, y));
                    TheIndices.Add(i);
                }

                minDepth = depth;
            }
            else if (depth - minDepth > depthRange)
            {
                continue;
            }

            if (minX > x)
            {
                if (minX - x > limitLength)
                {
                    continue;
                }

                minX = x;
            }

            if (maxX < x)
            {
                if (x - maxX > limitLength)
                {
                    continue;
                }

                maxX = x;
            }

            if (minY > y)
            {
                if (minY - y > limitLength)
                {
                    continue;
                }

                minY = y;
            }

            if (maxY < y)
            {
                if (y - maxY > limitLength)
                {
                    continue;
                }

                maxY = y;
            }

            CoordinateList.Add(new Vector2Int(x, y));
            TheIndices.Add(i);
        }

        if (maxX <= 0 || CoordinateList.Count < 1)
        {
            return false;
        }

        Center = new Vector2((maxX + minX) / 2, (maxY + minY) / 2);
        DepthRect = new RectInt(minX, minY, maxX - minX, maxY - minY);

        CurrentDepth = minDepth;

        HasBlob = true;

        return true;
    }

    public void BobSphereUpdate(ushort[] depthPixels, int minRange, int maxRange)
    {
        var minX = int.MaxValue;
        var minY = int.MaxValue;
        var maxX = int.MinValue;
        var maxY = int.MinValue;
        var minDepth = int.MaxValue;
        var length = depthPixels.Length;
        for (var i = 0; i < length; i++)
        {
            var depth = depthPixels[i];

            if (depth <= minRange ||
                depth > maxRange)
            {
                continue;
            }

            if (WallDepthBuffer != null)
            {
                var delta = WallDepthBuffer[i] - depth;

                if (delta > MaxDistanceFromWall)
                {
                    continue;
                }

                if (delta <= MinDistanceFromWall)
                {
                    continue;
                }
            }

            if (DistortData != null && !DistortData.AreaOfInterest.Contains(i))
            {
                continue;
            }

            var x = i % Width;
            var y = i / Width;

            if (minDepth > depth)
            {
                if (minDepth > depth + depthRange)
                {
                    minX = x;
                    minY = y;
                    maxX = x;
                    maxY = y;
                }

                minDepth = depth;
            }
            else if (depth - minDepth > depthRange)
            {
                continue;
            }

            if (minX > x)
            {
                if (minX - x > limitLength)
                {
                    continue;
                }

                minX = x;
            }

            if (maxX < x)
            {
                if (x - maxX > limitLength)
                {
                    continue;
                }

                maxX = x;
            }

            if (minY > y)
            {
                if (minY - y > limitLength)
                {
                    continue;
                }

                minY = y;
            }

            if (maxY < y)
            {
                if (y - maxY > limitLength)
                {
                    continue;
                }

                maxY = y;
            }
        }

        if (maxX < 0)
        {
            return;
        }

        /*var newX = ((maxX + minX) / 2) * sdp.x;
        var newY = (Height - (maxY + minY) / 2) * sdp.y;

        //bobImage.rectTransform.anchoredPosition = new Vector2(newX, newY);
        var deltaPos = (new Vector2(newX, newY) - bobImage.rectTransform.anchoredPosition) * Time.deltaTime * 8f;
        bobImage.rectTransform.anchoredPosition += deltaPos;*/

        CurrentDepth = minDepth;
    }

    public void EnsureBuffers(int width, int height)
    {
        if (Width == width && Height == height)
        {
            return;
        }

        Width = width;
        Height = height;

        //ObjectDetectorData.Instance.ChangeDepthSdp(width, height);

        DepthBuffer = new ushort[width * height];
    }

    public void MapDepthToTexture(int minRange, int maxRange)
    {
        EnsureTexture();

        var length = DepthBuffer.Length;
        for (var i = 0; i < length; i++)
        {
            var depth = DepthBuffer[i];

            /*test & debuging
            var x = i % astraConfig.TargetDepthWidth;
            var y = i / astraConfig.TargetDepthWidth;

            if (x >= depthRect.x && x <= depthRect.x + depthRect.width &&
                y >= depthRect.y && y <= depthRect.y + depthRect.height)
            {
                depthTextureBuffer[i] = Color.red;
                continue;
            }*/

            if (depth <= minRange ||
                depth > maxRange)
            {
                depthTextureBuffer[i] = new Color(0, 0, 0, 0);
                continue;
            }

            if (IsCloseToWall(depth, i))
            {
                depthTextureBuffer[i] = WallColor;
                continue;
            }

            var depthScaled = 1.0f - (depth / 10000.0f);
            var color = new Color(depthScaled, depthScaled, depthScaled, 1f);
            depthTextureBuffer[i] = color;
        }

        DepthTexture.SetPixels(depthTextureBuffer);
        DepthTexture.Apply();
    }

    public void MapFocusDepthToTexture(int minRange, int maxRange)
    {
        EnsureTexture();

        var length = DepthBuffer.Length;
        for (var i = 0; i < length; i++)
        {
            var color = new Color(0, 0, 0, 0);
            depthTextureBuffer[i] = color;
        }

        for (var i = 0; i < CoordinateList.Count; i++)
        {
            var v = CoordinateList[i];
            var index = v.y * Width + v.x;
            depthTextureBuffer[index] = Color.white;
        }

        DepthTexture.SetPixels(depthTextureBuffer);
        DepthTexture.Apply();
    }

    public void MapWallDepthToTexture()
    {
        EnsureTexture();

        var length = DepthBuffer.Length;
        for (var i = 0; i < length; i++)
        {
            var depth = WallDepthBuffer[i];
            if (UseFocusWall)
            {
                if (depth < 1200)
                {
                    depthTextureBuffer[i] = Color.black;
                }
                else
                {
                    depthTextureBuffer[i] = IsIgnoreArea(depth) ? Color.red : Color.green;
                }
                continue;
            }

            var depthScaled = 1.0f - (depth / 10000.0f);
            var color = new Color(depthScaled, depthScaled, depthScaled, 1f);
            depthTextureBuffer[i] = color;
        }

        DepthTexture.SetPixels(depthTextureBuffer);
        DepthTexture.Apply();
    }

    public bool IsCloseToWall(ushort depth, int wallDepthIndex)
    {
        return WallDepthBuffer != null && Mathf.Abs(depth - WallDepthBuffer[wallDepthIndex]) <= MinDistanceFromWall;
    }

    public bool IsIgnoreArea(ushort wallDepth)
    {
        return wallDepth - FocusWallDepth > ignoreAreaRange;
    }

    public float RawDepthToMeters(ushort depthValue)
    {
        /*if (depthValue < 2047)
        {
            return (float)(1.0 / (depthValue * -0.0030711016 + 3.3309495161));
        }

        return 0;*/
        return (float)(10.0 - depthValue / 1000.0);
    }

    private void EnsureTexture()
    {
        if (DepthTexture == null)
        {
            DepthTexture = new Texture2D(Width, Height);
            depthTextureBuffer = new Color[Width * Height];
            return;
        }

        if (DepthTexture.width != Width || DepthTexture.height != Height)
        {
            Object.Destroy(DepthTexture);
            DepthTexture = new Texture2D(Width, Height);
            depthTextureBuffer = new Color[Width * Height];
        }
    }
}
