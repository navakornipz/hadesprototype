﻿
public enum DepthSwipeState
{
    Idle,
    Begin,
    Moving,
    End
}
