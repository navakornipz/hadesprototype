﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class DepthObjectMidAirDetector
{
    public readonly Color AoiColor = new Color(0.5f, 1f, 0.5f, 0.5f);
    public readonly Color IgnoreAreaColor = new Color(1f, 0.5f, 0.5f, 0.5f);
    public const ushort ignoreAreaRange = 50;

    public ushort[] DepthBuffer;
    public RectInt DepthRect;
    public Texture2D DepthTexture;
    private Color[] depthTextureBuffer;

    public DistortionData DistortData;
    public List<Vector2Int> CoordinateList;
    public HashSet<int> TheIndices;
    public Vector2 Center;

    public int Width;
    public int Height;
    public int CurrentDepth;
    public bool HasBlob;

    private DepthDetectionType type;
    private float depthRange;
    private int maxRangeFromWall;
    private int limitLength;
    private ushort closestLengthFromWall;

    public static DepthObjectMidAirDetector Instance;

    public void Init(int width, int height, DepthDetectionType type, float depthRange = 50f, int limitLength = 10)
    {
        Instance = this;

        var depthBufferLength = width * height;
        DepthBuffer = new ushort[depthBufferLength];

        CoordinateList = new List<Vector2Int>();
        TheIndices = new HashSet<int>();

        Width = width;
        Height = height;

        this.type = type;
        this.limitLength = limitLength;
        this.depthRange = depthRange;
    }

    public void OnDepthUpdate(ushort[] depthBuffer, int width, int height)
    {
        EnsureBuffers(width, height);
        DepthBuffer = depthBuffer;
    }

    public bool BobAreaUpdate(int minRange, int maxRange)
    {
        HasBlob = false;
        switch (type)
        {
            case DepthDetectionType.EveryThing:
                return false;
            case DepthDetectionType.Nearest:
            case DepthDetectionType.Farest:
                return DetectRangeProcess(minRange, maxRange);
        }

        return false;
    }

    private bool DetectRangeProcess(int minRange, int maxRange)
    {
        var minX = int.MaxValue;
        var minY = int.MaxValue;
        var maxX = int.MinValue;
        var maxY = int.MinValue;
        var minDepth = int.MaxValue;
        var length = DepthBuffer.Length;

        CoordinateList.Clear();
        TheIndices.Clear();
        for (var i = 0; i < length; i++)
        {
            var depth = DepthBuffer[i];

            if (depth <= minRange ||
                depth > maxRange)
            {
                continue;
            }

            if (DistortData != null && !DistortData.AreaOfInterest.Contains(i))
            {
                continue;
            }

            var x = i % Width;
            var y = i / Width;

            if (minDepth > depth)
            {
                if (minDepth > depth + depthRange)
                {
                    minX = x;
                    minY = y;
                    maxX = x;
                    maxY = y;

                    CoordinateList.Clear();
                    TheIndices.Clear();
                    CoordinateList.Add(new Vector2Int(x, y));
                    TheIndices.Add(i);
                }

                minDepth = depth;
            }
            else if (depth - minDepth > depthRange)
            {
                continue;
            }

            if (minX > x)
            {
                if (minX - x > limitLength)
                {
                    continue;
                }

                minX = x;
            }

            if (maxX < x)
            {
                if (x - maxX > limitLength)
                {
                    continue;
                }

                maxX = x;
            }

            if (minY > y)
            {
                if (minY - y > limitLength)
                {
                    continue;
                }

                minY = y;
            }

            if (maxY < y)
            {
                if (y - maxY > limitLength)
                {
                    continue;
                }

                maxY = y;
            }

            CoordinateList.Add(new Vector2Int(x, y));
            TheIndices.Add(i);
        }

        if (maxX <= 0 || CoordinateList.Count < 1)
        {
            return false;
        }

        Center = new Vector2((maxX + minX) / 2, (maxY + minY) / 2);
        DepthRect = new RectInt(minX, minY, maxX - minX, maxY - minY);

        CurrentDepth = minDepth;

        HasBlob = true;

        return true;
    }

    public void BobSphereUpdate(ushort[] depthPixels, int minRange, int maxRange)
    {
        var minX = int.MaxValue;
        var minY = int.MaxValue;
        var maxX = int.MinValue;
        var maxY = int.MinValue;
        var minDepth = int.MaxValue;
        var length = depthPixels.Length;
        for (var i = 0; i < length; i++)
        {
            var depth = depthPixels[i];

            if (depth <= minRange ||
                depth > maxRange)
            {
                continue;
            }

            if (DistortData != null && !DistortData.AreaOfInterest.Contains(i))
            {
                continue;
            }

            var x = i % Width;
            var y = i / Width;

            if (minDepth > depth)
            {
                if (minDepth > depth + depthRange)
                {
                    minX = x;
                    minY = y;
                    maxX = x;
                    maxY = y;
                }

                minDepth = depth;
            }
            else if (depth - minDepth > depthRange)
            {
                continue;
            }

            if (minX > x)
            {
                if (minX - x > limitLength)
                {
                    continue;
                }

                minX = x;
            }

            if (maxX < x)
            {
                if (x - maxX > limitLength)
                {
                    continue;
                }

                maxX = x;
            }

            if (minY > y)
            {
                if (minY - y > limitLength)
                {
                    continue;
                }

                minY = y;
            }

            if (maxY < y)
            {
                if (y - maxY > limitLength)
                {
                    continue;
                }

                maxY = y;
            }
        }

        if (maxX < 0)
        {
            return;
        }

        /*var newX = ((maxX + minX) / 2) * sdp.x;
        var newY = (Height - (maxY + minY) / 2) * sdp.y;

        //bobImage.rectTransform.anchoredPosition = new Vector2(newX, newY);
        var deltaPos = (new Vector2(newX, newY) - bobImage.rectTransform.anchoredPosition) * Time.deltaTime * 8f;
        bobImage.rectTransform.anchoredPosition += deltaPos;*/

        CurrentDepth = minDepth;
    }

    public void EnsureBuffers(int width, int height)
    {
        if (Width == width && Height == height)
        {
            return;
        }

        Width = width;
        Height = height;

        //ObjectDetectorData.Instance.ChangeDepthSdp(width, height);

        DepthBuffer = new ushort[width * height];
    }

    public void MapDepthToTexture(int minRange, int maxRange)
    {
        EnsureTexture();

        var length = DepthBuffer.Length;
        for (var i = 0; i < length; i++)
        {
            var depth = DepthBuffer[i];

            if (depth <= minRange ||
                depth > maxRange)
            {
                depthTextureBuffer[i] = new Color(0, 0, 0, 0);
                continue;
            }

            var depthScaled = 1.0f - (depth / 10000.0f);
            var color = new Color(depthScaled, depthScaled, depthScaled, 1f);
            depthTextureBuffer[i] = color;
        }

        DepthTexture.SetPixels(depthTextureBuffer);
        DepthTexture.Apply();
    }

    public void MapFocusDepthToTexture(int minRange, int maxRange)
    {
        EnsureTexture();

        var length = DepthBuffer.Length;
        for (var i = 0; i < length; i++)
        {
            var color = new Color(0, 0, 0, 0);
            depthTextureBuffer[i] = color;
        }

        for (var i = 0; i < CoordinateList.Count; i++)
        {
            var v = CoordinateList[i];
            var index = v.y * Width + v.x;
            depthTextureBuffer[index] = Color.white;
        }

        DepthTexture.SetPixels(depthTextureBuffer);
        DepthTexture.Apply();
    }

    public float RawDepthToMeters(ushort depthValue)
    {
        return (float)(depthValue / 1000.0);
    }

    private void EnsureTexture()
    {
        if (DepthTexture == null)
        {
            DepthTexture = new Texture2D(Width, Height);
            depthTextureBuffer = new Color[Width * Height];
            return;
        }

        if (DepthTexture.width != Width || DepthTexture.height != Height)
        {
            Object.Destroy(DepthTexture);
            DepthTexture = new Texture2D(Width, Height);
            depthTextureBuffer = new Color[Width * Height];
        }
    }
}
