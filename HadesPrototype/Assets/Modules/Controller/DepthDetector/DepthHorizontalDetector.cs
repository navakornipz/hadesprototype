﻿using UnityEngine;

public class DepthHorizontalDetector
{
    // depth scale = depth / 1000 meters
    private const int minRange = 1;
    private const int maxRange = 5000;
    private readonly Color solidColor = new Color(0.5f, 1f, 0.5f, 0.5f);
    private readonly Color transparentColor = new Color(1f, 1f, 1f, 0);

    public Texture2D DebugTexture;
    private Color[] defaultBuffer;

    public int DepthWidth { get; private set; }
    public int DepthHeight { get; private set; }

    public void Init(int depthWidth, int depthHeight)
    {
        DepthWidth = depthWidth;
        DepthHeight = depthHeight;

        DebugTexture = new Texture2D(DepthWidth, (DepthWidth * Screen.height) / Screen.width);
        defaultBuffer = new Color[DepthWidth * DebugTexture.height];

        for (var i = 0; i < defaultBuffer.Length; i++)
        {
            defaultBuffer[i] = solidColor;
        }

        DebugTexture.SetPixels(defaultBuffer);
        DebugTexture.Apply();
    }

    public void OnDepthUpdate(ushort[] depthBuffer)
    {
        var min = ushort.MaxValue;
        var max = ushort.MinValue;

        /*for (var i = 0; i < depthBuffer.Length; i++)
        {
            var depth = depthBuffer[i];

            if (min > depth)
            {
                min = depth;
            }
            if (max < depth)
            {
                max = depth;
            }
        }*/

        var p = depthBuffer.Length / 2;
        var depth = depthBuffer[p - 1];

        if (depth == 0)
        {
            return;
        }

        Debug.LogErrorFormat("depth: {0}", depth);
    }
}
