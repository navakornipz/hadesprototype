﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    private const int startSources = 1;

    [SerializeField] private VideoPlayer videoPlayerPrefab;

    private Queue<VideoPlayer> freePlayer;
    private VideoPlayer playingPlayer;
    private Queue<VideoPlayer> waitingPlayer;

    private RawImage presentImage;
    private RawImage nextImage;

    public static VideoManager Instance;

    public bool IsPlaying
    {
        get
        {
            return playingPlayer != null && playingPlayer.isPlaying;
        }
    }

    private void Awake()
    {
        Instance = this;
        freePlayer = new Queue<VideoPlayer>();
        waitingPlayer = new Queue<VideoPlayer>();
    }

    private void Start()
    {
        for (var i = 0; i < startSources; i++)
        {
            AddVideoSource();
        }
    }

    private void Update()
    {
        if (waitingPlayer.Count > 0)
        {
            if (playingPlayer.clip.length - playingPlayer.time <= 1f)
            {
                var nextPlayer = waitingPlayer.Dequeue();
                StartCoroutine(DistortProcess(presentImage, nextImage));
            }
        }
    }

    public void PlayVideo(VideoClip clip, RawImage rawImage, bool isLoop = false)
    {
        var renderTexture = new RenderTexture((int)clip.width, (int)clip.height, 0);

        if (freePlayer.Count < 1)
        {
            AddVideoSource();
        }

        if (playingPlayer != null && playingPlayer.isPlaying)
        {
            var nextPlayer = freePlayer.Dequeue();
            nextPlayer.clip = clip;
            nextPlayer.targetTexture = renderTexture;
            rawImage.texture = renderTexture;
            nextPlayer.Play();
            nextPlayer.isLooping = isLoop;
            if (presentImage != null)
            {
                StartCoroutine(DistortProcess(presentImage, nextImage));
            }
            else
            {
                playingPlayer.Stop();
                playingPlayer = nextPlayer;
            }

            return;
        }

        if (playingPlayer == null)
        {
            playingPlayer = freePlayer.Dequeue();
        }
        
        playingPlayer.clip = clip;
        playingPlayer.targetTexture = renderTexture;
        rawImage.texture = renderTexture;
        playingPlayer.isLooping = isLoop;
        playingPlayer.Play();
    }

    public void PlayVideo(string url, int width, int height, RawImage rawImage, bool isLoop = false)
    {
        var renderTexture = new RenderTexture(width, height, 0);

        if (freePlayer.Count < 1)
        {
            AddVideoSource();
        }

        if (playingPlayer != null && playingPlayer.isPlaying)
        {
            var nextPlayer = freePlayer.Dequeue();
            nextPlayer.url = url;
            nextPlayer.targetTexture = renderTexture;
            rawImage.texture = renderTexture;
            nextPlayer.Play();
            nextPlayer.isLooping = isLoop;
            if (presentImage != null)
            {
                StartCoroutine(DistortProcess(presentImage, nextImage));
            }
            else
            {
                playingPlayer.Stop();
                playingPlayer = nextPlayer;
            }

            return;
        }

        if (playingPlayer == null)
        {
            playingPlayer = freePlayer.Dequeue();
        }

        playingPlayer.url = url;
        playingPlayer.targetTexture = renderTexture;
        rawImage.texture = renderTexture;
        playingPlayer.isLooping = isLoop;
        playingPlayer.Play();
    }

    public void PlayVideo(VideoClip clip, Renderer renderer, bool isLoop = false)
    {
        var renderTexture = new RenderTexture((int)clip.width, (int)clip.height, 0);

        if (freePlayer.Count < 1)
        {
            AddVideoSource();
        }

        if (playingPlayer != null && playingPlayer.isPlaying)
        {
            var nextPlayer = freePlayer.Dequeue();
            nextPlayer.clip = clip;
            nextPlayer.renderMode = VideoRenderMode.MaterialOverride;
            nextPlayer.targetMaterialRenderer = renderer;
            nextPlayer.Play();
            nextPlayer.isLooping = isLoop;
            if (presentImage != null)
            {
                StartCoroutine(DistortProcess(presentImage, nextImage));
            }
            else
            {
                playingPlayer.Stop();
                playingPlayer = nextPlayer;
            }
            return;
        }

        if (playingPlayer == null)
        {
            playingPlayer = freePlayer.Dequeue();
        }

        playingPlayer.clip = clip;
        playingPlayer.renderMode = VideoRenderMode.MaterialOverride;
        playingPlayer.targetMaterialRenderer = renderer;
        playingPlayer.isLooping = isLoop;
        playingPlayer.Play();
    }

    public void PlayVideo(string url, int width, int height, Renderer renderer, bool isLoop = false)
    {
        var renderTexture = new RenderTexture(width, height, 0);

        if (freePlayer.Count < 1)
        {
            AddVideoSource();
        }

        if (playingPlayer != null && playingPlayer.isPlaying)
        {
            var nextPlayer = freePlayer.Dequeue();
            nextPlayer.url = url;
            nextPlayer.renderMode = VideoRenderMode.MaterialOverride;
            nextPlayer.targetMaterialRenderer = renderer;
            nextPlayer.Play();
            nextPlayer.isLooping = isLoop;
            if (presentImage != null)
            {
                StartCoroutine(DistortProcess(presentImage, nextImage));
            }
            else
            {
                playingPlayer.Stop();
                playingPlayer = nextPlayer;
            }

            return;
        }

        if (playingPlayer == null)
        {
            playingPlayer = freePlayer.Dequeue();
        }

        playingPlayer.url = url;
        playingPlayer.renderMode = VideoRenderMode.MaterialOverride;
        playingPlayer.targetMaterialRenderer = renderer;
        playingPlayer.isLooping = isLoop;
        playingPlayer.Play();
    }

    public void Stop()
    {
        if (playingPlayer == null || !playingPlayer.isPlaying)
        {
            return;
        }

        playingPlayer.Stop();
        freePlayer.Enqueue(playingPlayer);
        playingPlayer = null;
    }

    private IEnumerator DistortProcess(RawImage present, RawImage next)
    {
        var duration = 1f;
        var fadeSpeed = 1f / duration;
        var color1 = present.color;
        var color2 = next.color;
        color1.a = 1f;
        color2.a = 0f;

        while (color1.a > 0)
        {
            var delta = fadeSpeed * Time.deltaTime;
            color1.a -= delta;
            color2.a += delta;
            present.color = color1;
            next.color = color2;
            yield return null;
        }

        present.color = new Color(1f, 1f, 1f, 0f);
        next.color = Color.white;
    }

    private void AddVideoSource()
    {
        var newPlayer = NewSource();
        freePlayer.Enqueue(newPlayer);
    }

    private VideoPlayer NewSource()
    {
        var newPlayer = Instantiate(videoPlayerPrefab, transform);
        return newPlayer;
    }

    public static void ClearRenderTexture(RenderTexture renderTexture)
    {
        RenderTexture rt = RenderTexture.active;
        RenderTexture.active = renderTexture;
        GL.Clear(true, true, Color.clear);
        RenderTexture.active = rt;
    }
}
