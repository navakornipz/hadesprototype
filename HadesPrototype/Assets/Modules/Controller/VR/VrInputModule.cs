﻿#if USING_STEAM_VR

using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;

public class VrInputModule : BaseInputModule
{
    [SerializeField] private Camera camera;
    [SerializeField] private SteamVR_Input_Sources targetSources;
    [SerializeField] private SteamVR_Action_Boolean clickAction;

    private GameObject currentObject;
    private PointerEventData data;

    protected override void Awake()
    {
        base.Awake();

        data = new PointerEventData(eventSystem);
    }

    public override void Process()
    {
        if (!SteamVR.active)
        {
            return;
        }

        data.Reset();
        data.position = new Vector2(camera.pixelWidth * 0.5f, camera.pixelHeight * 0.5f);

        eventSystem.RaycastAll(data, m_RaycastResultCache);
        data.pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
        currentObject = data.pointerCurrentRaycast.gameObject;

        m_RaycastResultCache.Clear();

        HandlePointerExitAndEnter(data, currentObject);

        if (clickAction.GetStateDown(targetSources))
        {
            ProcessPress(data);
        }

        if (clickAction.GetStateUp(targetSources))
        {
            ProcessRelease(data);
        }
    }

    public PointerEventData GetData()
    {
        return data;
    }

    private void ProcessPress(PointerEventData data)
    {

    }

    private void ProcessRelease(PointerEventData data)
    {

    }
}

#endif //USING_STEAM_VR