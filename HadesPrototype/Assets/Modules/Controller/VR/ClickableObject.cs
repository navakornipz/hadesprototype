﻿using System;
using UnityEngine;

public class ClickableObject : MonoBehaviour
{
    public Action OnClick;
}
