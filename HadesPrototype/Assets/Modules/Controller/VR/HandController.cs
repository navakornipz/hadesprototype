﻿#if USING_STEAM_VR
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class HandController : MonoBehaviour
{
    [SerializeField] private SteamVR_Action_Boolean grabAction;
    [SerializeField] private GameObject model;

    private SteamVR_Behaviour_Pose pose;
    private FixedJoint joint;

    private InteractableMe currentInteractable;
    private List<InteractableMe> contactInteractables;

    private ShoulderController currentShoulderController;

    private void Awake()
    {
        contactInteractables = new List<InteractableMe>();
        pose = GetComponentInParent<SteamVR_Behaviour_Pose>();
        joint = GetComponent<FixedJoint>();
    }

    private void Update()
    {
        if (grabAction.GetStateDown(pose.inputSource))
        {
            Pickup();
        }

        if (grabAction.GetStateUp(pose.inputSource))
        {
            Drop();
        }
    }

    private void LateUpdate()
    {
        if (currentInteractable == null)
        {
            return;
        }

        model.transform.position = currentInteractable.transform.position;
        transform.position = currentInteractable.transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Interactable"))
        {
            return;
        }

        var newObject = other.gameObject.GetComponent<InteractableMe>();
        if (newObject == null)
        {
            return;
        }

        if (contactInteractables.Contains(newObject))
        {
            return;
        }

        contactInteractables.Add(other.gameObject.GetComponent<InteractableMe>());
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.gameObject.CompareTag("Interactable"))
        {
            return;
        }

        contactInteractables.Remove(other.gameObject.GetComponent<InteractableMe>());
    }

    public void Pickup()
    {
        currentInteractable = GetNearestInteractable();

        if (currentInteractable == null)
        {
            return;
        }

        if (currentInteractable.Hand)
            currentInteractable.Hand.Drop();

        //currentInteractable.transform.position = transform.position;
        transform.position = currentInteractable.transform.position;
        model.transform.position = currentInteractable.transform.position;

        //joint.connectedBody = currentInteractable.Rigidbody;

        currentInteractable.Hand = this;
    }

    public void Drop()
    {
        if (currentInteractable == null)
        {
            return;
        }

        //var targetBody = currentInteractable.Rigidbody;
        //targetBody.velocity = pose.GetVelocity();
        //targetBody.angularVelocity = pose.GetAngularVelocity();
        currentInteractable.transform.localPosition = Vector3.zero;
        model.transform.localPosition = Vector3.zero;

        joint.connectedBody = null;

        currentInteractable.Hand = null;
        currentInteractable = null;
    }

    private InteractableMe GetNearestInteractable()
    {
        InteractableMe nearest = null;
        var minDistance = float.MaxValue;
        var distance = 0f;

        foreach (var interactable in contactInteractables)
        {
            distance = (interactable.transform.position - transform.position).sqrMagnitude;

            if (distance >= minDistance)
            {
                continue;
            }

            minDistance = distance;
            nearest = interactable;
        }

        return nearest;
    }
}

#endif //USING_STEAM_VR