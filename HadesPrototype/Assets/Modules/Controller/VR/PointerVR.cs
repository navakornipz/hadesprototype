﻿#if USING_STEAM_VR

using UnityEngine;

public class PointerVR : MonoBehaviour
{
    public const float DefaultLength = 5f;

    [SerializeField] private GameObject dot;
    [SerializeField] private VrInputModule inputModule;

    private LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        UpdateLine();
    }

    private void UpdateLine()
    {
        var targetLength = DefaultLength;

        var hit = CreateRaycast(targetLength);

        var endPosition = transform.position + (transform.forward * targetLength);

        if (hit.collider != null)
            endPosition = hit.point;

        dot.transform.position = endPosition;

        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, endPosition);
    }

    private RaycastHit CreateRaycast(float length)
    {
        RaycastHit hit;
        var ray = new Ray(transform.position, transform.forward);
        Physics.Raycast(ray, out hit, length);

        return hit;
    }
}

#endif