﻿#if USING_STEAM_VR

using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Rigidbody))]
public class InteractableMe : MonoBehaviour
{
    [HideInInspector] public HandController Hand;

    public Rigidbody Rigidbody;

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
    }
}

#endif //USING_STEAM_VR