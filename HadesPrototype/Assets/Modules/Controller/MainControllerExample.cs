﻿using UnityEngine;

public class MainControllerExample : BaseMain
{
    #region SerializeField

#if UNITY_EDITOR
    [SerializeField] private AppState startState;
#endif

    #endregion SerializeField

    #region Members

    #endregion Members

    #region Properties

    public static MainControllerExample Instance;

    #endregion

    #region Unity Method

    private void Awake()
    {
        Instance = this;

        state = new StateManager<AppState>();
        state.AddEnterMessage(AppState.Start, StartEnter);
        state.AddEnterMessage(AppState.Home, HomeEnter);
        state.AddProcess(AppState.Home, HomeProcess);
        state.AddLeaveMessage(AppState.Home, HomeLeave);
        state.AddEnterMessage(AppState.End, EndEnter);
        state.AddProcess(AppState.End, EndProcess);
        state.AddLeaveMessage(AppState.End, EndLeave);
    }

    private void Start()
    {
        state.Change(AppState.Start);
    }

    private void Update()
    {
        state.Process();
    }

    #endregion Unity Method

    #region Command

    #endregion Command

    #region Event

    protected override void StartEnter()
    {
        base.StartEnter();

#if UNITY_EDITOR
        if (startState == AppState.Start)
        {
            startState = AppState.Home;
        }
        state.Change(startState);
#else
        state.Change(AppState.Home);
#endif
    }

    private void HomeEnter()
    {
        StartFadeIn(null);
        //state.NextState = AppState.Play;
    }


    private void HomeProcess()
    {
        if (IsFading)
        {
            return;
        }
    }

    private void HomeLeave()
    {

    }

    private void EndEnter()
    {
        StartFadeIn(null);
        state.NextState = AppState.Home;
    }

    private void EndProcess()
    {
    }

    private void EndLeave()
    {
        Resources.UnloadUnusedAssets();
    }

    #endregion Event

    #region IEnumerator

    #endregion IEnumerator
}
