﻿using System.Collections.Generic;

public class StateMachine<T>
{
    #region Members

    private Dictionary<T, BaseState> states;

    public T CurrentState { get; private set; }
    public T PreviousState { get; private set; }
    public T NextState { get; set; }

    public bool IsInTransition;

    private BaseState currentState;

    #endregion

    #region Initialize

    public StateMachine()
    {
        states = new Dictionary<T, BaseState>();
        CurrentState = default(T);
    }

    public void AddState(T state, BaseState newState)
    {
        if (states.ContainsKey(state))
        {
            states.Remove(state);
        }

        newState.Init();

        states.Add(state, newState);
    }

    #endregion

    #region Command

    public void Process()
    {
        if (IsInTransition)
        {
            return;
        }

        if (currentState == null)
        {
            return;
        }

        currentState.Update();
    }

    public void Change(T newState)
    {
        if (currentState != null)
        {
            currentState.OnLeave();
        }

        PreviousState = CurrentState;
        CurrentState = newState;

        if (!states.TryGetValue(newState, out currentState))
        {
            return;
        }

        currentState.OnEnter();

        IsInTransition = false;
    }

    #endregion

}
