﻿
public abstract class BaseState
{
    protected MainController main;

    public virtual void Init()
    {
        main = MainController.Instance;
    }

    public virtual void OnEnter() {}
    public virtual void Update() {}
    public virtual void OnLeave() {}
}
