﻿using System.Collections.Generic;

public class StateManager<T>
{
    #region Define

    public delegate void CALLBACK();

    #endregion

    #region Members

    private Dictionary<T, CALLBACK> leaveList;
    private Dictionary<T, CALLBACK> enterList;
    private Dictionary<T, CALLBACK> processList;

    public T CurrentState { get; private set; }
    public T PreviousState { get; private set; }
    public T NextState { get; set; }

    public bool IsInTransition;

    #endregion

    #region Initialize

    public StateManager()
    {
        leaveList = new Dictionary<T, CALLBACK>();
        enterList = new Dictionary<T, CALLBACK>();
        processList = new Dictionary<T, CALLBACK>();

        CurrentState = default(T);
    }

    public void AddLeaveMessage(T state, CALLBACK callback)
    {
        if (leaveList.ContainsKey(state))
        {
            leaveList.Remove(state);
        }

        leaveList.Add(state, callback);
    }

    public void AddEnterMessage(T state, CALLBACK callback)
    {
        if (enterList.ContainsKey(state))
        {
            enterList.Remove(state);
        }

        enterList.Add(state, callback);
    }

    public void AddProcess(T state, CALLBACK callback)
    {
        if (processList.ContainsKey(state))
        {
            processList.Remove(state);
        }

        processList.Add(state, callback);
    }

    #endregion

    #region Command

    public void Process()
    {
        if (IsInTransition)
        {
            return;
        }

        CALLBACK process;
        if (!processList.TryGetValue(CurrentState, out process))
        {
            return;
        }

        process.Invoke();
    }

    public void Change(T newState)
    {
        CALLBACK process;

        if (leaveList.TryGetValue(CurrentState, out process))
        {
            process.Invoke();
        }

        PreviousState = CurrentState;
        CurrentState = newState;

        if (enterList.TryGetValue(newState, out process))
        {
            process.Invoke();
        }

        IsInTransition = false;
    }

    #endregion

}
