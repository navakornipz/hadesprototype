﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class LocalizeManager
{
    public const int thai = 0;

    public Dictionary<string, string> Data;

    public static LocalizeManager Instance;

    public void Init(int languageIndex, string filePath)
    {
        var textAsset = Resources.Load<TextAsset>(filePath);

        var lines = textAsset.text.Split('\r', '\n');

        if (Data == null)
        {
            Data = new Dictionary<string, string>();
        }
        else
        {
            Data.Clear();
        }

        foreach (var line in lines)
        {
            if (line.Length < 1 ||
                line[0] == '#')
            {
                continue;
            }

            var parts = Regex.Matches(line, @"[\""].+?[\""]|[^,]+")
                .Cast<Match>()
                .Select(m => m.Value)
                .ToList();

            if (parts.Count < 2)
            {
                continue;
            }

            if (languageIndex + 1 >= parts.Count)
            {
                Debug.LogErrorFormat("not found this language index: {0}", languageIndex);
                continue;
            }

            var key = parts[0];
            if (Data.ContainsKey(key))
            {
                Debug.LogErrorFormat("found duplicate key: {0}", key);
                continue;
            }
            var value = parts[languageIndex + 1];
            value = RemoveQuote(value);
            value = value.Replace("\\n", "\n");

            if (languageIndex == thai)
            {
                value = ThaiFontAdjuster.Adjust(value);
            }

            Data.Add(key, value);
        }
    }

    public string GetText(string key)
    {
        var value = string.Empty;
        Data.TryGetValue(key, out value);

        if (string.IsNullOrEmpty(value))
        {
            return string.Format("key {0} text not found!", key);
        }

        return value;
    }

    public string RemoveQuote(string value)
    {
        var newValue = value;
        if (value[0] == '"')
        {
            newValue = newValue.Remove(0, 1);
        }
        if (value[value.Length - 1] == '"')
        {
            newValue = newValue.Remove(newValue.Length - 1);
        }
        return newValue;
    }
}
