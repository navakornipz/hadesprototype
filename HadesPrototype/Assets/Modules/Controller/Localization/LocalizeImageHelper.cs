﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class LocalizeImageHelper : MonoBehaviour
{
    [SerializeField] private Sprite[] sprites;

    private void Start()
    {
        var image = GetComponent<Image>();

        if (AppData.SelectedLanguageIndex != 0)
        {
            var sIndex = AppData.SelectedLanguageIndex - 1;
            image.sprite = sprites[sIndex];
        }

        Destroy(this);
    }
}
