﻿using UnityEngine;
using UnityEngine.UI;

public class LocalizeHelper : MonoBehaviour
{
    private void Start()
    {
        var text = GetComponent<Text>();

        if (text == null)
        {
            return;
        }

        var newText = LocalizeManager.Instance.GetText(text.text);

        if (!string.IsNullOrEmpty(newText))
        {
            text.text = newText;
        }

        Destroy(this);
    }
}
