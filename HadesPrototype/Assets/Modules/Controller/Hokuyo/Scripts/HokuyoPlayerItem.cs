﻿#if USING_HOKUYO

using UnityEngine;
using UnityEngine.UI;

public class HokuyoPlayerItem : MonoBehaviour
{
    [SerializeField] private Text nameText;

    private Button button;
    [HideInInspector] public Vector2 WorldPosition;
    private bool isMark;
    public int PoIndex;

    [HideInInspector] public RectTransform RectTf;

    private void Awake()
    {
        button = GetComponentInChildren<Button>();
        button.onClick.AddListener(OnClick);

        RectTf = GetComponent<RectTransform>();

        if (RectTf != null)
        {
            return;
        }

        RectTf = gameObject.AddComponent<RectTransform>();
    }

    public void Init(string name, Vector2 worldPosition)
    {
        this.name = name;
        WorldPosition = worldPosition;
        nameText.text = name;
    }

    public void Mark()
    {
        button.GetComponent<Image>().color = Color.red;
        name = string.Format("M.{0}", nameText.text);
        nameText.text = name;
        isMark = true;
    }

    public void OnClick()
    {
        if (!isMark)
        {
            CalibratePanel.Instance.CreateMark(this);
            return;
        }
        
        
    }
}

#endif