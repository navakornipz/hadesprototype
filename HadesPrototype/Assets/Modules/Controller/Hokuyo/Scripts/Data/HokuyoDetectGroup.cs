﻿
public class HokuyoDetectGroup
{
    public const int MaxDiffStep = 20;
    public const int MaxDiffDistance = 500;

    public int StartStep;
    public int EndStep;
    public long MinDistance;
    public long MaxDistance;
    public long AverageDistance;
    public int MemberCount;

    public HokuyoDetectGroup()
    {
        StartStep = -1;
        EndStep = -1;
        MinDistance = long.MaxValue;
        MaxDistance = long.MinValue;
        AverageDistance = 0;
        MemberCount = 0;
    }
}
