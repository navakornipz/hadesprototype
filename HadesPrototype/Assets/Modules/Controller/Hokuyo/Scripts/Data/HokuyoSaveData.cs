﻿using System;
using UnityEngine;

[Serializable]
public class HokuyoSaveData
{
    public int TopLeftX;
    public int TopLeftY;
    public int TopRightX;
    public int TopRightY;
    public int BottomLeftX;
    public int BottomLeftY;
    public int BottomRightX;
    public int BottomRightY;

    public int AreaWidth;

    internal Vector2 GetTL()
    {
        return new Vector2(TopLeftX, TopLeftY);
    }

    internal Vector2 GetTR()
    {
        return new Vector2(TopRightX, TopRightY);
    }

    internal Vector2 GetBL()
    {
        return new Vector2(BottomLeftX, BottomLeftY);
    }

    internal Vector2 GetBR()
    {
        return new Vector2(BottomRightX, BottomRightY);
    }
}
