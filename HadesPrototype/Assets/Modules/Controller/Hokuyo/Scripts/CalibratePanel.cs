﻿#if USING_HOKUYO

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalibratePanel : BasePanel
{
    private const int width = 1024;
    private const int height = 1024;
    private const int radius = width / 2;
    private readonly Color blank = new Color(0, 0, 0, 0);

    [SerializeField] private Image image;
    [SerializeField] private Text debugText;
    [SerializeField] private HokuyoPlayerItem[] groupItemPrefab;
    [SerializeField] private GameObject selectSpotPanel;

    private Color[] originBuffers;
    private List<Color> colorBuffers;
    private List<HokuyoPlayerItem> markList;
    private Texture2D texture;
    private PoolManager<HokuyoPlayerItem> itemPool;
    private GameObject itemContainner;
    private LineRenderer2D lineRendererPrefab;
    private List<LineRenderer2D> lines;
    private HokuyoPlayerItem selectedItem;

    private int frameCount;

    public static CalibratePanel Instance;

    protected override void OnAwake()
    {
        Instance = this;

        base.OnAwake();

        colorBuffers = new List<Color>();
        markList = new List<HokuyoPlayerItem>();
        originBuffers = new Color[width * height];
        lineRendererPrefab = GlobalResources.GetLineRenderer2DPrefab();
        lines = new List<LineRenderer2D>();

        for (var i = 0; i < originBuffers.Length; i++)
        {
            originBuffers[i] = blank;
        }

        texture = new Texture2D(width, height);
        texture.SetPixels(originBuffers);
        texture.Apply();

        image.sprite = Sprite.Create(texture, new Rect(0, 0, width, height), new Vector2(0.5f, 0.5f));

        itemContainner = new GameObject("item.containner");
        itemContainner.transform.SetParent(transform);

        itemPool = new PoolManager<HokuyoPlayerItem>();
        itemPool.Init(groupItemPrefab, itemContainner.transform, 30);

        selectSpotPanel.SetActive(false);
    }

    protected override void IdleProcess()
    {
        /*var center = new Vector2(width / 2, height / 2);
        colorBuffers.Clear();
        colorBuffers.AddRange(originBuffers);
        for (var i = 0; i < 360; i++)
        {
            var angle = -120 + i;

            var endpoint = (Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.up).normalized * 512;
            //Debug.LogError(endpoint);
            DrawLine(center, endpoint);
            //break;
        }
        texture.SetPixels(colorBuffers.ToArray());
        texture.Apply();
        return;*/
        if (!HokuyoController.Instance.IsInitialized)
        {
            debugText.text = "can't connect hokuyo";
            return;
        }

        if (frameCount < AppData.CalibrateFrameSkip)
        {
            frameCount++;
            return;
        }

        frameCount = 0;

        HokuyoController.Instance.ReadData();

        if (!string.IsNullOrEmpty(HokuyoController.Instance.ErrorMessage))
        {
            Debug.LogError(HokuyoController.Instance.ErrorMessage);
            return;
        }

        HokuyoController.Instance.CollectPoint();

        colorBuffers.Clear();
        colorBuffers.AddRange(originBuffers);

        var distances = HokuyoController.Instance.Distances;

        if (distances.Count < 1)
        {
            return;
        }

        itemPool.ReturnAll();

        var center = new Vector2(width / 2, height / 2);
        var dMax = HokuyoController.Instance.MaxDistance;
        var aMin = HokuyoController.Instance.aMin;
        var aMax = HokuyoController.Instance.aMax;
        var startAngle = HokuyoController.Instance.StartAngle;
        var groupList = HokuyoController.Instance.GroupList;
        for (var i = 0; i < groupList.Count; i++)
        {
            var group = groupList[i];

            if (group.MemberCount < AppData.MinimumMember)
            {
                continue;
            }

            var averageStep = (group.EndStep + group.StartStep) / 2;
            var distancePixel = (int)(((float)group.AverageDistance / dMax) * radius);
            var angle = startAngle + HokuyoController.Instance.IncreaseAngle * (averageStep - aMin);
            var endpoint = (Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.up).normalized * distancePixel;
            var worldPosition = HokuyoController.Instance.ConvertToVector2(averageStep, group.AverageDistance);

            var newItem = itemPool.Get(image.transform);
            newItem.Init(string.Format("p{0}", i + 1), worldPosition);
            newItem.RectTf.anchoredPosition = new Vector2(endpoint.x, endpoint.y);
        }
        
        for (var i = aMin; i <= aMax; i++)
        {
            var distance = distances[i];
            var angle = startAngle + HokuyoController.Instance.IncreaseAngle * (i - aMin);

            if (distance <= HokuyoController.Instance.MinDistance)
            {
                distance = dMax;
            }

            var distancePixel = (int)(((float)distance / dMax) * radius);
            var endpoint = (Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.up).normalized * distancePixel;
            DrawLine(center, endpoint);
        }

        texture.SetPixels(colorBuffers.ToArray());
        texture.Apply();
    }

    private void DrawLine(Vector2 center, Vector2 endpoint)
    {
        var deltaX = endpoint.x;
        var deltaY = endpoint.y;

        var m = endpoint.y / endpoint.x;

        var start = (int)(endpoint.x < 0 ? endpoint.x : 0);
        var end = (int)(endpoint.x < 0 ? 0 : endpoint.x);

        for (var x = start; x < end; x++)
        {
            var y = (int)(m * x);
            var x1 = (int)center.x + x;
            var y1 = (int)center.y + y;
            var index = y1 * width + x1;
            if (index < 0 || index >= colorBuffers.Count)
            {
                continue;
            }
            colorBuffers[index] = Color.green;
        }
    }

    public void CreateMark(HokuyoPlayerItem item)
    {
        selectSpotPanel.SetActive(true);

        var newItem = Instantiate(item, image.transform);
        newItem.RectTf.anchoredPosition = item.RectTf.anchoredPosition;
        newItem.Init(string.Format("p{0}", markList.Count), item.WorldPosition);
        newItem.Mark();
        markList.Add(newItem);
        selectedItem = newItem;

        if (markList.Count < 1)
        {
            return;
        }

        for (var i = 0; i < lines.Count; i++)
        {
            Destroy(lines[i].gameObject);
        }
        lines.Clear();

        var center = new Vector2(512f, 512f);
        for (var i = 0; i < markList.Count; i++)
        {
            var newLine = Instantiate(lineRendererPrefab, image.transform);
            var v1 = markList[i].RectTf.anchoredPosition + center;
            var next = i + 1;

            if (next >= markList.Count)
            {
                next = 0;
            }
            var v2 = markList[next].RectTf.anchoredPosition + center;
            newLine.Init(v1, v2, Color.red, 2);
            lines.Add(newLine);
        }
    }

    public void OnTopLeftClick()
    {
        selectedItem.PoIndex = 0;
        selectSpotPanel.SetActive(false);
    }

    public void OnTopRightClick()
    {
        selectedItem.PoIndex = 1;
        selectSpotPanel.SetActive(false);
    }

    public void OnBottomLeftClick()
    {
        selectedItem.PoIndex = 2;
        selectSpotPanel.SetActive(false);
    }

    public void OnBottomRightClick()
    {
        selectedItem.PoIndex = 3;
        selectSpotPanel.SetActive(false);
    }

    private void CreateVisualMark(HokuyoPlayerItem item)
    {
        
    }

    public void OnApply()
    {
        if (markList.Count != 4)
        {
            return;
        }

        var center = (markList[0].WorldPosition + markList[1].WorldPosition + markList[2].WorldPosition + markList[3].WorldPosition) / 4;
        var tl = Vector2.zero;
        var tr = Vector2.zero;
        var bl = Vector2.zero;
        var br = Vector2.zero;
        var areaWidth = HokuyoController.Instance.MaxDistance * 2;
        for (var i = 0; i < markList.Count; i++)
        {
            var mark = markList[i];

            switch (mark.PoIndex)
            {
                case 0:
                    tl = mark.WorldPosition;
                    break;
                case 1:
                    tr = mark.WorldPosition;
                    break;
                case 2:
                    bl = mark.WorldPosition;
                    break;
                case 3:
                    br = mark.WorldPosition;
                    break;
            }
        }

        //MainController.Instance.InitDistortionData(areaWidth, tl, tr, bl, br);
        AppData.SaveHokuyoData(areaWidth, tl, tr, bl, br);
    }

    public void OnClear()
    {
        if (markList.Count < 1)
        {
            return;
        }

        for (var i = 0; i < markList.Count; i++)
        {
            Destroy(markList[i].gameObject);
            Destroy(lines[i].gameObject);
        }

        markList.Clear();
        lines.Clear();
    }

    public void CloseClick()
    {
        MainController.Instance.ChangeState(AppState.Home);
    }
}

#endif