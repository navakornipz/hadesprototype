﻿#if USING_HOKUYO

using System;
using System.Collections.Generic;
using System.IO.Ports;
using SCIP_library;
using UnityEngine;

public class HokuyoController
{
    private const int GET_NUM = 10;
    private const int start_step = 0;
    private const int end_step = 760;

    private string portName;
    private int baudrate;

    private string modelName;   //Model information of the sensor.
    public int MinDistance;    //Minimum measurable distance(mm)
    public int MaxDistance;    //Maximum measurable distance(mm)
    private int aRes;           //Angular resolution(Number of splits in 360 degree)
    public int aMin;            //First Step of the Measurement Range
    public int aMax;            //Last Step of the Measurement Range
    private int afrt;           //Step number on the sensor's front axis
    private int scan;           //Standard angular velocity
    public float IncreaseAngle;
    public float StartAngle;
    public string stat;

    private SerialPort serialPort;

    public List<HokuyoDetectGroup> GroupList;
    public List<long> Distances;
    public string ErrorMessage;

    public bool IsInitialized { get; private set; }

    public static HokuyoController Instance;

    public void Init(string portName, int baudrate, Action<string> OnCompleted)
    {
        this.portName = portName;
        this.baudrate = baudrate;
        Distances = new List<long>();
        GroupList = new List<HokuyoDetectGroup>();

        try
        {
            serialPort = new SerialPort(portName, baudrate);
            serialPort.NewLine = "\n\n";

            serialPort.Open();

            serialPort.Write(SCIP_Writer.PP());
            ReadPP(serialPort.ReadLine());

            serialPort.Write(SCIP_Writer.II());
            ReadII(serialPort.ReadLine());

            serialPort.Write(SCIP_Writer.SCIP2());
            Debug.Log(serialPort.ReadLine()); // ignore echo back
            serialPort.Write(SCIP_Writer.MD(start_step, end_step));
            Debug.Log(serialPort.ReadLine()); // ignore echo back
        }
        catch (Exception ex)
        {
            OnCompleted.Invoke(ex.Message);
            return;
        }

        IsInitialized = string.IsNullOrEmpty(stat);
        OnCompleted.Invoke(stat);
    }

    private void ReadPP(string config)
    {
        var lines = config.Split('\r', '\n', '\0');

        for (var i = 0; i < lines.Length; i++)
        {
            var line = lines[i];

            var sp = line.Split(';');

            var parts = sp[0].Split(':');

            if (parts.Length < 2)
            {
                continue;
            }

            if (parts[0] == "MODL")
            {
                modelName = parts[1];
            }
            else if (parts[0] == "DMIN")
            {
                int.TryParse(parts[1], out MinDistance);
            }
            else if (parts[0] == "DMAX")
            {
                int.TryParse(parts[1], out MaxDistance);
            }
            else if (parts[0] == "ARES")
            {
                int.TryParse(parts[1], out aRes);
            }
            else if (parts[0] == "AMIN")
            {
                int.TryParse(parts[1], out aMin);
            }
            else if (parts[0] == "AMAX")
            {
                int.TryParse(parts[1], out aMax);
            }
            else if (parts[0] == "AFRT")
            {
                int.TryParse(parts[1], out afrt);
            }
            else if (parts[0] == "SCAN")
            {
                int.TryParse(parts[1], out scan);
            }
        }

        IncreaseAngle = 360f / aRes;
        StartAngle = - ((afrt - aMin) + 1) * IncreaseAngle;

        var message = string.Format("Found Hokuyo\nModel: {0}\nDMIN: {1}\nDMAX: {2}\nARES: {3}", modelName, MinDistance, MaxDistance, aRes);
        message = string.Format("{0}\nAMIN: {1}\nAMAX: {2}\nAFRT: {3}", message, aMin, aMax, afrt);
        message = string.Format("{0}\nSCAN: {1}", message, scan);

        Debug.Log(message);
        ServicesLog.Save(message);
    }

    private void ReadII(string config)
    {
        var lines = config.Split('\r', '\n', '\0');
        var value = string.Empty;
        for (var i = 0; i < lines.Length; i++)
        {
            var line = lines[i];

            var sp = line.Split(';');

            var parts = sp[0].Split(':');

            if (parts.Length < 2)
            {
                continue;
            }

            if (parts[0] == "STAT")
            {
                stat = parts[1];
                value = sp[1];
                break;
            }
        }

        var message = string.Format("Hokuyo\nstat: {0}; value: {1}\n", stat, value);

        stat = value == "8" ? string.Empty : stat;

        Debug.Log(message);
        ServicesLog.Save(message);
    }

    public void ReadData()
    {
        try
        {
            Distances.Clear();
            ErrorMessage = string.Empty;
            long time_stamp = 0;
            //for (int i = 0; i < GET_NUM; ++i)
            {
                var receive_data = serialPort.ReadLine();
                if (!SCIP_Reader.MD(receive_data, ref time_stamp, ref Distances))
                {
                    ErrorMessage = receive_data;
                    return;
                }

                if (Distances.Count == 0)
                {
                    ErrorMessage = receive_data;
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            ErrorMessage = ex.Message;
        }
    }

    public void CollectPoint()
    {
        if (Distances.Count < 1)
        {
            return;
        }

        GroupList.Clear();

        var newGroup = new HokuyoDetectGroup();
        for (var i = aMin; i <= aMax; i++)
        {
            var distance = Distances[i];

            if (distance <= MinDistance)
            {
                continue;
            }

            if (distance >= MaxDistance)
            {
                continue;
            }

            if (newGroup.StartStep < 0)
            {
                newGroup.StartStep = i;
                newGroup.EndStep = i;
                newGroup.MinDistance = distance;
                newGroup.MaxDistance = distance;
                newGroup.AverageDistance = distance;
                newGroup.MemberCount = 1;
                continue;
            }

            if (newGroup.EndStep - i > HokuyoDetectGroup.MaxDiffStep ||
                Mathf.Abs(distance - newGroup.AverageDistance) > HokuyoDetectGroup.MaxDiffDistance)
            {
                GroupList.Add(newGroup);

                newGroup = new HokuyoDetectGroup
                {
                    StartStep = i,
                    EndStep = i,
                    MinDistance = distance,
                    MaxDistance = distance,
                    AverageDistance = distance,
                    MemberCount = 1
                };
                continue;
            }

            if (newGroup.MinDistance > distance)
            {
                newGroup.MinDistance = distance;
            }
            else if (newGroup.MaxDistance < distance)
            {
                newGroup.MaxDistance = distance;
            }

            newGroup.AverageDistance = ((newGroup.AverageDistance * newGroup.MemberCount) + distance) / (newGroup.MemberCount + 1);
            newGroup.MemberCount++;
            newGroup.EndStep = i;
        }

        if (newGroup.MemberCount < 1)
        {
            return;
        }

        GroupList.Add(newGroup);
    }

    public Vector2 ConvertToVector2(int step, long distance)
    {
        var angle = StartAngle + IncreaseAngle * (step - aMin);
        var v = (Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.up).normalized * distance;

        return v;
    }

    /*public int GetAvaliableGroupId()
    {
        var i = 0;
        while (GroupList.ContainsKey(i))
        {
            i++;
        }
        return i;
    }*/

    public void Dispose()
    {
        serialPort.Write(SCIP_Writer.QT()); // stop measurement mode
        serialPort.ReadLine(); // ignore echo back
        serialPort.Close();
    }
}

#endif