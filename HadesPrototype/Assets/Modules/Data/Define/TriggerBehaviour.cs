﻿
public enum TriggerBehaviour
{
    WhenEnter,
    WhenInTheArea,
    WhenOutOfArea
}
