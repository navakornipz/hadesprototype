﻿
public enum TriggerType
{
    ActiveObject,
    InActiveObject,
    PlayAnimation,
    ChangeAnimationState,
    CallFunction
}
