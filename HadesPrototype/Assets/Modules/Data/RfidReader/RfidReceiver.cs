﻿using System.Net;
using System.Text;
using UnityEngine;

public class RfidReceiver
{
    private readonly IPAddress ipAddress = IPAddress.Any;
    private const int port = 5333;
    private TCPServer server;

    public string Token { get; private set; }

    public void Init()
    {
        server = new TCPServer();
        server.Init(ipAddress, port, OnRecieveData);
        Token = string.Empty;
    }

    public void Start()
    {
        Token = string.Empty;
        server.Start();
    }

    public void Stop()
    {
        if (server == null)
        {
            return;
        }
        server.Stop();
    }

    private void OnRecieveData(byte[] bytes, int count)
    {
        var json = Encoding.ASCII.GetString(bytes, 0, count);

        var data = JsonUtility.FromJson<RfidData>(json);

        if (data == null || string.IsNullOrEmpty(data.token))
        {
            Debug.LogErrorFormat("can't serialize json: {0}", json);
            return;
        }

        Token = data.token;
        server.Stop();
    }
}
