﻿using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using UnityEngine;

public static class AppDataExample
{
    private const string configFileName = "ApplicationConfig.txt";
    public static int SelectedLanguageIndex { get; private set; }   // 0 thai, 1 english
    public static IPAddress MainScreenIPAddress = IPAddress.Loopback;
    public static int Port = 5001;

    public static void Load()
    {
        LoadAppConfig();
        LoadLocalizeText();
    }

    public static void LoadAppConfig()
    {
        var filePath = string.Format("{0}/{1}", Application.streamingAssetsPath, configFileName);
        if (!File.Exists(filePath))
        {
            Debug.LogErrorFormat("config file not found: {0}", filePath);
            return;
        }

        var sr = new StreamReader(filePath);
        var fileContents = sr.ReadToEnd();
        sr.Close();

        var lines = fileContents.Split('\r', '\n');
        for (var i = 0; i < lines.Length; i++)
        {
            var line = lines[i];

            if (line.Length < 1 ||
                line[0] == '#')
            {
                continue;
            }

            var parts = Regex.Matches(line, @"[\""].+?[\""]|[^,]+")
                .Cast<Match>()
                .Select(m => m.Value)
                .ToList();

            if (parts.Count < 2)
            {
                continue;
            }

            if (parts[0] == "MainScreenIpAddress")
            {
                if (string.IsNullOrEmpty(parts[1]))
                {
                    continue;
                }

                if (!IPAddress.TryParse(parts[1], out MainScreenIPAddress))
                {
                    MainScreenIPAddress = IPAddress.Loopback;
                    Debug.LogErrorFormat("IPAddress TryParse failed: {0}", parts[1]);
                }

                continue;
            }
            else if (parts[0] == "Port")
            {
                if (string.IsNullOrEmpty(parts[1]))
                {
                    continue;
                }

                if (!int.TryParse(parts[1], out Port))
                {
                    Port = 5001;
                    Debug.LogErrorFormat("Port TryParse failed: {0}", parts[1]);
                }
            }
        }
    }

    public static void LoadLocalizeText()
    {
        if (LocalizeManager.Instance == null)
        {
            LocalizeManager.Instance = new LocalizeManager();
        }

        //LocalizeManager.Instance.Init(SelectedLanguageIndex, AssetPath.LocalizeData);
    }

    public static void ChangeLanguage(int languageIndex)
    {
        SelectedLanguageIndex = languageIndex;
        LoadLocalizeText();
    }
}