﻿#if USING_CAMERA

using System;

[Serializable]
public class WebcamDeviceData
{
    public string Name;
    public int Width;
    public int Height;
    public int Fps;

    public WebcamDeviceData(string name)
    {
        Name = name;
        Width = 1280;
        Height = 720;
        Fps = 30;
    }
}

#endif