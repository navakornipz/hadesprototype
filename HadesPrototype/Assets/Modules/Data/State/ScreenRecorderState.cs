﻿
public enum ScreenRecorderState
{
    Idle = 0,
    Record,
    End
}
