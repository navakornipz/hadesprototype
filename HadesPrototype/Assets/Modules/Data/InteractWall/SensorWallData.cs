﻿using System;

[Serializable]
public class SensorWallData
{
    public int MinDistanceFromWall;
    public int MaxDistanceFromWall;
}
