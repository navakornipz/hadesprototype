﻿using System.Collections.Generic;
using UnityEngine;

public class DistortionData
{
    private int[] distortionIndices;

    public HashSet<int> AreaOfInterest;
    public Vector2 TL;
    public Vector2 TR;
    public Vector2 BL;
    public Vector2 BR;
    public int Width;
    public int Height;

    public int TotalBuffer
    {
        get
        {
            return distortionIndices.Length;
        }
    }

    public void Init(int sourceWidth, int desTextureWidth, int desTextureHeight, Vector2 tl, Vector2 tr, Vector2 bl, Vector2 br)
    {
        var sdp = sourceWidth / desTextureWidth;

        AreaOfInterest = new HashSet<int>();

        TL = tl;
        TR = tr;
        BL = bl;
        BR = br;
        Width = desTextureWidth;
        Height = desTextureHeight;

        var v1 = (br - bl) / (Width - 1);
        var v2 = (tl - bl) / (Height - 1);
        var v3 = (tr - tl) / (Width - 1);
        var v4 = (tr - br) / (Height - 1);

        var startVector = new Vector2[Width];
        var endVector = new Vector2[Height];

        for (var y = 0; y < Height; y++)
        {
            var x2 = (int)(bl.x + v2.x * y);
            var y2 = (int)(bl.y + v2.y * y);
            startVector[y] = new Vector2(x2, y2);

            x2 = (int)(br.x + v4.x * y);
            y2 = (int)(br.y + v4.y * y);
            endVector[y] = new Vector2(x2, y2);
        }

        distortionIndices = new int[Width * Height];
        var i = 0;
        for (var y = 0; y < Height; y++)
        {
            var start = startVector[y];
            var end = endVector[y];
            var v = (end - start) / (Width - 1);

            for (var x = 0; x < Width; x++)
            {
                var x2 = (int)(start.x + v.x * x);
                var y2 = (int)(start.y + v.y * x);

                var p = (y2 * sourceWidth) + x2;
                distortionIndices[i] = p;

                if (!AreaOfInterest.Contains(p))
                {
                    AreaOfInterest.Add(p);
                }
                i++;
            }
        }
    }

    public int[] GetDistortIndices()
    {
        return distortionIndices;
    }

    public int GetDistortIndex(int index)
    {
        if (distortionIndices == null)
        {
            Debug.LogError("distortionIndices is null.");
            return 0;
        }
        if (distortionIndices.Length <= index)
        {
            Debug.LogErrorFormat("distortionIndices overflow: {0}/{1}.", index, distortionIndices.Length);
            return 0;
        }
        return distortionIndices[index];
    }

    public int GetDistortIndex(int x, int y)
    {
        var index = y * Width + x;
        return GetDistortIndex(index);
    }

    public int GetIndex(int distortIndex)
    {
        var index = -1;
        for (var i = 0; i < distortionIndices.Length; i++)
        {
            if (distortionIndices[i] == distortIndex)
            {
                index = i;
                break;
            }
        }
        return index;
    }
}
