﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SensorWallDataController
{
    private const string filePath = "./SensorWall.dat";
    private static SensorWallData data;

    public static bool IsLoaded { get; private set; }

    public static void Load()
    {
        if (File.Exists(filePath))
        {
            var bf = new BinaryFormatter();
            var file = File.Open(filePath, FileMode.Open);
            data = (SensorWallData)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            data = new SensorWallData();
        }

        IsLoaded = true;
    }

    public static void Save()
    {
        var bf = new BinaryFormatter();
        var file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
    }

    public static void ResetData()
    {
        if (!File.Exists(filePath))
        {
            return;
        }

        File.Delete(filePath);
    }
}
