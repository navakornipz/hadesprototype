﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class InteractWallDataController
{
    private const string filePath = "./InteractWall.dat";
    private static InteractWallData data;

    public static bool IsLoaded { get; private set; }

    public static void Load()
    {
        if (File.Exists(filePath))
        {
            var bf = new BinaryFormatter();
            var file = File.Open(filePath, FileMode.Open);
            data = (InteractWallData)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            data = new InteractWallData();
        }

        IsLoaded = true;
    }

    public static void Save()
    {
        var bf = new BinaryFormatter();
        var file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
    }

    public static void ResetData()
    {
        if (!File.Exists(filePath))
        {
            return;
        }

        File.Delete(filePath);
    }

    /*public static void SetDistortPoint(Vector2[] points)
    {
        var totalPoint = points.Length;
        if (data.points == null)
        {
            data.points = new int[totalPoint][];
        }

        for (var i = 0; i < totalPoint; i++)
        {
            var point = points[i];
            var p = new int[2];
            p[0] = (int)point.x;
            p[1] = (int)point.y;
            data.points[i] = p;
        }
    }

    public static Vector2[] GetDistortPoint()
    {
        if (data == null || data.points == null)
        {
            return null;
        }

        var ps = data.points;
        var totalPoint = ps.Length;
        var points = new Vector2[totalPoint];

        for (var i = 0; i < totalPoint; i++)
        {
            var p = ps[i];
            var point = new Vector2(p[0], p[1]);
            points[i] = point;
        }

        return points;
    }*/

    public static void SetWallDepthBuffer(ushort[] wallDepthBuffer)
    {
        data.wallDepthBuffer = wallDepthBuffer;
    }

    public static ushort[] GetWallDepthBuffer()
    {
        if (data == null || data.wallDepthBuffer == null)
        {
            return null;
        }

        return data.wallDepthBuffer;
    }
}
