﻿using System.Collections.Generic;
using UnityEngine;

public class BlinkAnimation
{
    private const float disappearDuration = 0.75f;
    private const float appearDuration = 2f;

    private List<GameObject> gameObjects;
    private float timer = 0f;
    private bool isActive;

    public bool IsPlaying { get; private set; }

    public BlinkAnimation()
    {
        gameObjects = new List<GameObject>();
        isActive = true;
    }

    public void AddObject(GameObject gameObject)
    {
        gameObjects.Add(gameObject);
    }

    public void Play()
    {
        for (var i = 0; i < gameObjects.Count; i++)
        {
            gameObjects[i].SetActive(true);
        }

        IsPlaying = true;
        isActive = true;
        timer = 0;
    }

    public void Stop()
    {
        for (var i = 0; i < gameObjects.Count; i++)
        {
            gameObjects[i].SetActive(false);
        }

        IsPlaying = false;
    }

    public void Update()
    {
        if (!IsPlaying)
        {
            return;
        }

        timer += Time.deltaTime;

        if (isActive)
        {
            if (timer >= appearDuration)
            {
                for (var i = 0; i < gameObjects.Count; i++)
                {
                    gameObjects[i].SetActive(false);
                }
                timer = 0;
                isActive = false;
            }
            return;
        }

        if (timer < disappearDuration)
        {
            return;
        }

        for (var i = 0; i < gameObjects.Count; i++)
        {
            gameObjects[i].SetActive(true);
        }

        isActive = true;
        timer = 0;
    }
}
