﻿#if VSGIF
using AppAdvisory.VSGIF;
using System.Collections.Generic;
using UnityEngine;

public static class GifMaker
{
    private static Queue<RenderTexture> m_Frames;
    private static List<Texture2D> m_listText = new List<Texture2D>();
    private static List<Sprite> m_sprite = new List<Sprite>();

    public static void CreateGif(System.Action<int, string> onFileSaved, string filePath, List<Texture2D> textures, int width, int height, int delay = 300)
    {
        var m_Frames = new Queue<RenderTexture>();
        for (var i = 0; i < textures.Count; i++)
        {
            var item = new RenderTexture(width, height, 0);
            Graphics.Blit(textures[i], item);
            m_Frames.Enqueue(item);
        }

        var temp = new Texture2D(width, height, TextureFormat.RGB24, true);
        temp.hideFlags = HideFlags.HideAndDontSave;
        temp.wrapMode = TextureWrapMode.Clamp;
        temp.filterMode = FilterMode.Bilinear;
        temp.anisoLevel = 0;

        // Process the frame queue
        var frames = new List<GifFrame>(m_Frames.Count);
        while (m_Frames.Count > 0)
        {
            var m_f = m_Frames.Dequeue();
            var frame = ToGifFrame(m_f, temp);
            frames.Add(frame);

            SetListTexture2D(temp, width, height);
        }

        // Dispose the temporary texture
        Flush(temp);

        var encoder = new GifEncoder(0, 6);
        encoder.SetDelay(delay);
        //encoder.SetWaterMark(m_Watermark);

        var worker = new GIFMaker(System.Threading.ThreadPriority.Highest)
        {
            m_Encoder = encoder,
            m_Frames = frames,
            m_FilePath = filePath,
            m_OnFileSaved = null,
            m_OnFileSaveProgress = null
        };
        worker.m_OnFileSaved = onFileSaved;
        worker.Start();
    }

    private static GifFrame ToGifFrame(RenderTexture source, Texture2D target)
    {
        // TODO: Experiment with Compute Shaders, it may be faster to return data from a ComputeBuffer
        // than ReadPixels

        RenderTexture.active = source;
        target.ReadPixels(new Rect(0, 0, source.width, source.height), 0, 0);
        target.Apply();

        m_listText.Add(target);

        RenderTexture.active = null;

        return new GifFrame() { Width = target.width, Height = target.height, Data = target.GetPixels32() };
    }

    private static void Flush(Object obj)
    {
#if UNITY_EDITOR
        if (Application.isPlaying)
            Object.Destroy(obj);
        else
            Object.DestroyImmediate(obj);
#else
			Object.Destroy(obj);
#endif
    }

    public static void CleanUp()
    {
        if (m_Frames != null)
        {
            while (m_Frames.Count > 0)
            {
                var obj = m_Frames.Dequeue();
                if (obj == null)
                {
                    continue;
                }
                Object.Destroy(obj);
            }
        }
        
        if (m_listText != null)
        {
            for (var i = 0; i < m_listText.Count; i++)
            {
                if (m_listText[i] == null)
                {
                    continue;
                }
                Object.Destroy(m_listText[i]);
            }
            m_listText.Clear();
        }
        
        if (m_sprite == null)
        {
            return;
        }

        for (var i = 0; i < m_sprite.Count; i++)
        {
            var sprite = m_sprite[i];
            if (sprite == null)
            {
                continue;
            }
            if (sprite.texture != null)
            {
                Object.Destroy(sprite.texture);
            }
            Object.Destroy(sprite);
        }
    }

    private static void SetListTexture2D(Texture2D temp, int width, int height)
    {
        var n = new Texture2D(width, height, TextureFormat.RGB24, true);
        n.SetPixels32(temp.GetPixels32());
        n.Apply();
        var _s = Sprite.Create(n, new Rect(0, 0, temp.width, temp.height), new Vector2(0.5f, 0.5f));

        m_listText.Add(n);
        m_sprite.Add(_s);
    }
}
#endif