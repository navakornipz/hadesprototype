﻿using System.IO;
using UnityEngine;

public static class StreamingAssetHelper
{
    public static Texture2D LoadTexture(string filePath)
    {
        var imgData = File.ReadAllBytes(filePath);

        var texture = new Texture2D(2, 2);
        texture.LoadImage(imgData);

        return texture;
    }

    public static Sprite LoadSprite(string filePath)
    {
        var texture = LoadTexture(filePath);
        var imageRect = new Rect(0, 0, texture.width, texture.height);
        var pivot = new Vector2(0.5f, 0.5f);
        var sprite = Sprite.Create(texture, imageRect, pivot);

        return sprite;
    }
}
