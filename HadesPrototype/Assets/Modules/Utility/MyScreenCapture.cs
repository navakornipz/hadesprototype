﻿using UnityEngine;

public static class MyScreenCapture
{
    //have to capture after end of frame
    public static Texture2D GetCaptureScreen(Rect screenRect)
    {
        var fullTex = ScreenCapture.CaptureScreenshotAsTexture();
        var tex = new Texture2D((int)screenRect.width, (int)screenRect.height, TextureFormat.RGB24, false);
        var data = fullTex.GetPixels((int)screenRect.x, (int)screenRect.y, (int)screenRect.width, (int)screenRect.height);
        tex.SetPixels(data);
        tex.Apply();
        Object.Destroy(fullTex);

        return tex;
    }
}
