﻿using System.IO;

public static class FileManager
{
    public static string[] GetAllFiles(string folder, string file)
    {
        var d = new DirectoryInfo(folder);
        var fileInfo = d.GetFiles(file);
        var files = new string[fileInfo.Length];

        for (var i = 0; i < fileInfo.Length; i++)
        {
            files[i] = fileInfo[i].Name;
        }

        return files;
    }
}
