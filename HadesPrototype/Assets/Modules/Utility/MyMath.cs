﻿using UnityEngine;

public static class MyMath
{
    public static bool IsAboveLine(Vector2 lineStart, Vector2 lineEnd, Vector2 point)
    {
        return ((lineEnd.x - lineStart.x) * (point.y - lineStart.y) - (lineEnd.y - lineStart.y) * (point.x - lineStart.x)) > 0;
    }

    public static bool IsPointInRectangle(Vector2 tl, Vector2 tr, Vector2 bl, Vector2 br, Vector2 point)
    {
        var AB = Vect2d(tl, tr);
        var AD = Vect2d(tl, bl);
        var BC = Vect2d(tr, br);
        var CD = Vect2d(bl, br);
        float C1 = -1 * (AB.y * tl.x + AB.x * tl.y);
        float C2 = -1 * (AD.y * tl.x + AD.x * tl.y);
        float C3 = -1 * (BC.y * tr.x + BC.x * tr.y);
        float C4 = -1 * (CD.y * br.x + CD.x * br.y);
        float D1 = (AB.y * point.x + AB.x * point.y) + C1;
        float D2 = (AD.y * point.x + AD.x * point.y) + C2;
        float D3 = (BC.y * point.x + BC.x * point.y) + C3;
        float D4 = (CD.y * point.x + CD.x * point.y) + C4;

        return 0 >= D1 && 0 >= D4 && 0 <= D2 && 0 >= D3;
    }

    private static Vector2 Vect2d(Vector2 p1, Vector2 p2)
    {
        Vector2 temp = new Vector2();
        temp.x = (p2.x - p1.x);
        temp.y = -1 * (p2.y - p1.y);
        return temp;
    }
}
