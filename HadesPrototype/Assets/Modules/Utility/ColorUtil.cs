﻿using UnityEngine;

public static class ColorUtil
{
    public static float Distance(Color color1, Color color2)
    {
        return Vector3.Distance(new Vector3(color1.r, color1.g, color1.b), new Vector3(color2.r, color2.g, color2.b));
    }

    public static bool IsClose(Color color1, Color color2, float range)
    {
        return Mathf.Abs(color1.r - color2.r) < range && Mathf.Abs(color1.g - color2.g) < range && Mathf.Abs(color1.b - color2.b) < range;
    }
}