﻿using UnityEngine;

public static class GlobalResources
{
    private static LineRenderer2D lineRenderer2DPrefab;

    public static LineRenderer2D GetLineRenderer2DPrefab()
    {
        if (lineRenderer2DPrefab == null)
        {
            lineRenderer2DPrefab = Resources.Load<LineRenderer2D>(GlobalPath.LineRenderer2DPath);
        }

        return lineRenderer2DPrefab;
    }
}
