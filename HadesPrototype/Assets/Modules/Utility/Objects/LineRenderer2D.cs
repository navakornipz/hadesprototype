﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class LineRenderer2D : MonoBehaviour
{
    private Image lineImage;

    public void Init(Vector2 start, Vector2 end, Color color, int lineThin)
    {
        if (lineImage == null)
        {
            lineImage = GetComponent<Image>();
        }

        var delta = end - start;
        var angle = Vector3.Angle(Vector3.right, delta);
        var length = delta.magnitude;

        if (end.y < start.y)
        {
            angle = -angle;
        }

        lineImage.color = color;
        lineImage.rectTransform.anchoredPosition = start;
        lineImage.rectTransform.sizeDelta = new Vector2(length, lineThin);
        lineImage.rectTransform.rotation = Quaternion.identity;
        lineImage.rectTransform.Rotate(new Vector3(0, 0, angle));
    }
}
