﻿using UnityEngine;

public class Attacher : MonoBehaviour
{
    [SerializeField] private Transform attacher;
    [SerializeField] private Transform attaching;

    private Vector3 lastPosition;
    private Vector3 attachingLastPosition;
    [SerializeField] private Vector3 offset;

    private void Start()
    {
        lastPosition = transform.position;
    }

    private void LateUpdate()
    {
        if (attaching != null)
        {
            transform.position = attaching.transform.position + offset;
        }

        if (attacher == null)
        {
            return;
        }

        var deltaPosition = transform.position - lastPosition;

        attacher.position += deltaPosition;
        lastPosition = transform.position;
    }

    public void Attach(Transform attacher)
    {
        this.attacher = attacher;
        lastPosition = transform.position;
    }

    public void AttachTo(Transform attaching, Vector3 offset)
    {
        this.attaching = attaching;
        if (attaching == null)
        {
            return;
        }

        this.offset = offset;
        transform.position = attaching.position + offset;
        attachingLastPosition = attaching.position;

        if (attacher == null)
        {
            return;
        }

        var deltaPosition = transform.position - lastPosition;

        attacher.position += deltaPosition;
        lastPosition = transform.position;
    }
}
