﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class PedestalController : MonoBehaviour
{
    private const float rotateSpeed = 90f;

    [SerializeField] private Transform pedestalTf;
    [SerializeField] private float dragPower = 0.5f;
    [SerializeField] private float swipePower = 0.5f;
    [SerializeField] private float zoomPower = 0.5f;
    [SerializeField] private float minScale = 0.5f;
    [SerializeField] private float maxScale = 3f;
    [SerializeField] private bool FreezeX;

    [HideInInspector] public bool IsReady;

    private Vector3 originPosition;
    private Quaternion originRotation;
    private Vector3 originScale;

    public static PedestalController Instance;

    private void Awake()
    {
        Instance = this;

        if (pedestalTf == null)
        {
            return;
        }

        originPosition = pedestalTf.localPosition;
        originRotation = pedestalTf.localRotation;
        originScale = pedestalTf.localScale;
    }

    private void Start()
    {
        InputManager.Instance.AddTouchBeginEvent(OnTouchStart);
        InputManager.Instance.AddTouchMoveEvent(OnTouchMove);
        InputManager.Instance.AddSwipeEvent(OnSwipe);
        InputManager.Instance.AddZoomEvent(OnZoom);
    }

    private void OnTouchStart()
    {
        StopAllCoroutines();
    }

    private void OnTouchMove(Vector2 deltaPosition)
    {
        if (!IsReady)
        {
            return;
        }

        if (EventSystem.current.IsPointerOverGameObject(0) || EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (FreezeX)
        {
            var dist = deltaPosition.x;
            pedestalTf.Rotate(transform.up * dist * dragPower);
            return;
        }

        if (Vector3.Dot(transform.up, Vector3.up) >= 0)
        {
            pedestalTf.Rotate(transform.up, Vector3.Dot(deltaPosition, Camera.main.transform.right) * dragPower, Space.World);
        }
        else
        {
            pedestalTf.Rotate(transform.up, -Vector3.Dot(deltaPosition, Camera.main.transform.right) * dragPower, Space.World);
        }

        pedestalTf.Rotate(Camera.main.transform.right, -Vector3.Dot(deltaPosition, Camera.main.transform.up) * dragPower, Space.World);
        //Debug.LogErrorFormat("deltaPosition {0}", deltaPosition);
    }

    private void OnSwipe(float time, Vector2 deltaPosition)
    {
        if (!IsReady)
        {
            return;
        }

        StopAllCoroutines();
        StartCoroutine(SwipeProcess(time, deltaPosition.x));
    }

    private void OnZoom(float distance)
    {
        var delta = distance * zoomPower;
        pedestalTf.localScale += Vector3.one * delta;

        if (pedestalTf.localScale.x > maxScale)
        {
            pedestalTf.localScale = Vector3.one * maxScale;
        }
        else if (pedestalTf.localScale.x < minScale)
        {
            pedestalTf.localScale = Vector3.one * minScale;
        }
    }

    private IEnumerator SwipeProcess(float time, float distance)
    {
        var force = (-distance / time) * swipePower;
        while (Mathf.Abs(force) > 1f)
        {
            pedestalTf.Rotate(transform.up * force * Time.deltaTime);
            force *= 0.95f;
            yield return null;
        }
    }

    public void SetTarget(Transform tf)
    {
        pedestalTf = tf;
        originPosition = pedestalTf.localPosition;
        originRotation = pedestalTf.localRotation;
        originScale = pedestalTf.localScale;
    }

    public void Reset()
    {
        pedestalTf.localPosition = originPosition;
        pedestalTf.localRotation = originRotation;
        pedestalTf.localScale = originScale;
    }
}
