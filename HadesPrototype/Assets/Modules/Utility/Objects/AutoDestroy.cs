﻿using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    [SerializeField] private float lifeTime;
    [SerializeField] private bool destroyOnAwake;

    private float timer;

    private void Awake()
    {
        if (!destroyOnAwake)
        {
            return;
        }
        Destroy(gameObject);
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer < lifeTime)
        {
            return;
        }

        Destroy(gameObject);
    }
}
