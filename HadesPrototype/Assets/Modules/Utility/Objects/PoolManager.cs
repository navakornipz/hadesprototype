﻿using System.Collections.Generic;
using UnityEngine;

public class PoolManager<T> where T : MonoBehaviour
{
    private T[] prefabs;
    private Queue<T> objects;
    private Transform parent;

    public List<T> AliveList;

    public void Init(T[] objectPrefabs, Transform parent, int count)
    {
        prefabs = objectPrefabs;
        objects = new Queue<T>();
        this.parent = parent;

        for (var i = 0; i < count; i++)
        {
            AddNewObject();
        }

        AliveList = new List<T>();
    }

    public void Clear()
    {
        if (objects == null)
        {
            return;
        }

        while (objects.Count > 0)
        {
            var obj = objects.Dequeue();
            Object.Destroy(obj.gameObject);
        }
    }

    public void ReturnAll()
    {
        if (AliveList.Count < 1)
        {
            return;
        }

        for (var i = 0; i < AliveList.Count; i++)
        {
            var obj = AliveList[i];
            obj.transform.SetParent(parent);
            obj.gameObject.SetActive(false);
            objects.Enqueue(obj);
        }

        AliveList.Clear();
    }

    public T Get(Transform appearance)
    {
        if (objects.Count <= 0)
        {
            AddNewObject();
        }

        var obj = objects.Dequeue();
        obj.gameObject.SetActive(true);
        obj.transform.SetParent(appearance);
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localRotation = Quaternion.identity;
        obj.transform.localScale = Vector3.one;

        AliveList.Add(obj);

        return obj;
    }

    public void Return(T obj)
    {
        obj.transform.SetParent(parent);
        obj.gameObject.SetActive(false);
        objects.Enqueue(obj);
        AliveList.Remove(obj);
    }

    private void AddNewObject()
    {
        if (prefabs == null || prefabs.Length < 1)
        {
            Debug.LogError("Pool Manager: prefabs is null or empty!");
            return;
        }

        var index = Random.Range(0, prefabs.Length);
        if (prefabs[index] == null)
        {
            Debug.LogErrorFormat("Pool Manager: prefabs index {0} is null!", index);
            return;
        }

        var tobj = Object.Instantiate(prefabs[index], parent);
        tobj.gameObject.SetActive(false);

        var obj = tobj.gameObject.GetComponent<T>();
        if (obj == null)
        {
            Debug.LogError("Pool Manager: component in prefab not found!");
            return;
        }
        objects.Enqueue(obj);
    }
}
