﻿using UnityEngine;

public class LimitedJointController : MonoBehaviour
{
    [SerializeField] private float limitedAngle;
    [SerializeField] private Vector3 forceOrigin;

    private Quaternion originRotation;
    private Quaternion lastRotation;

    public bool Active;

    private void Awake()
    {
        if (forceOrigin.sqrMagnitude <= float.Epsilon)
        {
            originRotation = transform.localRotation;
            return;
        }

        originRotation = Quaternion.Euler(forceOrigin);
        lastRotation = originRotation;

        Active = false;
    }

    private void LateUpdate()
    {
        if (!Active)
        {
            return;
        }

        transform.localRotation = LimitedRotation(transform.localRotation, originRotation, limitedAngle);
        lastRotation = transform.localRotation;
    }

    private Quaternion LimitedRotation(Quaternion current, Quaternion origin, float limitedAngle)
    {
        var angle = Quaternion.Angle(current, origin);
        if (Mathf.Abs(angle) < limitedAngle)
        {
            return current;
        }

        return lastRotation;
    }
}
