﻿using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool
{
    private GameObject[] prefabs;
    private Queue<GameObject> objects;
    private Transform parent;

    public List<GameObject> AliveList;

    public void Init(GameObject[] objectPrefabs, Transform parent, int count)
    {
        prefabs = objectPrefabs;
        objects = new Queue<GameObject>();
        this.parent = parent;

        for (var i = 0; i < count; i++)
        {
            AddNewObject();
        }

        AliveList = new List<GameObject>();
    }

    public void Clear()
    {
        if (objects == null)
        {
            return;
        }

        while (objects.Count > 0)
        {
            var gobj = objects.Dequeue();
            Object.Destroy(gobj);
        }
    }

    public GameObject Get(Transform appearance)
    {
        if (objects.Count <= 0)
        {
            AddNewObject();
        }

        var obj = objects.Dequeue();
        obj.SetActive(true);
        obj.transform.parent = appearance;
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localRotation = Quaternion.identity;
        obj.transform.localScale = Vector3.one;

        AliveList.Add(obj);

        return obj;
    }

    public void Return(GameObject obj)
    {
        if (objects.Contains(obj))
        {
            return;
        }

        obj.transform.SetParent(parent);
        obj.SetActive(false);
        objects.Enqueue(obj);
        AliveList.Remove(obj);
    }

    private void AddNewObject()
    {
        if (prefabs == null || prefabs.Length < 1)
        {
            Debug.LogError("Pool Manager: prefabs is null or empty!");
            return;
        }

        var index = Random.Range(0, prefabs.Length);
        if (prefabs[index] == null)
        {
            Debug.LogErrorFormat("Pool Manager: prefabs index {0} is null!", index);
            return;
        }

        var gobj = Object.Instantiate(prefabs[index], parent);
        gobj.SetActive(false);

        objects.Enqueue(gobj);
    }
}
