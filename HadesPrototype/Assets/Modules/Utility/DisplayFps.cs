﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class DisplayFps : MonoBehaviour
{
    private const int frameSkip = 12;

    private Text fpsText;
    private float deltaTime;
    private int frameCount;

    private float timer;
    private float refresh;
    private float avgFramerate;

    private void Awake()
    {
        fpsText = GetComponentInChildren<Text>();
    }

    private void Update()
    {
        var timelapse = Time.smoothDeltaTime;
        timer = timer <= 0 ? refresh : timer -= timelapse;

        if (timer <= 0)
        {
            avgFramerate = (int)(1f / timelapse);
        }

        if (frameCount < frameSkip)
        {
            frameCount++;
            return;
        }

        frameCount -= frameSkip;
        fpsText.text = avgFramerate.ToString();
    }
}
