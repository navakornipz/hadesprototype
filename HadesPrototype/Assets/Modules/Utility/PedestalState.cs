﻿public enum PedestalState
{
    Idle,
    RotateLeft,
    RotateRight
}
