﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenRecorder : MonoBehaviour
{
    [HideInInspector] public List<Texture2D> Frames;

    [HideInInspector] public bool IsRecording;

    public static ScreenRecorder Instance;

    private void Awake()
    {
        Instance = this;
        Frames = new List<Texture2D>();
    }
    
    public void Record(float fps, Rect screenRect, float duration, Action onRecordingCompleted)
    {
        IsRecording = true;
        ClearTextures();
        StartCoroutine(Recording(fps, screenRect, duration, onRecordingCompleted));
    }

    private IEnumerator Recording(float fps, Rect screenRect, float duration, Action onRecordingCompleted)
    {
        var delay = 1f / fps;
        var timer = 0f;
        var frameTimer = delay;

        var renderTexture = new RenderTexture(720, 1280, -1);
        RenderTexture.active = renderTexture;

        while (timer < duration)
        {
            timer += Time.deltaTime;
            frameTimer += Time.deltaTime;

            if (frameTimer >= delay)
            {
                yield return new WaitForEndOfFrame();
                var newFrame = GetCaptureScreen(screenRect);
                Frames.Add(newFrame);
                frameTimer -= delay;
            }
            yield return null;
        }

        IsRecording = false;
        onRecordingCompleted.Invoke();
        renderTexture.Release();
        Destroy(renderTexture);
    }

    private void ClearTextures()
    {
        if (Frames == null ||
            Frames.Count < 1)
        {
            return;
        }

        for (var i = 0; i < Frames.Count; i++)
        {
            Destroy(Frames[i]);
        }
        Frames.Clear();
    }

    private Texture2D GetCaptureScreen(Rect screenRect)
    {
        var fullTex = ScreenCapture.CaptureScreenshotAsTexture();
        var tex = new Texture2D((int)screenRect.width, (int)screenRect.height, TextureFormat.RGB24, false);
        var data = fullTex.GetPixels((int)screenRect.x, (int)screenRect.y, (int)screenRect.width, (int)screenRect.height);
        tex.SetPixels(data);
        tex.Apply();
        Destroy(fullTex);

        return tex;
    }
}
