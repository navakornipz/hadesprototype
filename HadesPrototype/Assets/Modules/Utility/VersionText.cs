﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VersionText : MonoBehaviour
{
	private void Awake ()
    {
        var t = GetComponentInChildren<Text>(true);

        if (t != null)
        {
            t.text = string.Format("v{0}", Application.version);
        }
        else
        {
            var tmpro = GetComponentInChildren<TextMeshProUGUI>(true);
            tmpro.text = string.Format("v{0}", Application.version);
        }

        Destroy(this);
	}
}
