﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerListener : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    [HideInInspector] public int TouchId;
    [HideInInspector] public List<int> TouchIdList;

    public bool Pressed { get; private set; } = false;

    private void Awake()
    {
        TouchIdList = new List<int>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        TouchId = eventData.pointerId;

        if (!TouchIdList.Contains(eventData.pointerId))
        {
            TouchIdList.Add(eventData.pointerId);
        }

        Pressed = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        TouchIdList.Remove(eventData.pointerId);
        Pressed = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        TouchIdList.Remove(eventData.pointerId);
        Pressed = false;
    }
}
