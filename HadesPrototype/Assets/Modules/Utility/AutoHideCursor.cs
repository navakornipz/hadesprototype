﻿using UnityEngine;

public static class AutoHideCursor
{
    private static Vector3 lastMousePosition;
    private static float mouseMoveTimer;

    public static void Process()
    {
        if (Cursor.visible)
        {
            if ((Input.mousePosition - lastMousePosition).sqrMagnitude < float.Epsilon)
            {
                mouseMoveTimer += Time.deltaTime;

                if (mouseMoveTimer > 1f)
                {
                    Cursor.visible = false;
                }
            }
            else
            {
                mouseMoveTimer = 0;
            }

            lastMousePosition = Input.mousePosition;
        }
        else
        {
            if (Input.GetMouseButton(0))
            {
                Cursor.visible = true;
            }
        }
    }
}
