﻿#if USING_KINECT
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HandInput
{
    private readonly Vector2Int targetScreen = new Vector2Int(1920, 1080);

    private Dictionary<long, HandInputData[]> data;

    public void Init()
    {
        data = new Dictionary<long, HandInputData[]>();
    }

    public void Update()
    {
        if (KinectManager.Instance == null)
        {
            return;
        }

        var totalUser = KinectManager.Instance.GetUsersCount();

        if (totalUser < 1)
        {
            return;
        }

        var trackedUsers = KinectManager.Instance.GetAllUserIds();
        for (var i = data.Count - 1; i >= 0; i--)
        {
            var user = data.ElementAt(i);
            if (trackedUsers.Contains(user.Key))
            {
                continue;
            }

            data.Remove(user.Key);
        }

        for (var i = 0; i < trackedUsers.Count; i++)
        {
            var userId = trackedUsers[i];
            var newPosL = Vector2.zero;
            var newPosR = Vector2.zero;

            if (!data.ContainsKey(userId))
            {
                data.Add(userId, new HandInputData[] { new HandInputData(), new HandInputData()});
            }

            var value = data[userId];
            var lvalue = value[InputManager.LeftHand];
            var rvalue = value[InputManager.RightHand];

            var handLeft = (int)KinectInterop.JointType.HandLeft;
            if (KinectManager.Instance.IsJointTracked(userId, handLeft))
            {
                var newPos = KinectManager.Instance.GetJointPosColorOverlay(userId, handLeft, new Rect(0, 0, targetScreen.x, targetScreen.y));
                if (lvalue.Force < 0)
                {
                    lvalue.Force = 0;
                }
                else
                {
                    var deltaPos = newPos - lvalue.LastPosition;
                    lvalue.Direction = deltaPos.normalized;
                    lvalue.Force = deltaPos.sqrMagnitude;
                }

                lvalue.LastPosition = newPos;
            }
            else
            {
                lvalue.Force = -1;
            }

            var handRight = (int)KinectInterop.JointType.HandRight;
            if (KinectManager.Instance.IsJointTracked(userId, handRight))
            {
                var newPos = KinectManager.Instance.GetJointPosColorOverlay(userId, handRight, new Rect(0, 0, targetScreen.x, targetScreen.y));
                if (rvalue.Force < 0)
                {
                    rvalue.Force = 0;
                }
                else
                {
                    var deltaPos = newPos - rvalue.LastPosition;
                    rvalue.Direction = deltaPos.normalized;
                    rvalue.Force = deltaPos.sqrMagnitude;
                }

                rvalue.LastPosition = newPos;
            }
            else
            {
                rvalue.Force = -1;
            }

            value[InputManager.LeftHand] = lvalue;
            value[InputManager.RightHand] = rvalue;
            data[userId] = value;
        }
    }

    public List<HandInputData[]> GetHandInputData()
    {
        return data.Values == null ? null : data.Values.ToList();
    }

    public Dictionary<long, HandInputData[]> GetHandInputDataWithUser()
    {
        return data;
    }
}

#endif