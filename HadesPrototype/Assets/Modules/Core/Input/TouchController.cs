﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TouchController
{
    public delegate void OnBegin();
    public delegate void OnMove(Vector2 deltaPosition);
    public delegate void OnTap(Vector2 position);
    public delegate void OnSwipe(float time, Vector2 deltaPosition);
    public delegate void OnZoom(float distance);

    private const float tapTime = 1f;
    private const float tapDistance = 10f;
    private const float tapDistanceSqr = tapDistance * tapDistance;
    private const float swipeTime = 1f / 3f;
    private const float swipeDistance = 50f;
    private const float swipeDistanceSqr = swipeDistance * swipeDistance;

    private Vector2[] beginTouchPosition;
    private Vector2[] lastTouchPosition;
    private float lastDistance;

    private List<OnBegin> onBeginList;
    private List<OnMove> onMoveList;
    private List<OnTap> onTapList;
    private List<OnSwipe> onSwipeList;
    private List<OnZoom> onZoomList;

    private float startTime;

    public void Init()
    {
        beginTouchPosition = new Vector2[InputManager.MaxTouch];
        lastTouchPosition = new Vector2[InputManager.MaxTouch];

        onBeginList = new List<OnBegin>();
        onMoveList = new List<OnMove>();
        onTapList = new List<OnTap>();
        onSwipeList = new List<OnSwipe>();
        onZoomList = new List<OnZoom>();
    }

    public void Update()
    {
        var touchCount = 0;

#if UNITY_EDITOR
        if (Mathf.Abs(Input.mouseScrollDelta.y) > float.Epsilon)
        {
            touchCount = 2;
        }
        else if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0))
        {
            touchCount = 1;
        }
#else
        touchCount = Input.touchCount;
#endif

        switch (touchCount)
        {
            case 0:
                return;
            case 1:
                SingleTouchEvent();
                return;
            case 2:
                MultiTouchEvent();
                return;
        }
    }

    private void MultiTouchEvent()
    {
        var distance = 0f;
#if UNITY_EDITOR
        distance = Input.mouseScrollDelta.y;
#else
        var touch1 = Input.GetTouch(0);
        var touch2 = Input.GetTouch(1);

        switch (touch1.phase)
        {
            case TouchPhase.Began:
                {
                    beginTouchPosition[0] = touch1.position;
                    beginTouchPosition[1] = touch2.position;
                    lastDistance = Vector2.Distance(touch1.position, touch2.position);
                    break;
                }
            case TouchPhase.Moved:
                {
                    var newDistance = Vector2.Distance(touch1.position, touch2.position);
                    distance = newDistance - lastDistance;
                    lastDistance = newDistance;
                    break;
                }
            case TouchPhase.Canceled:
            case TouchPhase.Ended:
                {
                    break;
                }
        }
#endif

        if (Mathf.Abs(distance) <= float.Epsilon)
        {
            return;
        }

        for (var i = 0; i < onZoomList.Count; i++)
        {
            onZoomList[i].Invoke(distance);
        }
    }

    private void SingleTouchEvent()
    {
#if UNITY_EDITOR
        var touch = new Touch();
        touch.position = Input.mousePosition;
        touch.phase = TouchPhase.Stationary;
        if (Input.GetMouseButtonDown(0))
        {
            touch.phase = TouchPhase.Began;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            touch.phase = TouchPhase.Ended;
        }
        else if (Input.GetMouseButton(0))
        {
            touch.phase = TouchPhase.Moved;
        }
#else
        if (Input.touchCount < 1)
        {
            return;
        }
        var touch = Input.GetTouch(0);
#endif

        switch (touch.phase)
        {
            case TouchPhase.Began:
                startTime = Time.time;
                beginTouchPosition[0] = touch.position;
                for (var i = 0; i < onBeginList.Count; i++)
                {
                    onBeginList[i].Invoke();
                }
                break;
            case TouchPhase.Moved:
                var deltaPosition = lastTouchPosition[0] - touch.position;
                for (var i = 0; i < onMoveList.Count; i++)
                {
                    onMoveList[i].Invoke(deltaPosition);
                }
                break;
            case TouchPhase.Ended:
            case TouchPhase.Canceled:
                var totalTime = Time.time - startTime;
                var deltaPos = touch.position - beginTouchPosition[0];
                var distanceSqr = deltaPos.sqrMagnitude;

                if (totalTime < tapTime && distanceSqr < tapDistanceSqr)
                {
                    for (var i = 0; i < onTapList.Count; i++)
                    {
                        onTapList[i].Invoke(touch.position);
                    }
                }
                else if (totalTime <= swipeTime && distanceSqr > swipeDistanceSqr)
                {
                    for (var i = 0; i < onSwipeList.Count; i++)
                    {
                        onSwipeList[i].Invoke(totalTime, deltaPos);
                    }
                }
                break;
        }

        lastTouchPosition[0] = touch.position;
    }

    public void AddTouchBeginEvent(OnBegin callback)
    {
        if (onBeginList.Contains(callback))
        {
            return;
        }

        onBeginList.Add(callback);
    }

    public void RemoveTouchBeginEvent(OnBegin callback)
    {
        onBeginList.Remove(callback);
    }

    public void AddTouchMoveEvent(OnMove callback)
    {
        if (onMoveList.Contains(callback))
        {
            return;
        }

        onMoveList.Add(callback);
    }

    public void RemoveTouchBeginEvent(OnMove callback)
    {
        onMoveList.Remove(callback);
    }

    public void AddTapEvent(OnTap callback)
    {
        if (onTapList.Contains(callback))
        {
            return;
        }

        onTapList.Add(callback);
    }

    public void RemoveTapEvent(OnTap callback)
    {
        onTapList.Remove(callback);
    }

    public void AddSwipeEvent(OnSwipe callback)
    {
        if (onSwipeList.Contains(callback))
        {
            return;
        }

        onSwipeList.Add(callback);
    }

    public void RemoveSwipeEvent(OnSwipe callback)
    {
        onSwipeList.Remove(callback);
    }

    public void AddZoomEvent(OnZoom callback)
    {
        if (onZoomList.Contains(callback))
        {
            return;
        }

        onZoomList.Add(callback);
    }

    public void RemoveZoomEvent(OnZoom callback)
    {
        onZoomList.Remove(callback);
    }
}
