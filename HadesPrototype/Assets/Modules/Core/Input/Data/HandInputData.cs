﻿using UnityEngine;

public class HandInputData
{
    public Vector2 LastPosition;
    public Vector2 Position;
    public Vector2 Direction;
    public float Force;

    public HandInputData()
    {
        Force = -1;
    }
}
