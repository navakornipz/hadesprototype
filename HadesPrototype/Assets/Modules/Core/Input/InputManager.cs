﻿using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    #region Define

    public const int MaxTouch = 1;
    public const int LeftHand = 0;
    public const int RightHand = 1;

    #endregion

    #region Serialize Field

    [SerializeField] private bool touchEnable = false;
    [SerializeField] private bool handInputEnable = false;

    #endregion

    #region Members

    private TouchController touchController;

#if USING_KINECT
    private HandInput handInput;
#endif

    [HideInInspector] public bool IsActive;

#endregion

#region Properties

    public Queue<string> InputString;

    public static InputManager Instance;

#endregion

#region Unity Callback

    private void Awake()
    {
        Instance = this;
        Input.multiTouchEnabled = MaxTouch > 1;

        if (touchEnable)
        {
            touchController = new TouchController();
            touchController.Init();
        }

#if USING_KINECT
        if (handInputEnable)
        {
            handInput = new HandInput();
            handInput.Init();
        }
#endif

        InputString = new Queue<string>();
    }

    private void Update()
    {
        if (!IsActive)
        {
            return;
        }

        if (touchEnable)
        {
            touchController.Update();
        }

#if USING_KINECT
        if (handInputEnable)
        {
            handInput.Update();
        }
#endif
    }

    #endregion

#region Command

    public void AddTouchBeginEvent(TouchController.OnBegin callback)
    {
        touchController.AddTouchBeginEvent(callback);
    }

    public void AddTouchMoveEvent(TouchController.OnMove callback)
    {
        touchController.AddTouchMoveEvent(callback);
    }

    public void AddSwipeEvent(TouchController.OnSwipe callback)
    {
        touchController.AddSwipeEvent(callback);
    }

    public void AddTapEvent(TouchController.OnTap callback)
    {
        touchController.AddTapEvent(callback);
    }

    public void RemoveTapEvent(TouchController.OnTap callback)
    {
        touchController.RemoveTapEvent(callback);
    }

    public void AddZoomEvent(TouchController.OnZoom callback)
    {
        touchController.AddZoomEvent(callback);
    }

    public void RemoveZoomEvent(TouchController.OnZoom callback)
    {
        touchController.RemoveZoomEvent(callback);
    }

#if USING_KINECT
    public List<HandInputData[]> GetHandInputData()
    {
        return handInput.GetHandInputData();
    }

    public Dictionary<long, HandInputData[]> GetHandInputDataWithUser()
    {
        return handInput.GetHandInputDataWithUser();
    }
#endif

    #endregion

    #region Event

    public void OnVirtualKeyboard(string name)
    {
        InputString.Enqueue(name);
    }

#endregion
}
