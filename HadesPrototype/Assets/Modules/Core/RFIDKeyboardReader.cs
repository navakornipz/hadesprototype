﻿using System.Collections.Generic;
using UnityEngine;

public class RFIDKeyboardReader : MonoBehaviour
{
    private const float resetTime = 0.1f;
    private const int minLength = 8;
    private const int maxLength = 12;

    public string currentId;

    private float lastTime;

    public Queue<string> RFIDQueue;

    public static RFIDKeyboardReader Instance;

    private void Awake()
    {
        Instance = this;
        lastTime = Time.time;
        currentId = string.Empty;
        RFIDQueue = new Queue<string>();
    }

    private void Update()
    {
        if (!enabled)
        {
            return;
        }

        var currentTime = Time.time;
        var deltaTime = currentTime - lastTime;
        if (deltaTime > resetTime)
        {
            lastTime = currentTime;
            currentId = string.Empty;
        }

        if (Input.inputString.Length < 1)
        {
            return;
        }

        foreach (var c in Input.inputString)
        {
            if (c == '\r' || c == '\n')
            {
                if (currentId.Length > 0 &&
                    currentId.Length >= minLength &&
                    currentId.Length <= maxLength)
                {
                    RFIDQueue.Enqueue(currentId);
                }
                currentId = string.Empty;
                break;
            }

            currentId += c;
        }

        lastTime = currentTime;
    }
}
