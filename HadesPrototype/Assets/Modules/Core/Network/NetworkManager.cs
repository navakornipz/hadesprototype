﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{
    #region Members

    private UdpClient sender;
    private UdpClient receiver;

    private string hostAddress = "172.16.140.47";
    private int port = 19784;

    public bool IsMasterClient;
        
    private IPEndPoint receiveIPGroup;

    #endregion // Memebers

    #region Unity Callback

    private void Start()
    {
        //sender = new UdpClient(port, AddressFamily.InterNetwork);
        //var groupEP = new IPEndPoint(IsMasterClient ? IPAddress.Broadcast : IPAddress.Parse(hostAddress), port);
        //sender.Connect(groupEP);

        StartReceivingIP();
    }

    #endregion // Unity Callback

    #region Command

    public void SendData(string data)
    {
        string customMessage = data;

        if (customMessage != "")
        {
            sender.Send(Encoding.ASCII.GetBytes(customMessage), customMessage.Length);
        }
    }

    public void StartReceivingIP()
    {
        try
        {
            if (receiver == null)
            {
                receiver = new UdpClient(port);
                receiver.BeginReceive(new AsyncCallback(ReceiveData), null);
                Debug.LogError("setup");
            }
        }
        catch (SocketException e)
        {
            Debug.Log(e.Message);
        }
    }

    private void ReceiveData(IAsyncResult result)
    {
        Debug.LogError("recieve something");
        receiveIPGroup = new IPEndPoint(IPAddress.Any, port);
        byte[] received;
        if (receiver != null)
        {
            received = receiver.EndReceive(result, ref receiveIPGroup);
        }
        else
        {
            return;
        }
        receiver.BeginReceive(new AsyncCallback(ReceiveData), null);
        var receivedString = Encoding.ASCII.GetString(received);

        //TODO: recieve data and do something
        Debug.LogErrorFormat("recieve data: {0}", receivedString);
    }

    #endregion // Command
}
