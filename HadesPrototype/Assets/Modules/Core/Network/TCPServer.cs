﻿using System;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using UnityEngine;

public class TCPServer
{
    private Thread listenerThread;
    private TcpListener server;

    private IPAddress ipAddress;
    private int port;

    private Action<byte[], int> onRecieveData;

    public bool IsReady { get; private set; }

    public void Init(IPAddress ipAddress, int port, Action<byte[], int> onRecieveData)
    {
        IsReady = false;

        this.ipAddress = ipAddress;
        this.port = port;
        this.onRecieveData = onRecieveData;

        try
        {
            server = new TcpListener(ipAddress, port);
        }
        catch (SocketException e)
        {
            Debug.LogErrorFormat("SocketException: {0}", e.Message);
        }
    }

    public void Start()
    {
        if (server == null)
        {
            Debug.LogError("can't start server because server is null.");
            return;
        }

        if (listenerThread == null)
        {
            var ts = new ThreadStart(ListenerProcess);
            listenerThread = new Thread(ts);
        }
        listenerThread.Start();
    }

    public void Stop()
    {
        if (listenerThread != null)
        {
            listenerThread.Abort();
            listenerThread.Join();
            listenerThread = null;
        }
        
        if (server == null)
        {
            return;
        }
        server.Stop();
    }

    private void ListenerProcess()
    {
        try
        {
            // Start listening for client requests.
            server.Start();

            IsReady = true;

            // Buffer for reading data
            var bytes = new byte[256];

            // Enter the listening loop.
            while (true)
            {
                Thread.Sleep(10);

                Debug.Log("Waiting for a connection... ");

                // Perform a blocking call to accept requests.
                // You could also user server.AcceptSocket() here.
                var client = server.AcceptTcpClient();
                if (client == null)
                {
                    continue;
                }

                // Get a stream object for reading and writing
                var stream = client.GetStream();
                var swriter = new StreamWriter(stream);
                var i = 0;

                // Loop to receive all the data sent by the client.
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    onRecieveData(bytes, i);
                    if (client == null)
                    {
                        continue;
                    }
                    SendData(client.Client, new byte[] { 1 });
                }

                // Shutdown and end connection
                client.Close();
            }
        }
        catch (Exception ex)
        {
            if (ex is ThreadAbortException)
            {
                Console.WriteLine("ThreadAbortException: {0}", ex.Message);
                Thread.ResetAbort();
            }
            else if (ex is SocketException)
            {
                Debug.LogErrorFormat("SocketException: {0}", ex.Message);
            }
        }
        finally
        {
            server.Stop();
        }
    }

    public void SendData(Socket socket, byte[] data)
    {
        if (socket == null || !socket.Connected)
        {
            return;
        }

        var socketAsyncData = new SocketAsyncEventArgs();
        socketAsyncData.SetBuffer(data, 0, data.Length);
        socket.SendAsync(socketAsyncData);
    }
}