﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public static class UDPServer
{
    #region Members

    public static Dictionary<IPAddress, IPEndPoint> clients = new Dictionary<IPAddress, IPEndPoint>();

    private static UdpClient udpListener;
    private static Action<byte[]> onReceiveData;

    #endregion // Memebers

    #region Properties

    public static int MaxPlayers { get; private set; }
    public static int Port { get; private set; }

    #endregion

    #region Command

    public static void Start(int _maxPlayers, int _port, Action<byte[]> _onReceiveData)
    {
        MaxPlayers = _maxPlayers;
        Port = _port;
        onReceiveData = _onReceiveData;

        ServicesLog.Save("Starting server...");

        try
        {
            if (udpListener == null)
            {
                udpListener = new UdpClient(Port);
                udpListener.BeginReceive(new AsyncCallback(ReceiveData), null);
            }
        }
        catch (SocketException e)
        {
            Debug.LogError(e.Message);
            ServicesLog.Save(e.Message);
        }

        ServicesLog.Save(string.Format("Server started on port {0}.", Port));
    }

    public static void SendData(IPEndPoint _clientEndPoint, byte[] data)
    {
        if (data == null || data.Length < 1 ||
            udpListener == null)
        {
            return;
        }

        try
        {
            if (_clientEndPoint != null)
            {
                udpListener.BeginSend(data, data.Length, _clientEndPoint, null, null);
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            ServicesLog.Save(ex.Message);
        }
    }

    public static void SendData(byte[] data)
    {
        if (data == null || data.Length < 1 ||
            udpListener == null)
        {
            return;
        }

        try
        {
            foreach (var client in clients)
            {
                var clientEndPoint = client.Value;
                if (clientEndPoint != null)
                {
                    udpListener.BeginSend(data, data.Length, clientEndPoint, null, null);
                }
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            ServicesLog.Save(ex.Message);
        }
    }

    private static void ReceiveData(IAsyncResult result)
    {
        Debug.LogErrorFormat("receive something!");

        try
        {
            var clientEndPoint = new IPEndPoint(IPAddress.Any, 0);
            var data = udpListener.EndReceive(result, ref clientEndPoint);

            if (!clients.ContainsKey(clientEndPoint.Address))
            {
                clients.Add(clientEndPoint.Address, clientEndPoint);
            }

            udpListener.BeginReceive(new AsyncCallback(ReceiveData), null);
            //var receivedString = Encoding.ASCII.GetString(received);

            if (onReceiveData == null)
            {
                var message = "onReceiveData is null.";
                Debug.LogError(message);
                ServicesLog.Save(message);
            }
            onReceiveData.Invoke(data);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            ServicesLog.Save(ex.Message);
        }
    }

    #endregion // Command
}
