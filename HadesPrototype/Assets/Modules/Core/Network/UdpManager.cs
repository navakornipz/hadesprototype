﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class UdpManager
{
    #region Members

    private UdpClient sender;
    private UdpClient receiver;
    private IPEndPoint receiveIPGroup;
    private Action<byte[]> onReceiveData;
    private int port;

    #endregion // Memebers

    #region Command

    public void InitReceiver(IPAddress iPAddress, int port, Action<byte[]> onReceiveData)
    {
        this.onReceiveData = onReceiveData;
        this.port = port;

        StartReceivingIP();
    }

    public void InitSender(IPAddress iPAddress, int port)
    {
        sender = new UdpClient(port, AddressFamily.InterNetwork);
        var groupEP = new IPEndPoint(iPAddress, port);
        sender.Connect(groupEP);
    }

    public void SendData(string message)
    {
        if (string.IsNullOrEmpty(message) ||
            sender == null)
        {
            return;
        }

        sender.Send(Encoding.ASCII.GetBytes(message), message.Length);
    }

    public void SendData(byte[] data)
    {
        if (data == null || data.Length < 1 ||
            sender == null)
        {
            return;
        }

        sender.Send(data, data.Length);
    }

    public void StartReceivingIP()
    {
        try
        {
            if (receiver == null)
            {
                receiver = new UdpClient(port);
                receiver.BeginReceive(new AsyncCallback(ReceiveData), null);
            }
        }
        catch (SocketException e)
        {
            Debug.LogError(e.Message);
            ServicesLog.Save(e.Message);
        }
    }

    private void ReceiveData(IAsyncResult result)
    {
        Debug.LogErrorFormat("receive something!");
        receiveIPGroup = new IPEndPoint(IPAddress.Any, port);
        byte[] received;
        if (receiver != null)
        {
            received = receiver.EndReceive(result, ref receiveIPGroup);
        }
        else
        {
            return;
        }
        receiver.BeginReceive(new AsyncCallback(ReceiveData), null);
        //var receivedString = Encoding.ASCII.GetString(received);

        if (onReceiveData == null)
        {
            var message = "onReceiveData is null.";
            Debug.LogError(message);
            ServicesLog.Save(message);
        }
        onReceiveData.Invoke(received);
    }

    #endregion // Command
}
