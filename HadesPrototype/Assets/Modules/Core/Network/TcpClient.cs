﻿using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;

public class TCPClient
{
    private Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    private byte[] _recieveBuffer = new byte[8142];

    private Action<byte[], int> onRecieveData;

    public bool IsConnected
    {
        get
        {
            try
            {
                if (socket == null)
                {
                    return false;
                }

                if (!socket.Connected)
                {
                    return false;
                }
            
                return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0);
            }
            catch (SocketException) { return false; }
        }
    }

    public void Init(IPAddress ipAddress, int port, Action<byte[], int> onRecieveData)
    {
        try
        {
            socket.Connect(new IPEndPoint(ipAddress, port));
        }
        catch (SocketException ex)
        {
            Debug.LogError(ex.Message);
            ServicesLog.Save(string.Format("socket connect failed: {0}", ex.Message));
            return;
        }

        this.onRecieveData = onRecieveData;
        socket.BeginReceive(_recieveBuffer, 0, _recieveBuffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);
    }

    private void ReceiveCallback(IAsyncResult AR)
    {
        //Check how much bytes are recieved and call EndRecieve to finalize handshake
        var recieved = socket.EndReceive(AR);

        if (recieved <= 0)
        {
            socket.BeginReceive(_recieveBuffer, 0, _recieveBuffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);
            return;
        }

        //Copy the recieved data into new buffer , to avoid null bytes
        var recData = new byte[recieved];
        Buffer.BlockCopy(_recieveBuffer, 0, recData, 0, recieved);

        if (onRecieveData != null)
        {
            onRecieveData.Invoke(_recieveBuffer, recieved);
        }

        //Start receiving again
        socket.BeginReceive(_recieveBuffer, 0, _recieveBuffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);
    }

    public void SendData(byte[] data)
    {
        if (socket == null || !socket.Connected)
        {
            return;
        }

        var socketAsyncData = new SocketAsyncEventArgs();
        socketAsyncData.SetBuffer(data, 0, data.Length);
        socket.SendAsync(socketAsyncData);
    }

    public void Stop()
    {
        socket.Dispose();
    }
}