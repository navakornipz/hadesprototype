﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class UDPClient
{
    public int ClientId;
    public UdpClient Socket;
    public IPEndPoint EndPoint;

    private Action<byte[]> onReceiveData;

    public UDPClient(IPAddress iPAddress, int port)
    {
        EndPoint = new IPEndPoint(iPAddress, port);
    }

    public UDPClient(int clientId, IPEndPoint endPoint)
    {
        ClientId = clientId;
        endPoint = EndPoint;
    }

    public void Connect(int port, Action<byte[]> onReceiveData)
    {
        Socket = new UdpClient(port);

        this.onReceiveData = onReceiveData;

        Socket.Connect(EndPoint);
        Socket.BeginReceive(ReceiveData, null);
    }

    public void Dispose()
    {
        if (Socket == null)
        {
            return;
        }

        Socket.Close();
        Socket.Dispose();
    }

    public void SendData(byte[] data)
    {
        try
        {
            if (Socket != null)
            {
                Socket.BeginSend(data, data.Length, null, null);
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            ServicesLog.Save(ex.Message);
        }
    }

    private void ReceiveData(IAsyncResult result)
    {
        try
        {
            var data = Socket.EndReceive(result, ref EndPoint);
            Socket.BeginReceive(ReceiveData, null);

            if (onReceiveData == null)
            {
                var message = "onReceiveData is null.";
                Debug.LogError(message);
                ServicesLog.Save(message);
            }
            onReceiveData.Invoke(data);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            ServicesLog.Save(ex.Message);
        }
    }
}
