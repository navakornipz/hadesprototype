﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    private const float minFadeTime = 0.05f;

    [SerializeField] private AudioMixer masterMixer;

    [SerializeField] private AudioClip[] bgmClips;
    [SerializeField] private AudioClip[] ambienceClips;
    [SerializeField] private AudioClip[] sfxClips;

    [SerializeField] private AudioSource bgmSource;
    [SerializeField] private AudioSource ambienceSource;
    [SerializeField] private AudioSource[] sfxSources;

    private Queue<int> sfxAvailable;
    private List<int> sfxPlayingList;

    public int BgmVolume { get; private set; }
    public int SfxVolume { get; private set; }

    public static AudioManager Instance;

    private void Awake()
    {
        Instance = this;

        sfxAvailable = new Queue<int>();
        for (var i = 0; i < sfxSources.Length; i++)
        {
            sfxAvailable.Enqueue(i);
        }
        
        sfxPlayingList = new List<int>();

        SetBgmVolume(PlayerPrefs.GetInt("BgmVol", -20));
        SetSfxVolume(PlayerPrefs.GetInt("SfxVol", 0));
    }

    private void Update()
    {
        if (sfxPlayingList.Count < 1)
        {
            return;
        }

        for (var i = sfxPlayingList.Count - 1; i >= 0; i--)
        {
            var index = sfxPlayingList[i];
            if (!sfxSources[index].isPlaying)
            {
                sfxPlayingList.RemoveAt(i);
                sfxAvailable.Enqueue(index);
            }
        }
    }

    public void SetBgmVolume(int soundLevel)
    {
        if (BgmVolume == soundLevel)
        {
            return;
        }

        BgmVolume = soundLevel;
        masterMixer.SetFloat("BgmVol", soundLevel);
        PlayerPrefs.SetInt("BgmVol", BgmVolume);
    }

    public void SetSfxVolume(int soundLevel)
    {
        if (SfxVolume == soundLevel)
        {
            return;
        }

        SfxVolume = soundLevel;
        masterMixer.SetFloat("SfxVol", soundLevel);
        PlayerPrefs.SetInt("SfxVol", SfxVolume);
    }

    public void PlayBgm()
    {
        if (bgmSource.isPlaying)
        {
            return;
        }

        if (bgmSource.clip == null)
        {
            Debug.LogError("can't play bgm: no clip in source.");
            return;
        }

        bgmSource.Play();
    }

    public void PlayBgm(int bgmIndex, float fadeTime = 0)
    {
        if (bgmSource.isPlaying)
        {
            bgmSource.Stop();
        }

        bgmSource.clip = bgmClips[bgmIndex];
        bgmSource.loop = true;
        bgmSource.Play();

        if (fadeTime < minFadeTime)
        {
            return;
        }

        StartCoroutine(FadeIn(bgmSource, fadeTime));
    }

    public void PlayAmbience(int ambienceIndex, float fadeTime = 0)
    {
        if (ambienceSource.isPlaying)
        {
            ambienceSource.Stop();
        }

        ambienceSource.clip = ambienceClips[ambienceIndex];
        ambienceSource.loop = true;
        ambienceSource.Play();

        if (fadeTime < minFadeTime)
        {
            return;
        }

        StartCoroutine(FadeIn(ambienceSource, fadeTime));
    }

    public void PlaySfx(int sfxIndex)
    {
        var source = GetAvailiableSfxSource();
        if (source.isPlaying)
        {
            source.Stop();
        }

        if (sfxIndex >= sfxClips.Length)
        {
            return;
        }

        source.clip = sfxClips[sfxIndex];
        source.loop = false;
        source.Play();
    }

    private AudioSource GetAvailiableSfxSource()
    {
        var index = 0;
        if (sfxAvailable.Count < 1)
        {
            index = sfxPlayingList[0];
            sfxPlayingList.RemoveAt(0);
        }
        else
        {
            index = sfxAvailable.Dequeue();
        }

        sfxPlayingList.Add(index);
        return sfxSources[index];
    }

    public void StopBgm(float fadeTime = 0)
    {
        if (!bgmSource.isPlaying)
        {
            return;
        }

        if (fadeTime < minFadeTime)
        {
            bgmSource.Stop();
            return;
        }

        StartCoroutine(FadeOut(bgmSource, fadeTime));
    }

    public void StopAmbience(float fadeTime = 0)
    {
        if (!ambienceSource.isPlaying)
        {
            return;
        }

        if (fadeTime < minFadeTime)
        {
            ambienceSource.Stop();
            return;
        }

        StartCoroutine(FadeOut(ambienceSource, fadeTime));
    }

    private IEnumerator FadeIn(AudioSource audioSource, float fadeTime)
    {
        var volume = audioSource.volume;
        var speed = volume / fadeTime;
        var timer = 0f;
        audioSource.volume = 0;

        while (timer < fadeTime)
        {
            timer += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(0f, volume, timer);
            yield return null;
        }

        audioSource.volume = volume;
    }

    private IEnumerator FadeOut(AudioSource audioSource, float fadeTime)
    {
        var volume = audioSource.volume;
        var speed = volume / fadeTime;
        var timer = 0f;

        while (timer < fadeTime)
        {
            timer += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(volume, 0, timer);
            yield return null;
        }

        audioSource.Stop();
        audioSource.volume = volume;
    }
}
