﻿using System.IO;

public static class MyLog
{
    public static void Save(string filePath, string message)
    {
        using (StreamWriter writetext = File.AppendText(filePath))
        {
            writetext.WriteLine(message);
            writetext.Close();
        }
    }
}
