﻿using System;
using System.IO;
using UnityEngine;

public static class ServicesLog
{
    public const string logFolderName = "Logs";

    public static void Save(string message)
    {
        var path = Directory.GetCurrentDirectory();
        path = string.Format("{0}\\{1}", path, logFolderName);

        try
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var time = DateTime.UtcNow;
            var fileName = string.Format("services_log_{0}{1:00}{2:00}.txt", time.Year, time.Month, time.Day);
            var filePath = string.Format("{0}\\{1}", path, fileName);

            var logMessage = string.Format("{0:00}:{1:00}:{2:00}: {3}\n", time.Hour, time.Minute, time.Second, message);

            MyLog.Save(filePath, logMessage);
        }
        catch (Exception e)
        {
            Debug.LogErrorFormat("ServicesLog Failed: {0}", e.Message);
        }
    }
}
