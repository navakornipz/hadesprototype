﻿#if USING_OPENCV && USING_CAMERA

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebcamSettings : MonoBehaviour
{
    [SerializeField] private Image sample;
    [SerializeField] private Dropdown deviceDropdown;
    [SerializeField] private InputField widthInput;
    [SerializeField] private InputField heightInput;
    [SerializeField] private InputField fpsInput;

    private WebcamPlayerController webcamController;
    private WebcamDeviceData deviceData;
    private string webcamDeviceFormat;
    private int deviceNumber;
    private int selectedDeviceIndex;
    

    public void OnDestroy()
    {
        webcamController.Stop();
    }

    public void Init(WebcamPlayerController webcamController, string webcamDeviceFormat, int deviceNumber)
    {
        this.webcamController = webcamController;
        this.webcamDeviceFormat = webcamDeviceFormat;
        var devices = WebCamTexture.devices;
        var options = new List<string>();
        this.deviceNumber = deviceNumber;

        var deviceJson = PlayerPrefs.GetString(string.Format(webcamDeviceFormat, deviceNumber), string.Empty);
        deviceData = JsonUtility.FromJson<WebcamDeviceData>(deviceJson);
        if (deviceData == null)
        {
            deviceData = new WebcamDeviceData(devices[0].name);
        }

        selectedDeviceIndex = -1;
        deviceDropdown.ClearOptions();
        for (var i = 0; i < devices.Length; i++)
        {
            options.Add(devices[i].name);

            if (deviceData.Name != devices[i].name)
            {
                continue;
            }

            selectedDeviceIndex = i;
        }
        deviceDropdown.AddOptions(options);
        deviceDropdown.value = selectedDeviceIndex;

        widthInput.text = deviceData.Width.ToString();
        heightInput.text = deviceData.Height.ToString();
        fpsInput.text = deviceData.Fps.ToString();

        if (webcamController.IsReady)
        {
            var texture = webcamController.GetTexture();
            sample.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            sample.color = Color.white;
            return;
        }

        StartCoroutine(InitWebcam());
    }

    public void OnApply()
    {
        sample.color = Color.black;
        if (sample.sprite != null)
        {
            Destroy(sample.sprite.texture);
            Destroy(sample.sprite);
            sample.sprite = null;
        }

        if (deviceData == null)
        {
            Debug.LogError("Apply failed: deviceData should not be null.");
            return;
        }

        deviceData.Name = deviceDropdown.options[deviceDropdown.value].text;
        deviceData.Width = int.Parse(widthInput.text);
        deviceData.Height = int.Parse(heightInput.text);
        deviceData.Fps = int.Parse(fpsInput.text);

        var deviceJson = JsonUtility.ToJson(deviceData);
        PlayerPrefs.SetString(string.Format(webcamDeviceFormat, deviceNumber), deviceJson);

        StartCoroutine(InitWebcam());
    }

    public void OnWidthChanged(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            widthInput.text = "1280";
            return;
        }

        var width = 0;
        if (!int.TryParse(value, out width))
        {
            widthInput.text = "1280";
            return;
        }

        if (width < 640)
        {
            widthInput.text = "640";
            return;
        }

        widthInput.text = width.ToString();
    }

    public void OnHeightChanged(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            heightInput.text = "720";
            return;
        }

        var height = 0;
        if (!int.TryParse(value, out height))
        {
            heightInput.text = "720";
            return;
        }

        if (height < 480)
        {
            heightInput.text = "480";
            return;
        }

        heightInput.text = height.ToString();
    }

    public void OnFpsChanged(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            fpsInput.text = "30";
            return;
        }

        var fps = 0;
        if (!int.TryParse(value, out fps))
        {
            fpsInput.text = "30";
            return;
        }

        if (fps < 15)
        {
            fpsInput.text = "15";
            return;
        }

        fpsInput.text = fps.ToString();
    }

    private IEnumerator InitWebcam()
    {
        if (webcamController != null)
        {
            webcamController.Dispose();
            yield return null;
        }
        else
        {
            Debug.LogError("InitWebcam failed: webcam controller should not be null.");
            yield break;
        }

        webcamController.Init(deviceData.Name, deviceData.Width, deviceData.Height, deviceData.Fps, () =>
        {
            var texture = webcamController.GetTexture();
            sample.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            sample.color = Color.white;
        });
    }
}

#endif