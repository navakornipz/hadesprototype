﻿#if USING_OPENCV && USING_CAMERA

using OpenCVForUnity.CoreModule;
using OpenCVForUnity.UnityUtils;
using OpenCVForUnity.UnityUtils.Helper;
using System;
using UnityEngine;

[RequireComponent(typeof(WebCamTextureToMatHelper))]
public class WebcamPlayerController : MonoBehaviour
{
    #region Member

    private WebCamTextureToMatHelper webCamTextureToMatHelper;
    private Texture2D texture;
    private Action onInitCompleted;

    #endregion Member

    #region Properties

    public bool IsInitialized
    {
        get
        {
            return webCamTextureToMatHelper != null && webCamTextureToMatHelper.IsInitialized();
        }
    }

    public bool IsReady
    {
        get
        {
            return texture != null;
        }
    }

    public Mat RgbMat
    {
        get
        {
            return webCamTextureToMatHelper.GetMat();
        }
    }

    public bool IsUpdate
    {
        get
        {
            return webCamTextureToMatHelper.DidUpdateThisFrame();
        }
    }

    public bool IsAutoUpdateTexture { get; set; }

    #endregion Properties

    #region Unity Method

    private void Awake()
    {
        webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper>();
        webCamTextureToMatHelper.onInitialized.AddListener(OnInitialized);
        webCamTextureToMatHelper.onDisposed.AddListener(OnDisposed);
        webCamTextureToMatHelper.onErrorOccurred.AddListener(OnErrorOccurred);

        IsAutoUpdateTexture = true;
    }

    private void Update()
    {
        if (!IsAutoUpdateTexture)
        {
            return;
        }

        if (!webCamTextureToMatHelper.IsPlaying())
        {
            return;
        }

        if (!webCamTextureToMatHelper.DidUpdateThisFrame())
        {
            return;
        }

        var rgbaMat = webCamTextureToMatHelper.GetMat();
        Utils.fastMatToTexture2D(rgbaMat, texture);
    }

    private void OnDestroy()
    {
        Stop();

        if (webCamTextureToMatHelper != null)
            webCamTextureToMatHelper.Dispose();
    }

    #endregion Unity Method

    #region Command

    public void Init(string deviceName, int width, int height, int fps, Action onCompleted)
    {
        if (texture != null)
        {
            Destroy(texture);
        }

        webCamTextureToMatHelper.requestedDeviceName = deviceName;
        webCamTextureToMatHelper.requestedWidth = width;
        webCamTextureToMatHelper.requestedHeight = height;
        webCamTextureToMatHelper.requestedFPS = fps;
        onInitCompleted = onCompleted;

        webCamTextureToMatHelper.Initialize();
    }

    public Texture2D GetTexture()
    {
        if (!webCamTextureToMatHelper.IsInitialized())
        {
            return null;
        }

        return texture;
    }

    public void Play()
    {
        if (webCamTextureToMatHelper.IsPlaying())
        {
            return;
        }

        webCamTextureToMatHelper.Play();
    }

    public void Pause()
    {
        webCamTextureToMatHelper.Pause();
    }

    public void Stop()
    {
        webCamTextureToMatHelper.Stop();
    }

    public void Dispose()
    {
        webCamTextureToMatHelper.Dispose();
    }

    #endregion Command

    #region Event

    public void OnInitialized()
    {
        var webCamTextureMat = webCamTextureToMatHelper.GetMat();
        
        texture = new Texture2D(webCamTextureMat.cols(), webCamTextureMat.rows(), TextureFormat.RGBA32, false);

        if (onInitCompleted == null)
        {
            return;
        }

        onInitCompleted.Invoke();
    }

    public void OnDisposed()
    {
        if (texture != null)
        {
            Destroy(texture);
            texture = null;
        }
    }

    public void OnErrorOccurred(WebCamTextureToMatHelper.ErrorCode errorCode)
    {
        Debug.LogErrorFormat("WebCamTextureToMatHelper: {0}", errorCode);
    }

    #endregion Event
}

#endif //USING_OPENCV && USING_DLIBFACE