﻿#if USING_OPENCV && USING_DLIBFACE && USING_CAMERA

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DlibFaceLandmarkDetector;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ObjdetectModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.UnityUtils.Helper;
using UnityEngine.Video;
using System.Collections;

namespace DlibFaceLandmarkDetectorExample
{
    [RequireComponent(typeof(WebCamTextureToMatHelper), typeof(ImageOptimizationHelper))]
    public class MatchFaceScene : MonoBehaviour
    {
        private const string faceTestFile = "FaceMapping/FaceTest.png";

        [SerializeField] private GameObject facePlane;
        [SerializeField] private MeshRenderer quadRenderer;
        [SerializeField] private VideoPlayer vdo;

        [SerializeField] private Renderer faceRenderer;
        [SerializeField] private Material[] faceMaterials;

        [SerializeField] private RectTransform debugKnob;
        [SerializeField] private Transform debugSphere;

        [SerializeField] private int debugIndex;

        /// <summary>
        /// Determines if enable downscale.
        /// </summary>
        public bool enableDownScale;

        /// <summary>
        /// The enable downscale toggle.
        /// </summary>
        public Toggle enableDownScaleToggle;

        /// <summary>
        /// Determines if enable skipframe.
        /// </summary>
        public bool enableSkipFrame;

        /// <summary>
        /// The enable skipframe toggle.
        /// </summary>
        public Toggle enableSkipFrameToggle;

        /// <summary>
        /// Determines if use OpenCV FaceDetector for face detection.
        /// </summary>
        public bool useOpenCVFaceDetector;

        /// <summary>
        /// The use OpenCV FaceDetector toggle.
        /// </summary>
        public Toggle useOpenCVFaceDetectorToggle;

        /// <summary>
        /// The gray mat.
        /// </summary>
        Mat grayMat;

        /// <summary>
        /// The texture.
        /// </summary>
        Texture2D texture;

        /// <summary>
        /// The cascade.
        /// </summary>
        CascadeClassifier cascade;

        /// <summary>
        /// The webcam texture to mat helper.
        /// </summary>
        WebCamTextureToMatHelper webCamTextureToMatHelper;

        /// <summary>
        /// The image optimization helper.
        /// </summary>
        ImageOptimizationHelper imageOptimizationHelper;

        /// <summary>
        /// The face landmark detector.
        /// </summary>
        FaceLandmarkDetector faceLandmarkDetector;

        /// <summary>
        /// The FPS monitor.
        /// </summary>
        FpsMonitor fpsMonitor;

        /// <summary>
        /// The detection result.
        /// </summary>
        List<UnityEngine.Rect> detectionResult;

        /// <summary>
        /// The haarcascade_frontalface_alt_xml_filepath.
        /// </summary>
        string haarcascade_frontalface_alt_xml_filepath;

        /// <summary>
        /// The dlib shape predictor file name.
        /// </summary>
        string dlibShapePredictorFileName = "sp_human_face_68.dat";

        /// <summary>
        /// The dlib shape predictor file path.
        /// </summary>
        string dlibShapePredictorFilePath;

        private bool foundFace;
        private bool isDrawFaceWireFrame;

        private Mesh mesh;
        private List<Vector3> vertices;
        private List<Vector3> newVertices;
        private Quaternion originRotation;
        private Vector3 faceAdjustPosition;
        private float faceAdjustScale;
        private int currentFaceIndex;

        private int size;
        private float unit;
        private float lastAngle;
        private Vector2 texUnit;

        private int[] relocateArray;
        private Dictionary<int, Dictionary<int, Vector3>> averagePoints;

        private Texture2D faceTexture;

#if UNITY_WEBGL && !UNITY_EDITOR
        IEnumerator getFilePath_Coroutine;
#endif

        private void Start()
        {
            fpsMonitor = GetComponent<FpsMonitor>();

            enableDownScaleToggle.isOn = enableDownScale;
            enableSkipFrameToggle.isOn = enableSkipFrame;
            useOpenCVFaceDetectorToggle.isOn = useOpenCVFaceDetector;

            imageOptimizationHelper = gameObject.GetComponent<ImageOptimizationHelper>();
            webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper>();

            dlibShapePredictorFileName = DlibFaceLandmarkDetectorExample.dlibShapePredictorFileName;
#if UNITY_WEBGL && !UNITY_EDITOR
            getFilePath_Coroutine = GetFilePath ();
            StartCoroutine (getFilePath_Coroutine);
#else
            haarcascade_frontalface_alt_xml_filepath = OpenCVForUnity.UnityUtils.Utils.getFilePath("haarcascade_frontalface_alt.xml");
            dlibShapePredictorFilePath = DlibFaceLandmarkDetector.UnityUtils.Utils.getFilePath(dlibShapePredictorFileName);
            Run();
#endif

            var meshFilter = facePlane.GetComponent<MeshFilter>();
            mesh = meshFilter.mesh;
            vertices = new List<Vector3>();
            mesh.GetVertices(vertices);
            var triangles = new List<int>();
            mesh.GetTriangles(triangles, 0);
            triangles.Add(41);
            triangles.Add(40);
            triangles.Add(44);
            triangles.Add(114);
            triangles.Add(113);
            triangles.Add(115);
            mesh.SetTriangles(triangles, 0);
            originRotation = facePlane.transform.rotation;

            faceAdjustPosition = Vector3.zero;
            faceAdjustScale = 1f;

            relocateArray = new int[68];
            relocateArray[0] = 44;
            relocateArray[1] = 46;
            relocateArray[2] = 53;
            relocateArray[3] = 52;
            relocateArray[4] = 55;
            relocateArray[5] = 57;
            relocateArray[6] = 58;
            relocateArray[7] = 63;
            relocateArray[8] = 61;
            relocateArray[9] = 132;
            relocateArray[10] = 129;
            relocateArray[11] = 128;
            relocateArray[12] = 126;
            relocateArray[13] = 123;
            relocateArray[14] = 124;
            relocateArray[15] = 117;
            relocateArray[16] = 115;
            relocateArray[17] = 33;
            relocateArray[18] = 34;
            relocateArray[19] = 37;
            relocateArray[20] = 36;
            relocateArray[21] = 11;
            relocateArray[22] = 99;
            relocateArray[23] = 108;
            relocateArray[24] = 109;
            relocateArray[25] = 97;
            relocateArray[26] = 96;
            relocateArray[27] = 8; // no actual point
            relocateArray[28] = 15;
            relocateArray[29] = 16;
            relocateArray[30] = 18;
            relocateArray[31] = 19;
            relocateArray[32] = 20;
            relocateArray[33] = 21;
            relocateArray[34] = 79;
            relocateArray[35] = 80;
            relocateArray[36] = 26;
            relocateArray[37] = 28;
            relocateArray[38] = 27;
            relocateArray[39] = 24;
            relocateArray[40] = 23;
            relocateArray[41] = 25;
            relocateArray[42] = 87;
            relocateArray[43] = 90;
            relocateArray[44] = 91;
            relocateArray[45] = 89;
            relocateArray[46] = 88;
            relocateArray[47] = 86;
            relocateArray[48] = 50;
            relocateArray[49] = 74;
            relocateArray[50] = 75;
            relocateArray[51] = 76;
            relocateArray[52] = 78;
            relocateArray[53] = 139;
            relocateArray[54] = 121;
            relocateArray[55] = 140;
            relocateArray[56] = 133;
            relocateArray[57] = 66;
            relocateArray[58] = 65;
            relocateArray[59] = 77;
            relocateArray[60] = 73;
            relocateArray[61] = 70;
            relocateArray[62] = 69;
            relocateArray[63] = 135;
            relocateArray[64] = 138;
            relocateArray[65] = 134;
            relocateArray[66] = 68;
            relocateArray[67] = 67;

            averagePoints = new Dictionary<int, Dictionary<int, Vector3>>();
            CreateAveragePoint(17, new int[] { 16, 19 });
            CreateAveragePoint(81, new int[] { 16, 80 });
            CreateAveragePoint(14, new int[] { 15, 19 });
            CreateAveragePoint(82, new int[] { 15, 80 });
            CreateAveragePoint(22, new int[] { 8, 19 });
            CreateAveragePoint(84, new int[] { 8, 80 });
            CreateAveragePoint(12, new int[] { 15, 24 });
            CreateAveragePoint(83, new int[] { 15, 87 });
            CreateAveragePoint(13, new int[] { 15, 24 });
            CreateAveragePoint(85, new int[] { 15, 87 });
            CreateAveragePoint(9, new int[] { 12 });
            CreateAveragePoint(100, new int[] { 83 });

            CreateAveragePoint(29, new int[] { 28, 37 });
            CreateAveragePoint(30, new int[] { 27, 36 });
            CreateAveragePoint(31, new int[] { 11, 24 });
            CreateAveragePoint(32, new int[] { 34, 37 });

            CreateAveragePoint(92, new int[] { 91, 109 });
            CreateAveragePoint(93, new int[] { 90, 108 });
            CreateAveragePoint(94, new int[] { 90, 99 });
            CreateAveragePoint(95, new int[] { 89, 97 });

            CreateAveragePoint(41, new int[] { 40, 61, 96 });
            CreateAveragePoint(42, new int[] { 40, 61, 96 });
            CreateAveragePoint(2, new int[] { 40, 61, 96 });
            CreateAveragePoint(0, new int[] { 40, 61, 96 });
            CreateAveragePoint(4, new int[] { 40, 61, 96 });
            CreateAveragePoint(104, new int[] { 40, 61, 96 });
            CreateAveragePoint(105, new int[] { 40, 61, 96 });
            CreateAveragePoint(112, new int[] { 40, 61, 96 });
            CreateAveragePoint(113, new int[] { 40, 61, 96 });
            CreateAveragePoint(3, new int[] { 8, 61 });
            //CreateAveragePoint(6, new int[] { 8, 61 });
            //CreateAveragePoint(1, new int[] { 40, 61, 96 });
            //CreateAveragePoint(103, new int[] { 40, 61, 96 });

            CreateAveragePoint(7, new int[] { 11 });
            CreateAveragePoint(10, new int[] { 36 });
            CreateAveragePoint(38, new int[] { 37 });
            CreateAveragePoint(35, new int[] { 34 });
            CreateAveragePoint(101, new int[] { 99 });
            CreateAveragePoint(106, new int[] { 108 });
            CreateAveragePoint(107, new int[] { 109 });
            CreateAveragePoint(98, new int[] { 97 });

            CreateAveragePoint(5, new int[] { 10 });
            CreateAveragePoint(39, new int[] { 35 });
            CreateAveragePoint(43, new int[] { 38 });
            CreateAveragePoint(102, new int[] { 106 });
            CreateAveragePoint(110, new int[] { 98 });
            CreateAveragePoint(111, new int[] { 107 });

            //StartCoroutine(LoadStreamingAsset("FaceMapping/FaceTest.png"));
        }

        public IEnumerator LoadStreamingAsset(string fileName)
        {
            var filePath = "file://" + System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
            var www = new WWW(filePath);

            while (!www.isDone)
            {
                yield return null;
            }

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.error + " " + filePath);
                yield break;
            }
            else
            {
                faceTexture = www.texture;
                faceRenderer.material.mainTexture = faceTexture;
            }

            yield return 0;
        }

#if UNITY_WEBGL && !UNITY_EDITOR
        private IEnumerator GetFilePath ()
        {
            var getFilePathAsync_0_Coroutine = OpenCVForUnity.UnityUtils.Utils.getFilePathAsync ("haarcascade_frontalface_alt.xml", (result) => {
                haarcascade_frontalface_alt_xml_filepath = result;
            });
            yield return getFilePathAsync_0_Coroutine;

            var getFilePathAsync_1_Coroutine = DlibFaceLandmarkDetector.UnityUtils.Utils.getFilePathAsync (dlibShapePredictorFileName, (result) => {
                dlibShapePredictorFilePath = result;
            });
            yield return getFilePathAsync_1_Coroutine;

            getFilePath_Coroutine = null;

            Run ();
        }
#endif

        private void Run()
        {
            if (string.IsNullOrEmpty(dlibShapePredictorFilePath))
            {
                Debug.LogError("shape predictor file does not exist. Please copy from “DlibFaceLandmarkDetector/StreamingAssets/” to “Assets/StreamingAssets/” folder. ");
            }

            cascade = new CascadeClassifier(haarcascade_frontalface_alt_xml_filepath);
#if !UNITY_WSA_10_0
            if (cascade.empty())
            {
                Debug.LogError("cascade file is not loaded. Please copy from “OpenCVForUnity/StreamingAssets/” to “Assets/StreamingAssets/” folder. ");
            }
#endif

            faceLandmarkDetector = new FaceLandmarkDetector(dlibShapePredictorFilePath);

#if UNITY_ANDROID && !UNITY_EDITOR
            // Avoids the front camera low light issue that occurs in only some Android devices (e.g. Google Pixel, Pixel2).
            webCamTextureToMatHelper.avoidAndroidFrontCameraLowLightIssue = true;
#endif
            webCamTextureToMatHelper.Initialize();

            texUnit = Vector2.one;//new Vector2((float)1280 / texture.width, (float)720 / texture.height);
        }

        /// <summary>
        /// Raises the webcam texture to mat helper initialized event.
        /// </summary>
        public void OnWebCamTextureToMatHelperInitialized()
        {
            Debug.Log("OnWebCamTextureToMatHelperInitialized");

            Mat webCamTextureMat = webCamTextureToMatHelper.GetMat();
            Mat downscaleMat = imageOptimizationHelper.GetDownScaleMat(webCamTextureMat);

            texture = new Texture2D(webCamTextureMat.cols(), webCamTextureMat.rows(), TextureFormat.RGBA32, false);

            gameObject.GetComponent<Renderer>().material.mainTexture = texture;

            //gameObject.transform.localScale = new Vector3(webCamTextureMat.cols(), webCamTextureMat.rows(), 1);
            transform.localScale = new Vector3(Camera.main.orthographicSize * 2.0f * Screen.width / Screen.height, Camera.main.orthographicSize * 2.0f, 0.1f);
            Debug.Log("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);

            if (fpsMonitor != null)
            {
                fpsMonitor.Add("dlib shape predictor", dlibShapePredictorFileName);
                fpsMonitor.Add("original_width", webCamTextureToMatHelper.GetWidth().ToString());
                fpsMonitor.Add("original_height", webCamTextureToMatHelper.GetHeight().ToString());
                fpsMonitor.Add("downscaleRaito", imageOptimizationHelper.downscaleRatio.ToString());
                fpsMonitor.Add("frameSkippingRatio", imageOptimizationHelper.frameSkippingRatio.ToString());
                fpsMonitor.Add("downscale_width", downscaleMat.width().ToString());
                fpsMonitor.Add("downscale_height", downscaleMat.height().ToString());
                fpsMonitor.Add("orientation", Screen.orientation.ToString());
            }

            /*float width = webCamTextureMat.width();
            float height = webCamTextureMat.height();

            float widthScale = (float)Screen.width / width;
            float heightScale = (float)Screen.height / height;
            if (widthScale < heightScale)
            {
                Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
            }
            else
            {
                Camera.main.orthographicSize = height / 2;
            }*/


            grayMat = new Mat(webCamTextureMat.rows(), webCamTextureMat.cols(), CvType.CV_8UC1);

            detectionResult = new List<UnityEngine.Rect>();
        }

        /// <summary>
        /// Raises the web cam texture to mat helper disposed event.
        /// </summary>
        public void OnWebCamTextureToMatHelperDisposed()
        {
            Debug.Log("OnWebCamTextureToMatHelperDisposed");

            if (grayMat != null)
            {
                grayMat.Dispose();
                grayMat = null;
            }
            if (texture != null)
            {
                Texture2D.Destroy(texture);
                texture = null;
            }
        }

        /// <summary>
        /// Raises the web cam texture to mat helper error occurred event.
        /// </summary>
        /// <param name="errorCode">Error code.</param>
        public void OnWebCamTextureToMatHelperErrorOccurred(WebCamTextureToMatHelper.ErrorCode errorCode)
        {
            Debug.Log("OnWebCamTextureToMatHelperErrorOccurred " + errorCode);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                isDrawFaceWireFrame = !isDrawFaceWireFrame;
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                currentFaceIndex++;

                if (currentFaceIndex >= faceMaterials.Length)
                {
                    currentFaceIndex = 0;
                }

                faceRenderer.material = faceMaterials[currentFaceIndex];
                //faceRenderer.material.mainTexture = faceTexture;
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                StartCoroutine(LoadStreamingAsset(faceTestFile));
            }

            if (Input.GetKeyDown(KeyCode.H))
            {
                quadRenderer.enabled = !quadRenderer.enabled;
            }

            if (!vdo.enabled && Input.GetKeyDown(KeyCode.P))
            {
                vdo.enabled = true;
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                vdo.enabled = false;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                faceAdjustPosition -= new Vector3(10f, 0, 0);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                faceAdjustPosition += new Vector3(10f, 0, 0);
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                faceAdjustPosition -= new Vector3(0, 10f, 0);
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                faceAdjustPosition += new Vector3(0, 10f, 0);
            }

            if (Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                faceAdjustScale -= 0.01f;
            }
            else if (Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                faceAdjustScale += 0.01f;
            }

            if (webCamTextureToMatHelper.IsPlaying() && webCamTextureToMatHelper.DidUpdateThisFrame())
            {
                Mat rgbaMat = webCamTextureToMatHelper.GetMat();

                Mat downScaleRgbaMat = null;
                float DOWNSCALE_RATIO = 1.0f;
                if (enableDownScale)
                {
                    downScaleRgbaMat = imageOptimizationHelper.GetDownScaleMat(rgbaMat);
                    DOWNSCALE_RATIO = imageOptimizationHelper.downscaleRatio;
                }
                else
                {
                    downScaleRgbaMat = rgbaMat;
                    DOWNSCALE_RATIO = 1.0f;
                }

                // set the downscale mat
                OpenCVForUnityUtils.SetImage(faceLandmarkDetector, downScaleRgbaMat);

                // detect faces on the downscale image
                if (!enableSkipFrame || !imageOptimizationHelper.IsCurrentFrameSkipped())
                {
                    //detect face rects
                    if (useOpenCVFaceDetector)
                    {
                        // convert image to greyscale.
                        Imgproc.cvtColor(downScaleRgbaMat, grayMat, Imgproc.COLOR_RGBA2GRAY);

                        using (Mat equalizeHistMat = new Mat())
                        using (MatOfRect faces = new MatOfRect())
                        {
                            Imgproc.equalizeHist(grayMat, equalizeHistMat);

                            cascade.detectMultiScale(equalizeHistMat, faces, 1.1f, 2, 0 | Objdetect.CASCADE_SCALE_IMAGE, new Size(equalizeHistMat.cols() * 0.15, equalizeHistMat.cols() * 0.15), new Size());

                            List<OpenCVForUnity.CoreModule.Rect> opencvDetectResult = faces.toList();

                            // correct the deviation of the detection result of the face rectangle of OpenCV and Dlib.
                            detectionResult.Clear();
                            foreach (var opencvRect in opencvDetectResult)
                            {
                                detectionResult.Add(new UnityEngine.Rect((float)opencvRect.x, (float)opencvRect.y + (float)(opencvRect.height * 0.1f), (float)opencvRect.width, (float)opencvRect.height));
                            }
                        }

                    }
                    else
                    {
                        // Dlib's face detection processing time increases in proportion to the image size.
                        detectionResult = faceLandmarkDetector.Detect();
                    }

                    if (enableDownScale)
                    {
                        for (int i = 0; i < detectionResult.Count; ++i)
                        {
                            var rect = detectionResult[i];
                            detectionResult[i] = new UnityEngine.Rect(
                                rect.x * DOWNSCALE_RATIO,
                                rect.y * DOWNSCALE_RATIO,
                                rect.width * DOWNSCALE_RATIO,
                                rect.height * DOWNSCALE_RATIO);
                        }
                    }
                }

                // set the original scale image
                OpenCVForUnityUtils.SetImage(faceLandmarkDetector, rgbaMat);
                foundFace = false;

                if (detectionResult.Count > 0)
                {
                    foundFace = true;
                    newVertices = new List<Vector3>(vertices);
                }

                var posChanged = false;
                // detect face landmarks on the original image
                foreach (var rect in detectionResult)
                {
                    //detect landmark points
                    List<Vector2> points = faceLandmarkDetector.DetectLandmark(rect);

                    if (isDrawFaceWireFrame)
                    {
                        //draw landmark points
                        OpenCVForUnityUtils.DrawFaceLandmark(rgbaMat, points, new Scalar(0, 255, 0, 255), 2);
                        //draw face rect
                        OpenCVForUnityUtils.DrawFaceRect(rgbaMat, rect, new Scalar(255, 0, 0, 255), 2);
                    }

                    //Transform Face
                    var centerIndex = 29;
                    var chinPoint = points[centerIndex];
                    chinPoint.y = Screen.height - (chinPoint.y * texUnit.y);
                    chinPoint.x = chinPoint.x * texUnit.x;

                    var currentChinPoint3D = vertices[centerIndex];
                    var targetChinPoint3D = Camera.main.ScreenToWorldPoint(chinPoint);
                    var deltaPosition = targetChinPoint3D - currentChinPoint3D;
                    var newPos = new Vector3(deltaPosition.x, deltaPosition.y, -0.01f);
                    posChanged = (newPos - facePlane.transform.position).sqrMagnitude >= 5f;
                    //facePlane.transform.position = new Vector3(deltaPosition.x, deltaPosition.y, -0.01f);// (new Vector3(deltaPosition.x, deltaPosition.y, -0.01f) - facePlane.transform.position).magnitude;
                    facePlane.transform.position = !posChanged ? facePlane.transform.position : newPos;

                    if (posChanged)
                    {
                        //Scale Face
                        var leftIndex = 0;
                        var rightIndex = 16;
                        var leftPoint = points[leftIndex];
                        var rightPoint = points[rightIndex];
                        leftPoint.y = Screen.height - (leftPoint.y * texUnit.y);
                        leftPoint.x = leftPoint.x * texUnit.x;
                        rightPoint.y = Screen.height - (rightPoint.y * texUnit.y);
                        rightPoint.x = rightPoint.x * texUnit.x;

                        var distance = Vector3.Distance(leftPoint, rightPoint);
                        var curretnDistance = Vector3.Distance(vertices[leftIndex], vertices[rightIndex]);
                        var newScale = (distance / curretnDistance) * 0.8f;
                        if (Mathf.Abs(facePlane.transform.localScale.x - newScale) > 0.5f)
                            facePlane.transform.localScale = new Vector3(newScale, newScale, 1f);

                        //Rotate Face
                        var topIndex = 36;
                        var bottomIndex = 45;
                        var topPoint = points[topIndex];
                        var bottomPoint = points[bottomIndex];
                        var vector = topPoint - bottomPoint;
                        var angle = Vector3.Angle(Vector3.left, vector);
                        facePlane.transform.rotation = originRotation;
                        angle = vector.y < 0 ? angle : -angle;
                        //if (Mathf.Abs(lastAngle - angle) > 0f)
                        {
                            lastAngle = angle;
                            facePlane.transform.Rotate(new Vector3(0, 0, angle));
                        }
                    }

                    //Update Vertices
                    var total = 68;
                    if (newVertices == null)
                    {
                        newVertices = new List<Vector3>();
                    }
                    mesh.GetVertices(newVertices);
                    for (var i = 0; i < total; i++)
                    {
                        var index = relocateArray[i];
                        var point = points[i];
                        var w = texture.width;// Screen.width;
                        var h = texture.height;// Screen.height;
                        point.y = (h - (point.y * texUnit.y)) - h * 0.5f;
                        point.x = point.x * texUnit.x - w * 0.5f;
                        var ray = Camera.main.ScreenPointToRay(point);
                        var targetPoint3D = point;// Camera.main.ScreenToWorldPoint(point);

                        //debugKnob.anchoredPosition = point;
                        //debugSphere.position = targetPoint3D;

                        var newPoint = facePlane.transform.InverseTransformPoint(targetPoint3D);
                        newPoint.z = 0;
                        //newVertices[index] = newPoint;
                        newVertices[index] = ToNewPoint(newVertices[index], newPoint);
                    }

                    newVertices[40] = newVertices[33];
                    newVertices[114] = newVertices[96];

                    newVertices[45] = newVertices[33] + (newVertices[46] - newVertices[33]) * 0.75f;
                    newVertices[47] = newVertices[26] + (newVertices[46] - newVertices[26]) * 0.8f;
                    newVertices[48] = newVertices[25] + (newVertices[53] - newVertices[25]) * 0.6f;
                    newVertices[49] = newVertices[25] + (newVertices[53] - newVertices[25]) * 0.8f;
                    newVertices[116] = newVertices[96] + (newVertices[117] - newVertices[96]) * 0.75f;
                    newVertices[118] = newVertices[89] + (newVertices[117] - newVertices[89]) * 0.8f;
                    newVertices[119] = newVertices[88] + (newVertices[124] - newVertices[88]) * 0.6f;
                    newVertices[120] = newVertices[88] + (newVertices[124] - newVertices[88]) * 0.8f;

                    newVertices[122] = newVertices[121] + (newVertices[123] - newVertices[121]) * 0.5f;
                    newVertices[125] = newVertices[122];
                    newVertices[51] = newVertices[50] + (newVertices[52] - newVertices[50]) * 0.5f;
                    newVertices[54] = newVertices[51];

                    newVertices[136] = newVertices[135] + (newVertices[138] - newVertices[135]) * 0.5f;
                    newVertices[137] = newVertices[134] + (newVertices[138] - newVertices[134]) * 0.5f;
                    newVertices[71] = newVertices[70] + (newVertices[73] - newVertices[70]) * 0.5f;
                    newVertices[72] = newVertices[67] + (newVertices[73] - newVertices[67]) * 0.5f;

                    newVertices[127] = newVertices[129] + (newVertices[138] - newVertices[129]) * 0.5f;
                    newVertices[56] = newVertices[50] + (newVertices[58] - newVertices[50]) * 0.5f;
                    newVertices[131] = newVertices[132] + (newVertices[140] - newVertices[132]) * 0.3f;
                    newVertices[62] = newVertices[63] + (newVertices[77] - newVertices[63]) * 0.3f;
                    newVertices[60] = newVertices[61] + (newVertices[66] - newVertices[61]) * 0.7f;
                    newVertices[64] = newVertices[61] + (newVertices[66] - newVertices[61]) * 0.8f;
                    newVertices[130] = newVertices[66] + (newVertices[131] - newVertices[66]) * 0.4f;
                    newVertices[59] = newVertices[66] + (newVertices[62] - newVertices[66]) * 0.4f;

                    newVertices[1] = newVertices[2] + (newVertices[3] - newVertices[2]) * 0.7f;
                    newVertices[103] = newVertices[105] + (newVertices[3] - newVertices[105]) * 0.7f;

                    foreach (var pv in averagePoints)
                    {
                        newVertices[pv.Key] = AverageNewPoint(pv.Key);
                    }

                    newVertices[6] = newVertices[8] + (newVertices[8] - newVertices[15]) * 2f;

                    if (newVertices[41].x > newVertices[44].x)
                    {
                        var deltaX = newVertices[44].x - newVertices[41].x;
                        newVertices[41] += new Vector3(deltaX, 0,0);
                        newVertices[42] += new Vector3(deltaX, 0, 0) * 0.75f;
                        newVertices[2] += new Vector3(deltaX, 0, 0) * 0.25f;
                    }

                    if (newVertices[115].x > newVertices[113].x)
                    {
                        var deltaX = newVertices[115].x - newVertices[113].x;
                        newVertices[113] += new Vector3(deltaX, 0, 0);
                        newVertices[112] += new Vector3(deltaX, 0, 0) * 0.75f;
                        newVertices[105] += new Vector3(deltaX, 0, 0) * 0.25f;
                    }

                    //TODO: fix this later
                    /*newVertices[41] = newVertices[40];
                    newVertices[42] = newVertices[39];
                    newVertices[0] = newVertices[43];
                    newVertices[1] = newVertices[5];
                    newVertices[2] = newVertices[43];
                    newVertices[3] = newVertices[6];
                    newVertices[4] = newVertices[6];
                    newVertices[103] = newVertices[102];
                    newVertices[104] = newVertices[111];
                    newVertices[105] = newVertices[111];
                    newVertices[112] = newVertices[110];
                    newVertices[113] = newVertices[114];*/

                    mesh.SetVertices(newVertices);

                    facePlane.transform.localScale *= faceAdjustScale;
                    facePlane.transform.position += faceAdjustPosition;

                    debugSphere.position = facePlane.transform.TransformPoint(newVertices[debugIndex]);
                    break;
                }

                //Imgproc.putText (rgbaMat, "Original:(" + rgbaMat.width () + "," + rgbaMat.height () + ") DownScale:(" + downScaleRgbaMat.width () + "," + downScaleRgbaMat.height () + ") FrameSkipping: " + imageOptimizationHelper.frameSkippingRatio, new Point (5, rgbaMat.rows () - 10), Imgproc.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar (255, 255, 255, 255), 2, Imgproc.LINE_AA, false);

                OpenCVForUnity.UnityUtils.Utils.fastMatToTexture2D(rgbaMat, texture);

                //if (foundFace && posChanged)
                //    mesh.SetVertices(newVertices);
            }
        }

        /// <summary>
        /// Raises the destroy event.
        /// </summary>
        private void OnDestroy()
        {
            if (webCamTextureToMatHelper != null)
                webCamTextureToMatHelper.Dispose();

            if (imageOptimizationHelper != null)
                imageOptimizationHelper.Dispose();

            if (faceLandmarkDetector != null)
                faceLandmarkDetector.Dispose();

            if (cascade != null)
                cascade.Dispose();

#if UNITY_WEBGL && !UNITY_EDITOR
            if (getFilePath_Coroutine != null) {
                StopCoroutine (getFilePath_Coroutine);
                ((IDisposable)getFilePath_Coroutine).Dispose ();
            }
#endif
        }

        /// <summary>
        /// Raises the back button click event.
        /// </summary>
        public void OnBackButtonClick()
        {
            SceneManager.LoadScene("DlibFaceLandmarkDetectorExample");
        }

        /// <summary>
        /// Raises the play button click event.
        /// </summary>
        public void OnPlayButtonClick()
        {
            webCamTextureToMatHelper.Play();
        }

        /// <summary>
        /// Raises the pause button click event.
        /// </summary>
        public void OnPauseButtonClick()
        {
            webCamTextureToMatHelper.Pause();
        }

        /// <summary>
        /// Raises the stop button click event.
        /// </summary>
        public void OnStopButtonClick()
        {
            webCamTextureToMatHelper.Stop();
        }

        /// <summary>
        /// Raises the change camera button click event.
        /// </summary>
        public void OnChangeCameraButtonClick()
        {
            webCamTextureToMatHelper.requestedIsFrontFacing = !webCamTextureToMatHelper.IsFrontFacing();
        }

        /// <summary>
        /// Raises the enable downscale toggle value changed event.
        /// </summary>
        public void OnEnableDownScaleToggleValueChanged()
        {
            if (enableDownScaleToggle.isOn)
            {
                enableDownScale = true;
            }
            else
            {
                enableDownScale = false;
            }
        }

        /// <summary>
        /// Raises the enable skipframe toggle value changed event.
        /// </summary>
        public void OnEnableSkipFrameToggleValueChanged()
        {
            if (enableSkipFrameToggle.isOn)
            {
                enableSkipFrame = true;
            }
            else
            {
                enableSkipFrame = false;
            }
        }

        /// <summary>
        /// Raises the use OpenCV FaceDetector toggle value changed event.
        /// </summary>
        public void OnUseOpenCVFaceDetectorToggleValueChanged()
        {
            if (useOpenCVFaceDetectorToggle.isOn)
            {
                useOpenCVFaceDetector = true;
            }
            else
            {
                useOpenCVFaceDetector = false;
            }
        }
        
        private void CreateAveragePoint(int index, int[] closeIndices)
        {
            var newData = new Dictionary<int, Vector3>();
            var v1 = vertices[index];
            for (var i = 0; i < closeIndices.Length; i++)
            {
                var cIndex = closeIndices[i];
                var v2 = vertices[cIndex];
                var dVector = v1 - v2;
                newData.Add(cIndex, dVector);
            }
            averagePoints.Add(index, newData);
        }

        private Vector3 AverageNewPoint(int index)
        {
            var result = Vector3.zero;
            var cData = averagePoints[index];

            foreach (var p in cData)
            {
                var cIndex = p.Key;
                result += newVertices[cIndex] + p.Value;
            }

            result /= cData.Count;

            return result;
        }

        private Vector3 ToNewPoint(Vector3 currentPoint, Vector3 newPoint)
        {

            return (newPoint - currentPoint).sqrMagnitude < 0.5f ? Vector3.Slerp(currentPoint, newPoint, 0.3f) : newPoint;
            //return Vector3.Slerp(currentPoint, newPoint, 1f);
        }

        private float DistanceSq(Vector2 a, Vector2 b)
        {
            var x = b.x - a.x;
            var y = b.y - a.y;
            return x * x + y * y;
        }
    }
}

#endif