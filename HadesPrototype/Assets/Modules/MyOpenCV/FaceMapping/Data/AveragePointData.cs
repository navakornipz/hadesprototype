﻿using UnityEngine;

public class AveragePointData
{
    public float Distance;
    public Vector3 Direction;
}
