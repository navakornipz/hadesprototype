﻿#if USING_OPENCV && USING_DLIBFACE && USING_CAMERA

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DlibFaceLandmarkDetector;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ObjdetectModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.UnityUtils.Helper;
using UnityEngine.Video;
using System.Collections;
using UnityEngine.Networking;

namespace DlibFaceLandmarkDetectorExample
{
    [RequireComponent(typeof(WebCamTextureToMatHelper), typeof(ImageOptimizationHelper))]
    public class MatchFaceV2Scene : MonoBehaviour
    {
        private const string faceTestFile = "FaceMapping/FaceTest.png";

        private readonly string[] maskName =
        {
            "Yaya merged",
            "Thanos merged",
            "Titan Face",
            "Makeup 1",
            "Makeup 2",
            "Makeup 3",
            "Makeup 4"
        };

        [SerializeField] private GameObject facePlane;
        [SerializeField] private MeshRenderer quadRenderer;

        [SerializeField] private Material[] faceMaterials;

        [SerializeField] private Text debugText;
        [SerializeField] private RectTransform debugKnob;
        [SerializeField] private Transform debugSphere;

        [SerializeField] private int debugIndex;

        /// <summary>
        /// Determines if enable downscale.
        /// </summary>
        public bool enableDownScale;

        /// <summary>
        /// The enable downscale toggle.
        /// </summary>
        public Toggle enableDownScaleToggle;

        /// <summary>
        /// Determines if enable skipframe.
        /// </summary>
        public bool enableSkipFrame;

        /// <summary>
        /// The enable skipframe toggle.
        /// </summary>
        public Toggle enableSkipFrameToggle;

        /// <summary>
        /// Determines if use OpenCV FaceDetector for face detection.
        /// </summary>
        public bool useOpenCVFaceDetector;

        /// <summary>
        /// The use OpenCV FaceDetector toggle.
        /// </summary>
        public Toggle useOpenCVFaceDetectorToggle;

        /// <summary>
        /// The gray mat.
        /// </summary>
        Mat grayMat;

        /// <summary>
        /// The texture.
        /// </summary>
        Texture2D texture;

        /// <summary>
        /// The cascade.
        /// </summary>
        CascadeClassifier cascade;

        /// <summary>
        /// The webcam texture to mat helper.
        /// </summary>
        WebCamTextureToMatHelper webCamTextureToMatHelper;

        /// <summary>
        /// The image optimization helper.
        /// </summary>
        ImageOptimizationHelper imageOptimizationHelper;

        /// <summary>
        /// The face landmark detector.
        /// </summary>
        FaceLandmarkDetector faceLandmarkDetector;

        /// <summary>
        /// The FPS monitor.
        /// </summary>
        FpsMonitor fpsMonitor;

        /// <summary>
        /// The detection result.
        /// </summary>
        List<UnityEngine.Rect> detectionResult;

        /// <summary>
        /// The haarcascade_frontalface_alt_xml_filepath.
        /// </summary>
        string haarcascade_frontalface_alt_xml_filepath;

        /// <summary>
        /// The dlib shape predictor file name.
        /// </summary>
        string dlibShapePredictorFileName = "sp_human_face_68.dat";

        /// <summary>
        /// The dlib shape predictor file path.
        /// </summary>
        string dlibShapePredictorFilePath;

        private bool isDrawFaceWireFrame;
        private bool alreadyChangeFace;

        private List<MeshFilter> faceMeshes;
        private List<Renderer> faceRenderers;
        private List<VideoPlayer> faceVideoPlayers;
        private Vector3 faceAdjustPosition;
        private float faceAdjustScale;
        private int currentFaceIndex;

        private int size;
        private float unit;
        private float lastAngle;
        private Vector2 texUnit;

        private int[] relocateArray;

        private Texture2D faceTexture;

#if UNITY_WEBGL && !UNITY_EDITOR
        IEnumerator getFilePath_Coroutine;
#endif

        private void Start()
        {
            fpsMonitor = GetComponent<FpsMonitor>();

            enableDownScaleToggle.isOn = enableDownScale;
            enableSkipFrameToggle.isOn = enableSkipFrame;
            useOpenCVFaceDetectorToggle.isOn = useOpenCVFaceDetector;

            imageOptimizationHelper = gameObject.GetComponent<ImageOptimizationHelper>();
            webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper>();

            dlibShapePredictorFileName = DlibFaceLandmarkDetectorExample.dlibShapePredictorFileName;
#if UNITY_WEBGL && !UNITY_EDITOR
            getFilePath_Coroutine = GetFilePath ();
            StartCoroutine (getFilePath_Coroutine);
#else
            haarcascade_frontalface_alt_xml_filepath = OpenCVForUnity.UnityUtils.Utils.getFilePath("haarcascade_frontalface_alt.xml");
            dlibShapePredictorFilePath = DlibFaceLandmarkDetector.UnityUtils.Utils.getFilePath(dlibShapePredictorFileName);
            Run();
#endif
            faceMeshes = new List<MeshFilter>();
            faceRenderers = new List<Renderer>();
            faceVideoPlayers = new List<VideoPlayer>();
            faceMeshes.Add(facePlane.GetComponent<MeshFilter>());
            faceRenderers.Add(facePlane.GetComponent<Renderer>());
            faceVideoPlayers.Add(facePlane.GetComponent<VideoPlayer>());
            CreateNewFace();

            faceAdjustPosition = Vector3.zero;
            faceAdjustScale = 1f;

            relocateArray = new int[73];
            relocateArray[0] = 64;
            relocateArray[1] = 63;
            relocateArray[2] = 56;
            relocateArray[3] = 54;
            relocateArray[4] = 55;
            relocateArray[5] = 69;
            relocateArray[6] = 68;
            relocateArray[7] = 67;
            relocateArray[8] = 66;
            relocateArray[9] = 65;
            relocateArray[10] = 61;
            relocateArray[11] = 62;
            relocateArray[12] = 70;
            relocateArray[13] = 72;
            relocateArray[14] = 74;
            relocateArray[15] = 75;
            relocateArray[16] = 36;
            relocateArray[17] = 2;
            relocateArray[18] = 1;
            relocateArray[19] = 5;
            relocateArray[20] = 8;
            relocateArray[21] = 10;
            relocateArray[22] = 14;
            relocateArray[23] = 16;
            relocateArray[24] = 28;
            relocateArray[25] = 30;
            relocateArray[26] = 35;
            relocateArray[27] = 13;
            relocateArray[28] = 22;
            relocateArray[29] = 21;
            relocateArray[30] = 23;
            relocateArray[31] = 20;
            relocateArray[32] = 24;
            relocateArray[33] = 25;
            relocateArray[34] = 40;
            relocateArray[35] = 41;
            relocateArray[36] = 6;
            relocateArray[37] = 19;
            relocateArray[38] = 11;
            relocateArray[39] = 12;
            relocateArray[40] = 17;
            relocateArray[41] = 18;
            relocateArray[42] = 26;
            relocateArray[43] = 27;
            relocateArray[44] = 29;
            relocateArray[45] = 31;
            relocateArray[46] = 73;
            relocateArray[47] = 76;
            relocateArray[48] = 50;
            relocateArray[49] = 53;
            relocateArray[50] = 37;
            relocateArray[51] = 38;
            relocateArray[52] = 39;
            relocateArray[53] = 71;
            relocateArray[54] = 60;
            relocateArray[55] = 59;
            relocateArray[56] = 58;
            relocateArray[57] = 46;
            relocateArray[58] = 47;
            relocateArray[59] = 48;
            relocateArray[60] = 49;
            relocateArray[61] = 42;
            relocateArray[62] = 43;
            relocateArray[63] = 51;
            relocateArray[64] = 57;
            relocateArray[65] = 52;
            relocateArray[66] = 44;
            relocateArray[67] = 45;
            relocateArray[68] = 34;
            relocateArray[69] = 32;
            relocateArray[70] = 9;
            relocateArray[71] = 4;
            relocateArray[72] = 3;

            debugText.text = maskName[currentFaceIndex];
            alreadyChangeFace = true;
            //StartCoroutine(LoadStreamingAsset("FaceMapping/FaceTest.png"));
        }

        public IEnumerator LoadStreamingAsset(string fileName)
        {
            var filePath = "file://" + System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
            
            var www = UnityWebRequestTexture.GetTexture(filePath);

            yield return www.SendWebRequest();
            
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                faceTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                for (var i = 0; i < faceRenderers.Count; i++)
                {
                    faceRenderers[i].material.mainTexture = faceTexture;
                }
            }
        }

#if UNITY_WEBGL && !UNITY_EDITOR
        private IEnumerator GetFilePath ()
        {
            var getFilePathAsync_0_Coroutine = OpenCVForUnity.UnityUtils.Utils.getFilePathAsync ("haarcascade_frontalface_alt.xml", (result) => {
                haarcascade_frontalface_alt_xml_filepath = result;
            });
            yield return getFilePathAsync_0_Coroutine;

            var getFilePathAsync_1_Coroutine = DlibFaceLandmarkDetector.UnityUtils.Utils.getFilePathAsync (dlibShapePredictorFileName, (result) => {
                dlibShapePredictorFilePath = result;
            });
            yield return getFilePathAsync_1_Coroutine;

            getFilePath_Coroutine = null;

            Run ();
        }
#endif

        private void Run()
        {
            if (string.IsNullOrEmpty(dlibShapePredictorFilePath))
            {
                Debug.LogError("shape predictor file does not exist. Please copy from “DlibFaceLandmarkDetector/StreamingAssets/” to “Assets/StreamingAssets/” folder. ");
            }

            cascade = new CascadeClassifier(haarcascade_frontalface_alt_xml_filepath);
#if !UNITY_WSA_10_0
            if (cascade.empty())
            {
                Debug.LogError("cascade file is not loaded. Please copy from “OpenCVForUnity/StreamingAssets/” to “Assets/StreamingAssets/” folder. ");
            }
#endif

            faceLandmarkDetector = new FaceLandmarkDetector(dlibShapePredictorFilePath);

#if UNITY_ANDROID && !UNITY_EDITOR
            // Avoids the front camera low light issue that occurs in only some Android devices (e.g. Google Pixel, Pixel2).
            webCamTextureToMatHelper.avoidAndroidFrontCameraLowLightIssue = true;
#endif
            webCamTextureToMatHelper.Initialize();

            texUnit = Vector2.one;//new Vector2((float)1280 / texture.width, (float)720 / texture.height);
        }

        /// <summary>
        /// Raises the webcam texture to mat helper initialized event.
        /// </summary>
        public void OnWebCamTextureToMatHelperInitialized()
        {
            Debug.Log("OnWebCamTextureToMatHelperInitialized");

            Mat webCamTextureMat = webCamTextureToMatHelper.GetMat();
            Mat downscaleMat = imageOptimizationHelper.GetDownScaleMat(webCamTextureMat);

            texture = new Texture2D(webCamTextureMat.cols(), webCamTextureMat.rows(), TextureFormat.RGBA32, false);

            gameObject.GetComponent<Renderer>().material.mainTexture = texture;

            //gameObject.transform.localScale = new Vector3(webCamTextureMat.cols(), webCamTextureMat.rows(), 1);
            transform.localScale = new Vector3(Camera.main.orthographicSize * 2.0f * Screen.width / Screen.height, Camera.main.orthographicSize * 2.0f, 0.1f);
            Debug.Log("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);

            if (fpsMonitor != null)
            {
                fpsMonitor.Add("dlib shape predictor", dlibShapePredictorFileName);
                fpsMonitor.Add("original_width", webCamTextureToMatHelper.GetWidth().ToString());
                fpsMonitor.Add("original_height", webCamTextureToMatHelper.GetHeight().ToString());
                fpsMonitor.Add("downscaleRaito", imageOptimizationHelper.downscaleRatio.ToString());
                fpsMonitor.Add("frameSkippingRatio", imageOptimizationHelper.frameSkippingRatio.ToString());
                fpsMonitor.Add("downscale_width", downscaleMat.width().ToString());
                fpsMonitor.Add("downscale_height", downscaleMat.height().ToString());
                fpsMonitor.Add("orientation", Screen.orientation.ToString());
            }

            grayMat = new Mat(webCamTextureMat.rows(), webCamTextureMat.cols(), CvType.CV_8UC1);

            detectionResult = new List<UnityEngine.Rect>();
        }

        /// <summary>
        /// Raises the web cam texture to mat helper disposed event.
        /// </summary>
        public void OnWebCamTextureToMatHelperDisposed()
        {
            Debug.Log("OnWebCamTextureToMatHelperDisposed");

            if (grayMat != null)
            {
                grayMat.Dispose();
                grayMat = null;
            }
            if (texture != null)
            {
                Texture2D.Destroy(texture);
                texture = null;
            }
        }

        /// <summary>
        /// Raises the web cam texture to mat helper error occurred event.
        /// </summary>
        /// <param name="errorCode">Error code.</param>
        public void OnWebCamTextureToMatHelperErrorOccurred(WebCamTextureToMatHelper.ErrorCode errorCode)
        {
            Debug.Log("OnWebCamTextureToMatHelperErrorOccurred " + errorCode);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                isDrawFaceWireFrame = !isDrawFaceWireFrame;
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                NextFace();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                StartCoroutine(LoadStreamingAsset(faceTestFile));
            }

            if (Input.GetKeyDown(KeyCode.H))
            {
                quadRenderer.enabled = !quadRenderer.enabled;
            }

            if (!faceVideoPlayers[0].enabled && Input.GetKeyDown(KeyCode.P))
            {
                for (var i = 0; i < faceVideoPlayers.Count; i++)
                {
                    faceVideoPlayers[i].enabled = true;
                }
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                for (var i = 0; i < faceVideoPlayers.Count; i++)
                {
                    faceVideoPlayers[i].enabled = false;
                }
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                faceAdjustPosition -= new Vector3(10f, 0, 0);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                faceAdjustPosition += new Vector3(10f, 0, 0);
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                faceAdjustPosition -= new Vector3(0, 10f, 0);
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                faceAdjustPosition += new Vector3(0, 10f, 0);
            }

            if (Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                faceAdjustScale -= 0.01f;
            }
            else if (Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                faceAdjustScale += 0.01f;
            }

            if (webCamTextureToMatHelper.IsPlaying() && webCamTextureToMatHelper.DidUpdateThisFrame())
            {
                Mat rgbaMat = webCamTextureToMatHelper.GetMat();

                Mat downScaleRgbaMat = null;
                float DOWNSCALE_RATIO = 1.0f;
                if (enableDownScale)
                {
                    downScaleRgbaMat = imageOptimizationHelper.GetDownScaleMat(rgbaMat);
                    DOWNSCALE_RATIO = imageOptimizationHelper.downscaleRatio;
                }
                else
                {
                    downScaleRgbaMat = rgbaMat;
                    DOWNSCALE_RATIO = 1.0f;
                }

                // set the downscale mat
                OpenCVForUnityUtils.SetImage(faceLandmarkDetector, downScaleRgbaMat);

                // detect faces on the downscale image
                if (!enableSkipFrame || !imageOptimizationHelper.IsCurrentFrameSkipped())
                {
                    //detect face rects
                    if (useOpenCVFaceDetector)
                    {
                        // convert image to greyscale.
                        Imgproc.cvtColor(downScaleRgbaMat, grayMat, Imgproc.COLOR_RGBA2GRAY);

                        using (Mat equalizeHistMat = new Mat())
                        using (MatOfRect faces = new MatOfRect())
                        {
                            Imgproc.equalizeHist(grayMat, equalizeHistMat);

                            cascade.detectMultiScale(equalizeHistMat, faces, 1.1f, 2, 0 | Objdetect.CASCADE_SCALE_IMAGE, new Size(equalizeHistMat.cols() * 0.15, equalizeHistMat.cols() * 0.15), new Size());

                            List<OpenCVForUnity.CoreModule.Rect> opencvDetectResult = faces.toList();

                            // correct the deviation of the detection result of the face rectangle of OpenCV and Dlib.
                            detectionResult.Clear();
                            foreach (var opencvRect in opencvDetectResult)
                            {
                                detectionResult.Add(new UnityEngine.Rect((float)opencvRect.x, (float)opencvRect.y + (float)(opencvRect.height * 0.1f), (float)opencvRect.width, (float)opencvRect.height));
                            }
                        }

                    }
                    else
                    {
                        // Dlib's face detection processing time increases in proportion to the image size.
                        detectionResult = faceLandmarkDetector.Detect();
                    }

                    if (enableDownScale)
                    {
                        for (int i = 0; i < detectionResult.Count; ++i)
                        {
                            var rect = detectionResult[i];
                            detectionResult[i] = new UnityEngine.Rect(
                                rect.x * DOWNSCALE_RATIO,
                                rect.y * DOWNSCALE_RATIO,
                                rect.width * DOWNSCALE_RATIO,
                                rect.height * DOWNSCALE_RATIO);
                        }
                    }
                }

                // set the original scale image
                OpenCVForUnityUtils.SetImage(faceLandmarkDetector, rgbaMat);

                if (detectionResult.Count == 0)
                {
                    if (!alreadyChangeFace)
                    {
                        NextFace();
                        alreadyChangeFace = true;
                    }
                }
                else
                {
                    alreadyChangeFace = false;
                }

                while (faceMeshes.Count < detectionResult.Count)
                {
                    CreateNewFace();
                }

                // detect face landmarks on the original image
                for (var j = 0; j < detectionResult.Count; j++)
                {
                    //detect landmark points
                    var points = faceLandmarkDetector.DetectLandmark(detectionResult[j]);
                    AddForeheadPoints(points);

                    if (isDrawFaceWireFrame)
                    {
                        //draw landmark points
                        OpenCVForUnityUtils.DrawFaceLandmark(rgbaMat, points, new Scalar(0, 255, 0, 255), 2);
                    }

                    //Update Vertices
                    var total = 73;
                    var meshFilter = faceMeshes[j];
                    var mesh = meshFilter.mesh;
                    var newVertices = new List<Vector3>();
                    mesh.GetVertices(newVertices);

                    if (!meshFilter.gameObject.activeInHierarchy)
                    {
                        meshFilter.gameObject.SetActive(true);
                    }
                    
                    for (var i = 0; i < total; i++)
                    {
                        var index = relocateArray[i];
                        var point = points[i];
                        var w = texture.width;// Screen.width;
                        var h = texture.height;// Screen.height;
                        point.y = (h - (point.y * texUnit.y)) - h * 0.5f;
                        point.x = point.x * texUnit.x - w * 0.5f;
                        var ray = Camera.main.ScreenPointToRay(point);
                        var targetPoint3D = point;// Camera.main.ScreenToWorldPoint(point);

                        //debugKnob.anchoredPosition = point;
                        //debugSphere.position = targetPoint3D;

                        var newPoint = facePlane.transform.InverseTransformPoint(targetPoint3D);
                        newPoint.z = 0;

                        newVertices[index] = ToNewPoint(newVertices[index], newPoint);
                    }

                    newVertices[0] = (newVertices[3] + newVertices[4]) * 0.5f;
                    newVertices[7] = (newVertices[4] + newVertices[9]) * 0.5f;
                    newVertices[15] = (newVertices[9] + newVertices[32]) * 0.5f;
                    newVertices[33] = (newVertices[34] + newVertices[32]) * 0.5f;

                    mesh.SetVertices(newVertices);
                    /*facePlane.transform.localScale *= faceAdjustScale;
                    facePlane.transform.position += faceAdjustPosition;

                    debugSphere.position = facePlane.transform.TransformPoint(newVertices[debugIndex]);
                    break;*/
                }

                if (faceMeshes.Count > detectionResult.Count)
                {
                    for (var i = detectionResult.Count; i < faceMeshes.Count; i++)
                    {
                        faceMeshes[i].gameObject.SetActive(false);
                    }
                }

                //draw video capture to quad screen
                OpenCVForUnity.UnityUtils.Utils.fastMatToTexture2D(rgbaMat, texture);
            }
        }

        /// <summary>
        /// Raises the destroy event.
        /// </summary>
        private void OnDestroy()
        {
            if (webCamTextureToMatHelper != null)
                webCamTextureToMatHelper.Dispose();

            if (imageOptimizationHelper != null)
                imageOptimizationHelper.Dispose();

            if (faceLandmarkDetector != null)
                faceLandmarkDetector.Dispose();

            if (cascade != null)
                cascade.Dispose();

#if UNITY_WEBGL && !UNITY_EDITOR
            if (getFilePath_Coroutine != null) {
                StopCoroutine (getFilePath_Coroutine);
                ((IDisposable)getFilePath_Coroutine).Dispose ();
            }
#endif
        }

        /// <summary>
        /// Raises the back button click event.
        /// </summary>
        public void OnBackButtonClick()
        {
            SceneManager.LoadScene("DlibFaceLandmarkDetectorExample");
        }

        /// <summary>
        /// Raises the play button click event.
        /// </summary>
        public void OnPlayButtonClick()
        {
            webCamTextureToMatHelper.Play();
        }

        /// <summary>
        /// Raises the pause button click event.
        /// </summary>
        public void OnPauseButtonClick()
        {
            webCamTextureToMatHelper.Pause();
        }

        /// <summary>
        /// Raises the stop button click event.
        /// </summary>
        public void OnStopButtonClick()
        {
            webCamTextureToMatHelper.Stop();
        }

        /// <summary>
        /// Raises the change camera button click event.
        /// </summary>
        public void OnChangeCameraButtonClick()
        {
            webCamTextureToMatHelper.requestedIsFrontFacing = !webCamTextureToMatHelper.IsFrontFacing();
        }

        /// <summary>
        /// Raises the enable downscale toggle value changed event.
        /// </summary>
        public void OnEnableDownScaleToggleValueChanged()
        {
            if (enableDownScaleToggle.isOn)
            {
                enableDownScale = true;
            }
            else
            {
                enableDownScale = false;
            }
        }

        /// <summary>
        /// Raises the enable skipframe toggle value changed event.
        /// </summary>
        public void OnEnableSkipFrameToggleValueChanged()
        {
            if (enableSkipFrameToggle.isOn)
            {
                enableSkipFrame = true;
            }
            else
            {
                enableSkipFrame = false;
            }
        }

        /// <summary>
        /// Raises the use OpenCV FaceDetector toggle value changed event.
        /// </summary>
        public void OnUseOpenCVFaceDetectorToggleValueChanged()
        {
            if (useOpenCVFaceDetectorToggle.isOn)
            {
                useOpenCVFaceDetector = true;
            }
            else
            {
                useOpenCVFaceDetector = false;
            }
        }

        private Vector3 ToNewPoint(Vector3 currentPoint, Vector3 newPoint)
        {
            return (newPoint - currentPoint).sqrMagnitude < 0.5f ? Vector3.Slerp(currentPoint, newPoint, 0.3f) : newPoint;
        }

        private void AddForeheadPoints(List<Vector2> points)
        {
            if (points.Count != 68)
            {
                Debug.LogErrorFormat("face mask point not 68 points!");
                return;
            }

            Vector2 noseLength = new Vector2(points[27].x - points[30].x, points[27].y - points[30].y);
            Vector2 glabellaPoint = new Vector2((points[19].x + points[24].x) / 2f, (points[19].y + points[24].y) / 2f);

            points.Add(new Vector2(points[26].x + noseLength.x * 0.8f, points[26].y + noseLength.y * 0.8f));
            points.Add(new Vector2(points[24].x + noseLength.x, points[24].y + noseLength.y));
            points.Add(new Vector2(glabellaPoint.x + noseLength.x * 1.1f, glabellaPoint.y + noseLength.y * 1.1f));
            points.Add(new Vector2(points[19].x + noseLength.x, points[19].y + noseLength.y));
            points.Add(new Vector2(points[17].x + noseLength.x * 0.8f, points[17].y + noseLength.y * 0.8f));
        }

        private void CreateNewFace()
        {
            var newFace = Instantiate(facePlane);
            faceMeshes.Add(newFace.GetComponent<MeshFilter>());
            faceRenderers.Add(newFace.GetComponent<Renderer>());
            faceVideoPlayers.Add(newFace.GetComponent<VideoPlayer>());
        }

        private void NextFace()
        {
            currentFaceIndex++;

            if (currentFaceIndex >= faceMaterials.Length)
            {
                currentFaceIndex = 0;
            }

            for (var i = 0; i < faceRenderers.Count; i++)
            {
                faceRenderers[i].material = faceMaterials[currentFaceIndex];
            }

            debugText.text = currentFaceIndex >= maskName.Length ? faceMaterials[currentFaceIndex].name : maskName[currentFaceIndex];
        }

        private float DistanceSq(Vector2 a, Vector2 b)
        {
            var x = b.x - a.x;
            var y = b.y - a.y;
            return x * x + y * y;
        }
    }
}

#endif