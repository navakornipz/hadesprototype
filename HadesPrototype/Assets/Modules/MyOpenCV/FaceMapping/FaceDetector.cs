﻿#if USING_OPENCV && USING_DLIBFACE

using System.Collections.Generic;
using DlibFaceLandmarkDetector;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ObjdetectModule;
using OpenCVForUnity.UnityUtils.Helper;
using UnityEngine;

public class FaceDetector
{
    #region Members

    private ImageOptimizationHelper imageOptimizationHelper;
    private FaceLandmarkDetector faceLandmarkDetector;
    private CascadeClassifier cascade;
    private List<List<Vector2>> facePoints;
    private string haarcascade_frontalface_alt_xml_filepath;
    private string dlibShapePredictorFileName = "sp_human_face_68.dat";
    private string dlibShapePredictorFilePath;
    private float scaleRatio;
    private int maxDetect;

    #endregion

    #region Properties

    public List<List<Vector2>> DetectionFaces
    {
        get
        {
            return facePoints;
        }
    }

    public List<Vector2> FirstFace
    {
        get
        {
            return facePoints[0];
        }
    }

    public bool FoundFace
    {
        get
        {
            return facePoints.Count > 0;
        }
    }

    public bool FindNearestPerson { get; set; }

    #endregion

    #region Command

    public void Init(int maxDetect, float scaleRatio)
    {
        this.maxDetect = maxDetect;
        this.scaleRatio = scaleRatio;

        haarcascade_frontalface_alt_xml_filepath = OpenCVForUnity.UnityUtils.Utils.getFilePath("haarcascade_frontalface_alt.xml");
        dlibShapePredictorFilePath = string.Format("{0}/{1}", Application.streamingAssetsPath, dlibShapePredictorFileName);
        cascade = new CascadeClassifier(haarcascade_frontalface_alt_xml_filepath);
        faceLandmarkDetector = new FaceLandmarkDetector(dlibShapePredictorFilePath);
        facePoints = new List<List<Vector2>>();
    }

    #endregion Command

    #region Event

    public void DetectFace(Mat orignalMat, Mat downScaleMat)
    {
        OpenCVForUnityUtils.SetImage(faceLandmarkDetector, downScaleMat);
        var detectionResult = faceLandmarkDetector.Detect();

        facePoints.Clear();

        if (detectionResult.Count < 1)
        {
            return;
        }

        if (FindNearestPerson)
        {
            if (Mathf.Abs(scaleRatio - 1f) > float.Epsilon)
            {
                var maxSize = 0f;
                var index = 0;
                for (int i = 0; i < detectionResult.Count; ++i)
                {
                    var rect = detectionResult[i];
                    var size = rect.width * rect.height;
                    
                    if (size > maxSize)
                    {
                        maxSize = size;
                        index = i;
                    }
                }

                var nearestRect = detectionResult[index];
                detectionResult.Clear();
                detectionResult.Add(nearestRect);
            }
        }

        if (Mathf.Abs(scaleRatio - 1f) > float.Epsilon)
        {
            for (int i = 0; i < maxDetect; ++i)
            {
                if (i >= detectionResult.Count)
                {
                    break;
                }

                var rect = detectionResult[i];
                detectionResult[i] = new UnityEngine.Rect(
                    rect.x * scaleRatio,
                    rect.y * scaleRatio,
                    rect.width * scaleRatio,
                    rect.height * scaleRatio);
            }
        }

        OpenCVForUnityUtils.SetImage(faceLandmarkDetector, orignalMat);

        facePoints.Clear();
        for (int i = 0; i < maxDetect; ++i)
        {
            if (i >= detectionResult.Count)
            {
                break;
            }

            var points = faceLandmarkDetector.DetectLandmark(detectionResult[i]);
            facePoints.Add(points);
        }
    }

    #endregion Event
}

#endif