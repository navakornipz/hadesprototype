﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FindFaceGeometry : MonoBehaviour
{
    [SerializeField] private MeshFilter faceMeshFilter;
    [SerializeField] private Transform pointTransform;

    [SerializeField] private Text debugText;

    private Mesh faceMesh;

    private List<Vector3> vertices;

    private int currentIndex = 0;

    private void Awake()
    {
        faceMesh = faceMeshFilter.mesh;
        vertices = new List<Vector3>();
        faceMesh.GetVertices(vertices);
        PointUpdate();
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            currentIndex++;

            if (currentIndex >= vertices.Count)
            {
                currentIndex = 0;
            }

            PointUpdate();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            currentIndex--;

            if (currentIndex < 0)
            {
                currentIndex = vertices.Count - 1;
            }

            PointUpdate();
        }
    }

    private void PointUpdate()
    {
        pointTransform.position = vertices[currentIndex];
        debugText.text = string.Format("current index: {0}", currentIndex);
    }
}
