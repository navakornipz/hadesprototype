﻿#if USING_OPENCV

using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnityExample;
using System.Collections.Generic;

public class HandDetector
{
    private const int depthRange = 2000;

    private ColorBlobDetector detector;
    private Scalar blobColorHsv;
    private Scalar CONTOUR_COLOR;
    private Scalar CONTOUR_COLOR_WHITE;
    private Size SPECTRUM_SIZE;
    private int numberOfFingers;

    public void Init()
    {
        detector = new ColorBlobDetector();
        blobColorHsv = new Scalar(255);
        SPECTRUM_SIZE = new Size(200, 64);
        CONTOUR_COLOR = new Scalar(255, 0, 0, 255);
        CONTOUR_COLOR_WHITE = new Scalar(255, 255, 255, 255);

        //detector.SetHsvColor(blobColorHsv);
        //Imgproc.resize(detector.GetSpectrum(), spectrumMat, SPECTRUM_SIZE);
    }

    public int DetectionProcess(Mat rgbaMat)
    {
        Imgproc.GaussianBlur(rgbaMat, rgbaMat, new Size(3, 3), 1, 1);

        var contours = detector.GetContours();
        detector.Process(rgbaMat);

        if (contours.Count <= 0)
        {
            return 0;
        }

        var rect = Imgproc.minAreaRect(new MatOfPoint2f(contours[0].toArray()));

        var boundWidth = rect.size.width;
        var boundHeight = rect.size.height;
        var boundPos = 0;

        for (var i = 1; i < contours.Count; i++)
        {
            rect = Imgproc.minAreaRect(new MatOfPoint2f(contours[i].toArray()));
            if (rect.size.width * rect.size.height > boundWidth * boundHeight)
            {
                boundWidth = rect.size.width;
                boundHeight = rect.size.height;
                boundPos = i;
            }
        }

        var contour = contours[boundPos];
        var boundRect = Imgproc.boundingRect(new MatOfPoint(contour.toArray()));
        Imgproc.rectangle(rgbaMat, boundRect.tl(), boundRect.br(), CONTOUR_COLOR_WHITE, 2, 8, 0);

        var a = boundRect.br().y - boundRect.tl().y;
        a = a * 0.7;
        a = boundRect.tl().y + a;

        Imgproc.rectangle(rgbaMat, boundRect.tl(), new Point(boundRect.br().x, a), CONTOUR_COLOR, 2, 8, 0);

        var pointMat = new MatOfPoint2f();
        Imgproc.approxPolyDP(new MatOfPoint2f(contour.toArray()), pointMat, 3, true);
        contour = new MatOfPoint(pointMat.toArray());

        var hull = new MatOfInt();
        var convexDefect = new MatOfInt4();
        Imgproc.convexHull(new MatOfPoint(contour.toArray()), hull);

        if (hull.toArray().Length < 3)
            return 0;

        Imgproc.convexityDefects(new MatOfPoint(contour.toArray()), hull, convexDefect);

        var hullPoints = new List<MatOfPoint>();
        var listPo = new List<Point>();
        for (var j = 0; j < hull.toList().Count; j++)
        {
            listPo.Add(contour.toList()[hull.toList()[j]]);
        }

        var e = new MatOfPoint();
        e.fromList(listPo);
        hullPoints.Add(e);

        var listPoDefect = new List<Point>();

        if (convexDefect.rows() > 0)
        {
            var convexDefectList = convexDefect.toList();
            var contourList = contour.toList();
            for (var j = 0; j < convexDefectList.Count; j = j + 4)
            {
                var farPoint = contourList[convexDefectList[j + 2]];
                var depth = convexDefectList[j + 3];
                if (depth > depthRange && farPoint.y < a)
                {
                    listPoDefect.Add(contourList[convexDefectList[j + 2]]);
                }
            }
        }

        //Imgproc.drawContours(rgbaMat, hullPoints, -1, CONTOUR_COLOR, 3);

        numberOfFingers = listPoDefect.Count;
        if (numberOfFingers > 5)
            numberOfFingers = 5;

        /*foreach (Point p in listPoDefect)
        {
            Imgproc.circle(rgbaMat, p, 6, new Scalar(255, 0, 255, 255), -1);
        }*/

        return numberOfFingers;
    }

    public void Dispose()
    {
        if (detector != null)
            detector.Dispose();
    }
}

#endif