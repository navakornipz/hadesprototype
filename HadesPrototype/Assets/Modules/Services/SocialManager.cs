﻿#if USING_GOOGLE_PLAYGAMES

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class SocialManager
{
    private ILeaderboard leaderBoard;

    private string leaderBoardId;

    public static SocialManager Instance;

    public void Init(string leaderBoardId)
    {
        this.leaderBoardId = leaderBoardId;

        PlayGamesPlatform.DebugLogEnabled = true;

        //var config = new PlayGamesClientConfiguration.Builder().Build();
        //PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)
                {
                    Debug.Log("Authenticated");
                    return;
                }

                Debug.Log("Failed to authenticate");
            });
        }
    }

    private void DidLoadLeaderboard(bool result)
    {
        Debug.LogFormat("Received {0} scores", leaderBoard.scores.Length);
        foreach (IScore score in leaderBoard.scores)
        {
            Debug.Log(score);
        }
    }

    public void LoadScore()
    {
        leaderBoard.LoadScores(result => DidLoadLeaderboard(result));
    }

    public void ReportScore()
    {
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate(ProcessAuthentication);
            return;
        }

        Social.ReportScore(Player.instance.highestScore, leaderBoardId, OnReportScoreCompleted);
    }

    private void ProcessAuthentication(bool success)
    {
        if (success)
        {
            Debug.Log("Authenticated");
            Social.ReportScore(Player.instance.highestScore, leaderBoardId, OnReportScoreCompleted);
            return;
        }

        Debug.Log("Failed to authenticate");
    }

    private void OnReportScoreCompleted(bool success)
    {
        if (success)
        {
            Debug.Log("report score successful.");
            return;
        }

        Debug.Log("report score failed.");
    }
}

#endif