﻿#if USING_UNITY_ADS

using System;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManagerExample : MonoBehaviour, IUnityAdsListener
{
    private UnityAdsControllerExample controller;
    private Action onWatchAdsCompleted;

    public static AdsManagerExample Instance;

    public bool IsReady
    {
        get
        {
            return controller.IsReady();
        }
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        controller = new UnityAdsControllerExample();
        controller.AddListener(this);
        controller.Init();
    }

    public void ShowRewardedVideo(Action onCompleted)
    {
        controller.Show();
        onWatchAdsCompleted = onCompleted;
    }

    public void ShowInterstitialAd(Action onCompleted = null)
    {
        controller.ShowInterstitialAd();
        onWatchAdsCompleted = onCompleted;
    }

    public void OnUnityAdsReady(string placementId)
    {
        Debug.Log("ads ready");
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            if (onWatchAdsCompleted != null)
            {
                onWatchAdsCompleted.Invoke();
            }
        }
        else if (showResult == ShowResult.Skipped)
        {
            // Do not reward the user for skipping the ad.
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.LogError(message);
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        Debug.LogFormat("ads start: {0}", placementId);
    }
}

#endif