﻿
public abstract class BaseAdsController
{
    protected abstract void OnInit();

    public abstract void Show();
    public abstract void ShowInterstitialAd();
    public abstract bool IsReady();

    public void Init()
    {
        OnInit();
    }
}
