﻿#if USING_UNITY_ADS

using UnityEngine.Advertisements;

public class UnityAdsControllerExample : BaseAdsController
{
#if UNITY_IOS
    private string gameId = "3986724";
#elif UNITY_ANDROID
    private string gameId = "3986725";
#endif

#if UNITY_EDITOR
    private bool testMode = false;
#else
    private bool testMode = false;
#endif

    public string PlacementId = "rewardedVideo";

    protected override void OnInit()
    {
        Advertisement.Initialize(gameId, testMode);
    }

    public override void Show()
    {
        Advertisement.Show(PlacementId);
    }

    public override void ShowInterstitialAd()
    {
        if (!Advertisement.IsReady())
        {
            return;
        }

        Advertisement.Show();
    }

    public override bool IsReady()
    {
        return Advertisement.IsReady(PlacementId);
    }

    public void AddListener(IUnityAdsListener listener)
    {
        Advertisement.AddListener(listener);
    }
}

#endif