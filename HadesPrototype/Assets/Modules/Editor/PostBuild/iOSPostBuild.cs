﻿#if UNITY_IOS || UNITY_IPHONE
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.IO;

public static class iOSPostBuild
{
    private const string bundleUrlName = "ppl.unity.dreamwalker";
    private const string schemeName = "ppl.unity.dreamwalker";

    [PostProcessBuild]
    public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            var projectPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

            var pbxProject = new PBXProject();
            pbxProject.ReadFromFile(projectPath);

            var target = pbxProject.TargetGuidByName("Unity-iPhone");

            // Disable bitcode
            pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

            // Add push notifications as a capability on the target
            pbxProject.AddBuildProperty(target, "SystemCapabilities", "{com.apple.Push = {enabled = 1;};}");

            pbxProject.WriteToFile(projectPath);
        }
    }

    [PostProcessBuild]
    public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            // background location useage key (new in iOS 8)
            // background modes
            PlistElementArray bgModes = rootDict.CreateArray("UIBackgroundModes");
            bgModes.AddString("remote-notification");

            PlistElementArray caps = null;

            if (rootDict["UIRequiredDeviceCapabilities"] != null)
            {
                caps = rootDict["UIRequiredDeviceCapabilities"].AsArray();
            }
            else
            {
                caps = rootDict.CreateArray("UIRequiredDeviceCapabilities");
            }
            caps.AddString("gamekit");

            //add app schema
            var urlTypesArray = rootDict.CreateArray("CFBundleURLTypes");
            var dict = urlTypesArray.AddDict();
            dict.SetString("CFBundleURLName", bundleUrlName);
            var schemesArray = dict.CreateArray("CFBundleURLSchemes");
            schemesArray.AddString(schemeName);

            var newKey = "NSLocationWhenInUseUsageDescription";
            rootDict.SetString(newKey, "Dream Walker will tracks your location.");

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
}
#endif