#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

public class PrefabsEditor : EditorWindow
{
    private const int targetByMesh = 0;
    private const int targetByName = 1;
    private const int targetByComponent = 2;

    private List<Object> SelectedObjects = new List<Object>();
    private Mesh srcMesh;
    private Mesh desMesh;
    private Material desMaterial;
    private string targetName;
    private string targetHierarchy;
    private string desName;
    private Component targetComponent;
    private Component addComponent;
    private Component removeComponent;
    private Vector3 newPosition;
    private Vector3 newRotation;
    private Vector3 newScale = Vector3.one;

    private int targetId = 0;
    private int isLocalTransform = 0;
    private bool isAddComponent = false;
    private bool isRemoveComponent = false;
    private bool isReplaceMesh = false;
    private bool isFixTransform = false;
    private bool isReplaceMaterial = false;
    private bool isFixHierarchy = false;
    private bool isRename = false;

    private Vector2 scrollPosition;

    [MenuItem("Tools/TheEyes/Prefabs Editor")]
    public static void ShowWindow()
    {
        GetWindow(typeof(PrefabsEditor));
    }

    private void OnGUI()
    {
        GUILayout.Label("Select Folder or Prefab", EditorStyles.boldLabel);

        var objCount = SelectedObjects.Count + 1;

        for (var i = 0; i < objCount; i++)
        {
            var obj = i == objCount - 1 ? null : SelectedObjects[i];
            obj = EditorGUILayout.ObjectField(obj, typeof(Object), true);

            if (obj != null && !SelectedObjects.Contains(obj))
            {
                if (i >= SelectedObjects.Count)
                {
                    SelectedObjects.Add(obj);
                }
                else
                {
                    SelectedObjects[i] = obj;
                }
            }
        }

        EditorGUILayout.Space();

        GUILayout.Label("Target By", EditorStyles.boldLabel);
        targetId = GUILayout.SelectionGrid(targetId, new string[] { "Mesh", "Name", "Component"}, 3, "toggle");

        switch(targetId)
        {
            case targetByMesh:
                srcMesh = EditorGUILayout.ObjectField(srcMesh, typeof(Mesh), true) as Mesh;
                break;
            case targetByName:
                targetName = GUILayout.TextField(targetName);
                break;
            case targetByComponent:
                targetComponent = EditorGUILayout.ObjectField(targetComponent, typeof(Component), true) as Component;
                break;
        }

        EditorGUILayout.Space();

        GUILayout.Label("TODO:", EditorStyles.boldLabel);

        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

        isRename = GUILayout.Toggle(isRename, "Rename Object");
        if (isRename)
        {
            desName = GUILayout.TextField(desName);
            EditorGUILayout.Space();
        }

        isAddComponent = GUILayout.Toggle(isAddComponent, "Add Component");
        if (isAddComponent)
        {
            addComponent = EditorGUILayout.ObjectField(addComponent, typeof(Component), true) as Component;
            EditorGUILayout.Space();
        }

        isRemoveComponent = GUILayout.Toggle(isRemoveComponent, "Remove Component");
        if (isRemoveComponent)
        {
            removeComponent = EditorGUILayout.ObjectField(removeComponent, typeof(Component), true) as Component;
            EditorGUILayout.Space();
        }

        isReplaceMesh = GUILayout.Toggle(isReplaceMesh, "Add/Change mesh");
        if (isReplaceMesh)
        {
            desMesh = EditorGUILayout.ObjectField(desMesh, typeof(Mesh), true) as Mesh;
            EditorGUILayout.Space();
        }
        
        isFixTransform = GUILayout.Toggle(isFixTransform, "Edit Transform");
        if (isFixTransform)
        {
            isLocalTransform = GUILayout.SelectionGrid(isLocalTransform, new string[] { "Local", "World" }, 2, "toggle");
            newPosition = EditorGUILayout.Vector3Field("Position", newPosition);
            newRotation = EditorGUILayout.Vector3Field("Rotation", newRotation);
            newScale = EditorGUILayout.Vector3Field("Scale", newScale);
            EditorGUILayout.Space();
        }
        
        isReplaceMaterial = GUILayout.Toggle(isReplaceMaterial, "Change Material");
        if (isReplaceMaterial)
        {
            desMaterial = EditorGUILayout.ObjectField(desMaterial, typeof(Material), true) as Material;
            EditorGUILayout.Space();
        }

        isFixHierarchy = GUILayout.Toggle(isFixHierarchy, "Change Hierarchy");
        if (isFixHierarchy)
        {
            targetHierarchy = GUILayout.TextField(targetHierarchy);
            EditorGUILayout.Space();
        }

        EditorGUILayout.EndScrollView();

        EditorGUILayout.Space();

        if (GUILayout.Button("Apply"))
        {
            switch (targetId)
            {
                case targetByName:
                    if (string.IsNullOrEmpty(targetName))
                    {
                        Debug.LogErrorFormat("target by name cannot be null.");
                        return;
                    }
                    break;
                case targetByComponent:
                    if (targetComponent == null)
                    {
                        Debug.LogErrorFormat("target by component cannot be null.");
                        return;
                    }
                    break;
            }
            OnApply();
        }

        EditorGUILayout.Space();

        if (GUILayout.Button("Clear"))
        {
            OnClear();
        }
    }

    private void OnApply()
    {
        foreach (var obj in SelectedObjects)
        {
            if(obj is GameObject)
            {
                EditingPrefab(obj as GameObject);
                continue;
            }

            if ((obj is DefaultAsset) == false)
            {
                continue;
            }

            var folder = AssetDatabase.GetAssetPath(obj);

            if (!string.IsNullOrEmpty(Path.GetExtension(folder)))
            {
                continue;
            }

            GoIntoFolder(folder);
        }

        AssetDatabase.SaveAssets();
        Debug.Log("Done");
    }

    private void EditingPrefab(GameObject gameObject)
    {
        var targetObjects = new List<GameObject>();

        var gobj = Instantiate(gameObject);
        gobj.name = gameObject.name;

        switch (targetId)
        {
            case targetByMesh:
                {
                    var meshFilters = gobj.GetComponentsInChildren<MeshFilter>(true);
                    foreach (var meshFilter in meshFilters)
                    {
                        if (meshFilter.sharedMesh != srcMesh)
                        {
                            continue;
                        }

                        targetObjects.Add(meshFilter.gameObject);
                    }
                    break;
                }
            case targetByName:
                {
                    var transforms = gobj.GetComponentsInChildren<Transform>(true);

                    foreach (var tf in transforms)
                    {
                        if(tf.name != targetName)
                        {
                            continue;
                        }

                        targetObjects.Add(tf.gameObject);
                    }
                    break;
                }
            case targetByComponent:
                {
                    var components = gobj.GetComponentsInChildren(targetComponent.GetType(), true);

                    foreach (var cmp in components)
                    {
                        targetObjects.Add(cmp.gameObject);
                    }
                    break;
                }
        }

        if(targetObjects.Count < 1)
        {
            Debug.LogError("Not found any target");
            DestroyImmediate(gobj);
            return;
        }

        foreach (var target in targetObjects)
        {
            //Rename Object
            if (isRename && !string.IsNullOrEmpty(desName))
            {
                target.name = desName;
            }

            //Add Component
            if (isAddComponent && addComponent != null)
            {
                if (target.GetComponent(addComponent.GetType()) == null)
                {
                    target.AddComponent(addComponent.GetType());
                }
            }

            //Remove Component
            if (isRemoveComponent && removeComponent != null)
            {
                var component = target.GetComponent(removeComponent.GetType());
                if (component != null)
                {
                    DestroyImmediate(component, true);
                }
            }

            //Change Mesh
            if (isReplaceMesh)
            {
                var meshFilter = target.GetComponent<MeshFilter>();

                if(meshFilter == null)
                {
                    meshFilter = target.AddComponent<MeshFilter>();

                    if (target.GetComponent<MeshRenderer>() == null)
                    {
                        target.AddComponent<MeshRenderer>();
                    }
                }

                meshFilter.mesh = desMesh;
            }

            //Edit Transform
            if (isFixTransform)
            {
                if (isLocalTransform == 0)
                {
                    target.transform.localScale = newScale;
                    target.transform.localPosition = newPosition;
                    target.transform.localRotation = Quaternion.Euler(newRotation);
                }
                else
                {
                    if(target.transform.parent != null)
                    {
                        target.transform.localScale = new Vector3(newScale.x / target.transform.parent.lossyScale.x, newScale.y / target.transform.parent.lossyScale.y, newScale.z / target.transform.parent.lossyScale.z);
                    }
                    else
                    {
                        target.transform.localScale = newScale;
                    }

                    target.transform.position = newPosition;
                    target.transform.rotation = Quaternion.Euler(newRotation);
                }
            }

            //Change Material
            if (isReplaceMaterial)
            {
                var renderer = target.GetComponent<Renderer>();

                if(renderer != null)
                {
                    renderer.sharedMaterial = desMaterial;
                }
            }

            //Change Hierarchy
            if (isFixHierarchy && !string.IsNullOrEmpty(targetHierarchy))
            {
                var parts = targetHierarchy.Split('/', '\\');
                var head = target.transform.parent;
                var tempList = new Dictionary<string, Transform>();
                tempList.Add(target.name, target.transform);
                while (true)
                {
                    if (head == null)
                    {
                        break;
                    }

                    tempList.Add(head.name, head);
                    if (head.name == parts[0])
                    {
                        break;
                    }
                    head = head.parent;
                }

                if (head == null)
                {
                    Debug.LogError("Change hierarchy failed: Can't find head object");

                    foreach (var pair in tempList)
                    {
                        Debug.LogErrorFormat("Key {0} Value: {1}", pair.Key, pair.Value);
                    }
                }
                else
                {
                    //Rearrange
                    for (var i = 1; i < parts.Length; i++)
                    {
                        var part = parts[i];
                        Transform tf = null;

                        if(!tempList.TryGetValue(part, out tf))
                        {
                            var newObj = new GameObject(part);
                            tf = newObj.transform;
                            tempList.Add(tf.name, tf);
                        }

                        var parentName = parts[i - 1];
                        var parent = tempList[parentName];
                        if (tf.parent != parent)
                        {
                            tf.parent = parent;
                        }
                    }

                    var last = parts[parts.Length - 1];
                    if (last != target.name)
                    {
                        target.transform.parent = tempList[last];
                    }

                    tempList.Remove(target.name);
                    //Remove
                    foreach (var pair in tempList)
                    {
                        var key = pair.Key;
                        var foundInNewHierarchy = false;

                        foreach (var part in parts)
                        {
                            if (key != part)
                            {
                                continue;
                            }

                            foundInNewHierarchy = true;
                            break;
                        }

                        if(foundInNewHierarchy)
                        {
                            continue;
                        }

                        DestroyImmediate(pair.Value.gameObject, true);
                    }
                }
            }
        }

        PrefabUtility.ReplacePrefab(gobj, PrefabUtility.GetPrefabObject(gameObject), ReplacePrefabOptions.ConnectToPrefab);
        DestroyImmediate(gobj);
    }

    private void GoIntoFolder(string folder)
    {
        var files = Directory.GetFiles(folder);

        foreach (var file in files)
        {
            var extension = Path.GetExtension(file);
            if (extension != ".prefab")
            {
                if(string.IsNullOrEmpty(extension))
                {
                    GoIntoFolder(file);
                }
                
                continue;
            }

            var gobj = AssetDatabase.LoadAssetAtPath<GameObject>(file);

            if (gobj == null)
            {
                continue;
            }

            EditingPrefab(gobj);
        }
    }

    private void OnClear()
    {
        for (var i = 0; i < SelectedObjects.Count; i++)
        {
            SelectedObjects[i] = null;
        }
        SelectedObjects.Clear();
        srcMesh = null;
        desMesh = null;
        desMaterial = null;
        targetName = string.Empty;
        targetHierarchy = string.Empty;
        targetComponent = null;
        isLocalTransform = 0;
        newPosition = Vector3.zero;
        newRotation = Vector3.zero;
        newScale = Vector3.one;
        addComponent = null;
        removeComponent = null;
    }
}

#endif