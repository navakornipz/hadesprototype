﻿using UnityEditor;
using UnityEngine;

public class UserEditor : EditorWindow
{
    [MenuItem("Tools/TheEyes/User Editor")]
    public static void ShowWindow()
    {
        GetWindow(typeof(UserEditor));
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Reset PlayerPref"))
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("Reset PlayerPref done.");
        }

        if (GUILayout.Button("Delete Product Data"))
        {
            //ProductDataController.ResetData();
            Debug.Log("Delete Save Data done.");
        }
    }
}