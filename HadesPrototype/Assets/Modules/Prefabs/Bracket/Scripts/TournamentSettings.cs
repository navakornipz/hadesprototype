﻿using UnityEngine;

[CreateAssetMenu(fileName = "TournamentSettings", menuName = "ScriptableObjects/Bracket/TournamentSettings", order = 1)]
public class TournamentSettings : ScriptableObject
{
    public string TournamentTitle;
    public Sprite BackgroundSprite;
}
