using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemieController : MonoBehaviour
{
    private const float walkSpeed = 5f;

    [SerializeField] private Transform bodyTf;
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private AttackController meleeAttack;
    [SerializeField] private AttackController rangeAttack;

    private StateManager<HeroState> primaryState;
    private StateManager<HeroState> secondaryState;
    private float animateTimer;
    private Vector3 dashDirection;

    private void Awake()
    {
        primaryState = new StateManager<HeroState>();
        primaryState.AddEnterMessage(HeroState.Idle, PIdleEnter);
        primaryState.AddProcess(HeroState.Idle, PIdleProcess);
        primaryState.AddEnterMessage(HeroState.Walk, WalkEnter);
        primaryState.AddProcess(HeroState.Walk, WalkProcess);
        primaryState.AddEnterMessage(HeroState.Dash, DashEnter);

        secondaryState = new StateManager<HeroState>();
        secondaryState.AddEnterMessage(HeroState.MeleeAttack, MeleeAttackEnter);
        secondaryState.AddEnterMessage(HeroState.RangeAttack, RangeAttackEnter);

        meleeAttack.gameObject.SetActive(false);
        rangeAttack.gameObject.SetActive(false);
    }
    
    private void Update()
    {
        primaryState.Process();
        secondaryState.Process();
    }

    private void PIdleEnter()
    {
        bodyTf.rotation = Quaternion.identity;
    }

    private void PIdleProcess()
    {
        var distance = (HeroController.Instance.transform.position - transform.position).sqrMagnitude;
        if (distance < 100)
        {
            primaryState.Change(HeroState.Walk);
        }
    }

    private void WalkEnter()
    {
        animateTimer = 0;
    }

    private void WalkProcess()
    {
        var direction = HeroController.Instance.transform.position - transform.position;
        transform.forward = direction.normalized;
        rigidbody.MovePosition(transform.position + transform.forward * walkSpeed * Time.deltaTime);
        rigidbody.velocity = Vector3.zero;

        animateTimer += Time.deltaTime;

        bodyTf.rotation = Quaternion.identity;
        var angle = Mathf.Sin(animateTimer * 20f) * 5f;
        bodyTf.Rotate(angle, 0, 0);
    }

    private void DashEnter()
    {
        StartCoroutine(DashProcess());
    }

    private IEnumerator DashProcess()
    {
        var duration = 0.25f;
        var timer = 0f;
        var dashSpeed = 50f;

        bodyTf.localRotation = Quaternion.Euler(new Vector3(30f, 0, 0));

        transform.forward = dashDirection;

        while (timer < duration)
        {
            timer += Time.deltaTime;

            rigidbody.MovePosition(transform.position + transform.forward * dashSpeed * Time.deltaTime);
            rigidbody.velocity = Vector3.zero;

            yield return null;
        }

        primaryState.Change(HeroState.Walk);
    }

    private void MeleeAttackEnter()
    {
        StartCoroutine(MeleeAttackProcess());
    }

    private IEnumerator MeleeAttackProcess()
    {
        var duration = 0.25f;
        var timer = 0f;
        var attackTf = meleeAttack.transform;
        var originPos = attackTf.localPosition;
        var angle = -3.5f;
        attackTf.RotateAround(transform.position, Vector3.up, 90f);

        meleeAttack.gameObject.SetActive(true);

        while (timer < duration)
        {
            timer += Time.deltaTime;
            attackTf.RotateAround(transform.position, Vector3.up, angle);

            yield return null;
        }

        attackTf.localPosition = originPos;
        meleeAttack.gameObject.SetActive(false);

        secondaryState.Change(HeroState.Idle);
    }

    private void RangeAttackEnter()
    {
        StartCoroutine(RangeAttackProcess());
    }

    private IEnumerator RangeAttackProcess()
    {
        var duration = 0.25f;
        var timer = 0f;
        var attackTf = rangeAttack.transform;
        var originPos = attackTf.localPosition;
        var speed = 50f;
        var direction = transform.forward;

        rangeAttack.gameObject.SetActive(true);

        while (timer < duration)
        {
            timer += Time.deltaTime;
            attackTf.transform.position += direction * speed * Time.deltaTime;

            yield return null;
        }

        attackTf.localPosition = originPos;
        rangeAttack.gameObject.SetActive(false);

        secondaryState.Change(HeroState.Idle);
    }
}
