using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Vector3 offset;
    [SerializeField] private Vector3 focusOffset;

    private void Update()
    {
        transform.position = target.position + offset;
        transform.LookAt(target.position + focusOffset);
    }
}
