﻿using System.Collections.Generic;
using UnityEngine;

public class MainController : BaseMain
{
    #region SerializeField

#if UNITY_EDITOR
    [SerializeField] private AppState startState;
#endif

    #endregion SerializeField

    #region Members

    [HideInInspector] public BasePanel CurrentPanel;
    private BaseAppDialog dialog;

    private new StateMachine<AppState> state;

    #endregion Members

    #region Properties

    public static MainController Instance;

    #endregion

    #region Unity Method

    private void Awake()
    {
        Instance = this;

        state = new StateMachine<AppState>();
        state.AddState(AppState.Home, new HomeState());
        state.AddState(AppState.PlayGround, new PlayGroundState());
    }

    private void Start()
    {
        base.StartEnter();

        StartCoroutine(TheMallService.GetBrandData(OnGetBrandsCompleted, OnGetBrandsFailed));

#if UNITY_EDITOR
        if (startState == AppState.Start)
        {
            startState = AppState.Home;
        }
        state.Change(startState);
#else
        state.Change(AppState.Home);
#endif
    }

    private void Update()
    {
        state.Process();
    }

#endregion Unity Method

#region Command

    public T CreateDialog<T>() where T : BaseDialog
    {
        return BaseDialog.Create<T>(dialogTransform);
    }

    public T CreatePanel<T>() where T : BaseDialog
    {
        return BaseDialog.Create<T>(backgroundTransform);
    }

    public override void GoToState(AppState newState)
    {
        if (state.IsInTransition)
        {
            return;
        }

        state.IsInTransition = true;
        StartFadeOut(() =>
        {
            ChangeState(newState);
        });
    }

    public override void ChangeState(AppState newState)
    {
        state.Change(newState);
    }

    #endregion Command

    #region Event

    private void OnGetBrandsCompleted(BrandData[] brands)
    {
        ServicesLog.Save("Get brands successful.");
        AppData.Brands = brands;
    }

    private void OnGetBrandsFailed(string message)
    {
        Debug.LogError(message);
        ServicesLog.Save(message);
    }

    #endregion Event

    #region IEnumerator

    #endregion IEnumerator
}
