﻿
public class HomeState : BaseState
{
    public override void OnEnter()
    {
        main.StartFadeIn(null);
        //main.CurrentPanel = main.CreatePanel<HomePanel>();
        MainController.Instance.StartLoadScene("Home");
    }

    public override void Update()
    {
        if (main.IsFading)
        {
            return;
        }

        /*if (Input.GetKeyDown(KeyCode.F1))
        {
            main.ChangeState(AppState.MainMenu);
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            main.ChangeState(AppState.PlayGround);
        }*/
    }

    public override void OnLeave()
    {
        main.CurrentPanel.Close();
        main.CurrentPanel = null;

        main.UnloadScene("Home");
    }
}
