﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public static class TheMallService
{
    private const string uriFormat = @"https://theeyesplayground.com/media/themall/sendmail.php";
    private const float timeOut = 30f;

    public static IEnumerator SendImage(byte[] imageData, string email, Action onCompleted, Action<string> onFailed)
    {
        var uri = uriFormat;// string.Format(uriFormat, @"send_image.php");
        var wwwForm = new WWWForm();
        var fileName = string.Format("{0}.png", DateTime.UtcNow.ToString("yyyyMMddHHmmss"));
        if (imageData != null)
        {
            wwwForm.AddBinaryData("image", imageData, fileName, "image/png");
        }
        wwwForm.AddField("email", email);

        var www = UnityWebRequest.Post(uri, wwwForm);
        www.SendWebRequest();

        ServicesLog.Save(string.Format("request: {0}\n send to {1}", www.uri, email));

        var timer = 0f;
        while (!www.isDone)
        {
            timer += Time.deltaTime;
            if (timer >= timeOut)
            {
                onFailed("Please, check your internet connection.");
                yield break;
            }

            yield return null;
        }

        if (www.error != null)
        {
            onFailed(www.error);
            yield break;
        }

        if (www.downloadHandler == null)
        {
            onFailed("recieved data is null");
            yield break;
        }

        var responseData = JsonUtility.FromJson<ResponseSendImage>(www.downloadHandler.text);
        //Debug.LogError(www.downloadHandler.text);
        if (responseData == null)
        {
            onFailed("response data is null");
            yield break;
        }

        if (!responseData.success)
        {
            onFailed(responseData.error);
            yield break;
        }

        onCompleted();
    }

    public static IEnumerator GetBrandData(Action<BrandData[]> onCompleted, Action<string> onFailed)
    {
        var path = Directory.GetCurrentDirectory();
        var uri = string.Format("{0}/brands.txt", path);

        var request = UnityWebRequest.Get(uri);
        request.SendWebRequest();

        ServicesLog.Save(string.Format("Request Brand Data: {0}", uri));

        var timer = 0f;
        while (!request.isDone)
        {
            timer += Time.deltaTime;
            if (timer >= timeOut)
            {
                onFailed("Time out. Please, check your internet connection.");
                yield break;
            }

            yield return null;
        }

        if (request.error != null)
        {
            onFailed(request.error);
            yield break;
        }

        if (request.downloadHandler == null)
        {
            onFailed("Response data is null.");
            yield break;
        }

        var responseData = JsonUtility.FromJson<ProductGenerator.Brands>(request.downloadHandler.text);
        //Debug.LogError(www.downloadHandler.text);
        if (responseData == null)
        {
            onFailed(string.Format("Response data serailze failed: {0}", request.downloadHandler.text));
            yield break;
        }

        /*if (!responseData.status)
        {
            onFailed(responseData.msg);
            yield break;
        }*/

        onCompleted(responseData.brands);
    }
}
