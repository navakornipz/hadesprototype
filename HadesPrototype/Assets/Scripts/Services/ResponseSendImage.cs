﻿using System;

[Serializable]
public class ResponseSendImage
{
    public bool success;
    public string image;
    public string email;
    public string message;
    public string error;
}
