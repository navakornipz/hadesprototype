﻿
public enum ShareState
{
    Start,
    Countdown,
    Capture,
    AskForRetry,
    InputEmail,
    ShareRequest,
    End
}
