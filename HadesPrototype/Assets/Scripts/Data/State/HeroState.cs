
public enum HeroState
{
    Idle,
    Walk,
    MeleeAttack,
    RangeAttack,
    Dash,
    Dead
}
