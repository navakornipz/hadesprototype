﻿using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using UnityEngine;

public static class AppData
{
    public const int Thai = 0;
    public const int English = 1;
    private const string configFileName = "ApplicationConfig.txt";

    public static IPAddress IpAddress = IPAddress.Broadcast;
    public static int Port = 17951;

    public static int SelectedLanguageIndex { get; set; }   // 0 thai, 1 english
    public static bool IsMasterClient = false;

    public static BrandData[] Brands;

    public static string CurrentLanguage
    {
        get
        {
            switch (SelectedLanguageIndex)
            {
                case 1:
                    return "English";
            }

            return "Thai";
        }
    }

    public static void LoadAppConfig()
    {
        var filePath = string.Format("./{0}", configFileName);
        if (!File.Exists(filePath))
        {
            Debug.LogErrorFormat("config file not found: {0}", filePath);
            return;
        }

        var sr = new StreamReader(filePath);
        var fileContents = sr.ReadToEnd();
        sr.Close();

        var lines = fileContents.Split('\r', '\n');
        for (var i = 0; i < lines.Length; i++)
        {
            var line = lines[i];

            if (line.Length < 1 ||
                line[0] == '#')
            {
                continue;
            }

            var parts = Regex.Matches(line, @"[\""].+?[\""]|[^,]+")
                .Cast<Match>()
                .Select(m => m.Value)
                .ToList();

            if (parts.Count < 2)
            {
                continue;
            }

            if (parts[0] == "MasterClient")
            {
                if (string.IsNullOrEmpty(parts[1]))
                {
                    continue;
                }

                IsMasterClient = parts[1].ToLower() == "true";

                continue;
            }
            else if (parts[0] == "ServerIpAddress")
            {
                if (string.IsNullOrEmpty(parts[1]))
                {
                    continue;
                }

                if (!IPAddress.TryParse(parts[1], out IpAddress))
                {
                    IpAddress = IPAddress.Broadcast;
                    ServicesLog.Save("parse ip address failed.");
                }
            }
            else if (parts[0] == "Port")
            {
                if (string.IsNullOrEmpty(parts[1]))
                {
                    continue;
                }

                if (!int.TryParse(parts[1], out Port))
                {
                    Port = 17951;
                    ServicesLog.Save("parse port failed.");
                }
            }
        }
    }
}