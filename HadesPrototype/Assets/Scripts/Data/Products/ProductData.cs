﻿using System;

[Serializable]
public class ProductData
{
    public const int Libstick = 0;
    public const int BrushOn = 1;
    public const int EyeShadow = 2;

    public string Name;
    public string DisplayName;
    public int Type;
    public string[] Colors;
}
