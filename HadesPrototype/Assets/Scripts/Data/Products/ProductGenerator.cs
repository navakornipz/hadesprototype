﻿using System.IO;
using UnityEngine;
using System;

public class ProductGenerator : MonoBehaviour
{
    private const int totalBrand = 12;

    [Serializable]
    public class Brands
    {
        public BrandData[] brands;
    }

    private void Start()
    {
        #region add Brand

        var brands = new BrandData[totalBrand];
        brands[0] = new BrandData
        {
            Name = "CharlotteTilbury",
            DisplayName = "CHARLOTTE TILBURY",
            Data = new ProductData[4]
        };

        brands[1] = new BrandData
        {
            Name = "threeCE",
            DisplayName = "3CE",
            Data = new ProductData[4]
        };

        brands[2] = new BrandData
        {
            Name = "YvesSaintLaurent",
            DisplayName = "YVES SAINT LAURENT",
            Data = new ProductData[2]
        };

        brands[3] = new BrandData
        {
            Name = "GiorgioArmani",
            DisplayName = "GIORGIO ARMANI",
            Data = new ProductData[2]
        };

        brands[4] = new BrandData
        {
            Name = "EsteeLauder",
            DisplayName = "Estee Lauder",
            Data = new ProductData[2]
        };

        brands[5] = new BrandData
        {
            Name = "Lancome",
            DisplayName = "LANCOME",
            Data = new ProductData[2]
        };

        brands[6] = new BrandData
        {
            Name = "Clinique",
            DisplayName = "CLINIQUE",
            Data = new ProductData[5]
        };

        brands[7] = new BrandData
        {
            Name = "BenefitCosmetics",
            DisplayName = "Benefit Cosmetics",
            Data = new ProductData[2]
        };

        brands[8] = new BrandData
        {
            Name = "Givenchy",
            DisplayName = "GIVENCHY",
            Data = new ProductData[2]
        };

        brands[9] = new BrandData
        {
            Name = "Guerlain",
            DisplayName = "GUERLAIN",
            Data = new ProductData[1]
        };

        brands[10] = new BrandData
        {
            Name = "Nars",
            DisplayName = "NARS",
            Data = new ProductData[2]
        };

        brands[11] = new BrandData
        {
            Name = "BobbiBrown",
            DisplayName = "Bobbi Brown",
            Data = new ProductData[2]
        };

        #endregion //add brand

        #region Add Products

        //Charlotte
        var brandIndex = 0;
        var data = brands[brandIndex].Data;
        var productIndex = 0;
        data[productIndex] = new ProductData
        {
            Name = "EyeShadow",
            DisplayName = "อายแชโดว์ Darling Easy Eye Palette 5.4 ก.",
            Type = ProductData.EyeShadow,
            Colors = new string[]
            {
                "g1",
                "g2",
                "#c9948c",
                "g3",
                "#ba7d7c",
                "#8d5c5f"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "BrushOn",
            DisplayName = "บลัชออน Cheek To Chic สี Ecstasy 8กรัม",
            Type = ProductData.BrushOn,
            Colors = new string[]
            {
                "#e7907f",
                "#fa887d"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "บลัชออน Cheek To Chic สี Ecstasy 8กรัม",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#e27076"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs2",
            DisplayName = "ลิปสติก Matte Revolution สี Walk of no shame 3.5ก.",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#993d40"
            }
        };
        //end Charlotte

        //3CE
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "ลิปสติก Blurring Liquid Lip สี Chapter Pink 5.5ก.",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#d75f6b"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs2",
            DisplayName = "ลิปสติก Blurring Liquid สี Pull Off 5.5ก.",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#cf543d"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "EyeShadow",
            DisplayName = "อายแชโดว์ Mini Multi Color สี Brown Rum 3.2ก.",
            Type = ProductData.EyeShadow,
            Colors = new string[]
            {
                "#fdc69f",
                "#d1936a",
                "#c36541",
                "#99573d"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "BrushOn",
            DisplayName = "บลัชออน Take A Layer สี  cabbage rose 4.2ก.",
            Type = ProductData.BrushOn,
            Colors = new string[]
            {
                "#c35256"
            }
        };
        //End 3CE

        //Yves
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "ลิปสติก Blurring Liquid Lip สี Chapter Pink 5.5ก.",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#af1e07"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs2",
            DisplayName = "ลิปสติก TATOUAGE COUTURE VELVET CREAM LIPSTICK  สี 204 Beige Underground",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#e86970"
            }
        };
        //End Yves

        //Armani
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "ลิปสติก LIP MAESTRO สี 500",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#d96c76"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs2",
            DisplayName = "ลิปสติก LIP MAESTRO สี 405",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#8f190f"
            }
        };
        //End Armani

        //Estee
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "BrushOn",
            DisplayName = "บลัชออน Pure Color Envy Sculpting Blush สี Pink Kiss",
            Type = ProductData.BrushOn,
            Colors = new string[]
            {
                "#e77e82"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "ลิปสติก Pure Color Envy Matte Sculpting Lipstick สีPersuasive 333",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#bd4b43"
            }
        };
        //End Estee

        //Lancome
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "ลิปสติกL'Absolu Rouge Intimatte 274 Killing Me Softly",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#e97f7f"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs2",
            DisplayName = "ลิปสติกL'Absolu Rouge Intimatte 888 Kind of Sexy",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#881c22"
            }
        };
        //End Lancome

        //Clinique
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "EyeShadow",
            DisplayName = "อายแชโดว์ L'Absolu Rouge Intimatte สี Fuller Fudge",
            Type = ProductData.EyeShadow,
            Colors = new string[]
            {
                "g1"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "BrushOn1",
            DisplayName = "บลัชออนCheek Pop™ 3.5ก. สี Cola Pop",
            Type = ProductData.BrushOn,
            Colors = new string[]
            {
                "#9e3127"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "BrushOn2",
            DisplayName = "บลัชออนCheek Pop™ 3.5ก. สี Poppy Pop",
            Type = ProductData.BrushOn,
            Colors = new string[]
            {
                "#d3644f"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "ลิปสติกPop™ Lip Colour + Primer 3.9ก. สี Poppy Pop",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#f53f3b"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs2",
            DisplayName = "ลิปสติกPop™ Lip Colour + Primer 3.9ก. สี Sweet Pop",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#fd87a7"
            }
        };
        //End Clinique

        //Benefit Cosmetics
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "BrushOn",
            DisplayName = "บลัชออน Dandelion Powder Blush 7ก.",
            Type = ProductData.BrushOn,
            Colors = new string[]
            {
                "#efc1c1"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "ลิป California Kissin Colorbalm Ruby 22",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#d21b31"
            }
        };
        //End Benefit Cosmetics

        //Givenchy
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "ลิปสติก Le Rouge Deep Velvet สี N12 3.4 ก.",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#c15161"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs2",
            DisplayName = "ลิปสติก Le Rouge Deep Velvet สีN36 3.4 ก.",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#c52831"
            }
        };
        //End Givenchy

        //Guerlain
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "ลิปสติก Rouge G De Guerlain The Lipstick Shade Exceptional Formula สี No.588 Sheer Shine",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#d26b62"
            }
        };
        //End Guerlain

        //Nars
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "Libs1",
            DisplayName = "ลิปกลอส SP21 Euphora Lip Shine สี Egoiste 5.5 มล.",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#e58578"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "Libs2",
            DisplayName = "ลิปกลอส SP21 Euphora Lip Shine สี Dark Deeds 5.5 มล.",
            Type = ProductData.Libstick,
            Colors = new string[]
            {
                "#970b2c"
            }
        };
        //End Nars

        //Bobbi Brown
        brandIndex++;
        productIndex = 0;
        data = brands[brandIndex].Data;
        data[productIndex] = new ProductData
        {
            Name = "BrushOn1",
            DisplayName = "บลัชออน Brightening Brick สี Coral 6.6ก.",
            Type = ProductData.BrushOn,
            Colors = new string[]
            {
                "g1",
                "g2",
                "g3",
                "g4",
                "g5",
                "g6"
            }
        };

        productIndex++;
        data[productIndex] = new ProductData
        {
            Name = "BrushOn2",
            DisplayName = "บลัชออน Brightening Brick สี Pink 6.6ก.",
            Type = ProductData.BrushOn,
            Colors = new string[]
            {
                "g1",
                "g2",
                "g3",
                "g4",
                "g5",
                "g6"
            }
        };
        //End Bobbi Brown

        #endregion //Add Products

        var newBrands = new Brands { brands = brands };
        var json = JsonUtility.ToJson(newBrands);
        Debug.LogError(json);
        var filePath = string.Format("./brands.txt");

        if (!File.Exists(filePath))
        {
            File.Delete(filePath);
        }

        File.WriteAllText(filePath, json);

        Debug.LogError("Generate file done.");
    }
}
