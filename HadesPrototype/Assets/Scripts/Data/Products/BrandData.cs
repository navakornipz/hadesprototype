﻿using System;

[Serializable]
public class BrandData
{
    public string Name;
    public string DisplayName;
    public ProductData[] Data;
}
