﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SharePanel : MonoBehaviour
{
    [SerializeField] private Text countdownText;
    [SerializeField] private Text messageText;
    [SerializeField] private GameObject askForRetryPopup;
    [SerializeField] private Image captureImage;
    [SerializeField] private GameObject inputEmailPanel;
    [SerializeField] private GameObject sendImagePopup;
    [SerializeField] private GameObject resendButton;
    [SerializeField] private GameObject closeButton;

    private StateManager<ShareState> state;
    private Texture2D captureTexture;
    private string currentEmail;

    private void Awake()
    {
        state = new StateManager<ShareState>();
        state.AddProcess(ShareState.Start, StartProcess);
        state.AddEnterMessage(ShareState.Countdown, CountdownEnter);
        state.AddEnterMessage(ShareState.AskForRetry, AskForRetryEnter);
        state.AddLeaveMessage(ShareState.AskForRetry, AskForRetryLeave);
        state.AddEnterMessage(ShareState.InputEmail, InputEmailEnter);
        state.AddLeaveMessage(ShareState.InputEmail, InputEmailLeave);
        state.AddEnterMessage(ShareState.ShareRequest, ShareRequestEnter);
        state.AddLeaveMessage(ShareState.ShareRequest, ShareRequestLeave);
        state.AddEnterMessage(ShareState.End, EndEnter);

        askForRetryPopup.SetActive(false);
        inputEmailPanel.SetActive(false);
        resendButton.SetActive(false);
        closeButton.SetActive(false);

        countdownText.text = string.Empty;
    }

    private void OnEnable()
    {
        askForRetryPopup.SetActive(false);
        inputEmailPanel.SetActive(false);
        sendImagePopup.SetActive(false);
        resendButton.SetActive(false);
        closeButton.SetActive(false);

        messageText.text = string.Empty;

        state.Change(ShareState.Start);
    }

    private void Update()
    {
        state.Process();
    }

    public void Send(string email)
    {
        currentEmail = email;
        state.Change(ShareState.ShareRequest);
    }

    public void OnRetry()
    {
        state.Change(ShareState.Start);
    }

    public void OnShare()
    {
        state.Change(ShareState.InputEmail);
    }

    public void OnCancel()
    {
        state.Change(ShareState.End);
    }

    public void OnResend()
    {
        state.Change(ShareState.ShareRequest);
    }

    public void OnCloseResponse()
    {
        state.Change(ShareState.End);
    }

    private void OnSendImageCompleted()
    {
        closeButton.SetActive(true);
        var message = AppData.SelectedLanguageIndex == AppData.Thai ? "ส่งสำเร็จแล้ว" : "send image completed.";
        messageText.text = message;
        ServicesLog.Save(message);
    }

    private void OnSendImageFailed(string error)
    {
        resendButton.SetActive(true);
        closeButton.SetActive(true);
        var message = AppData.SelectedLanguageIndex == AppData.Thai ? "ส่งล้มเหลว" : "send image failed.";
        message = string.Format("{0}\n{1}", message, error);
        messageText.text = message;
        ServicesLog.Save(message);
    }

    private void StartProcess()
    {
        countdownText.text = "3";
        state.Change(ShareState.Countdown);
    }

    private void CountdownEnter()
    {
        StartCoroutine(CountdownProcess());
    }

    private void AskForRetryEnter()
    {
        askForRetryPopup.SetActive(true);
        captureImage.sprite = Sprite.Create(captureTexture, new Rect(0, 0, captureTexture.width, captureTexture.height), new Vector2(0.5f, 0.5f));
    }

    private void AskForRetryLeave()
    {
        askForRetryPopup.SetActive(false);
    }

    private void InputEmailEnter()
    {
        inputEmailPanel.SetActive(true);
    }

    private void InputEmailLeave()
    {
        inputEmailPanel.SetActive(false);
    }

    private void ShareRequestEnter()
    {
        sendImagePopup.SetActive(true);

        messageText.text = AppData.SelectedLanguageIndex == AppData.Thai ? "กำลังส่ง.." : "Sending..";

        if (string.IsNullOrEmpty(currentEmail))
        {
            OnSendImageFailed("email address is empty.");
            return;
        }

        if (captureTexture == null)
        {
            OnSendImageFailed("captureTexture is empty.");
            return;
        }

        var imageData = captureTexture.EncodeToPNG();
        StartCoroutine(TheMallService.SendImage(imageData, currentEmail, OnSendImageCompleted, OnSendImageFailed));
    }

    private void ShareRequestLeave()
    {
        messageText.text = string.Empty;
        sendImagePopup.SetActive(false);
    }

    private void EndEnter()
    {
        gameObject.SetActive(false);
    }

    private IEnumerator CountdownProcess()
    {
        var timer = 3f;

        while (timer > 0)
        {
            yield return null;

            timer -= Time.deltaTime;
            var number = (int)timer + 1;
            countdownText.text = number.ToString();
        }

        countdownText.text = string.Empty;

        yield return null;
        yield return new WaitForEndOfFrame();

        if (captureTexture)
        {
            Destroy(captureTexture);
        }

        captureTexture = ScreenCapture.CaptureScreenshotAsTexture();
        var pixels = captureTexture.GetPixels();
        for (var i = 0; i < pixels.Length; i++)
        {
            pixels[i].a = 1f;
        }
        captureTexture.SetPixels(pixels);
        captureTexture.Apply();
        state.Change(ShareState.AskForRetry);
    }
}
