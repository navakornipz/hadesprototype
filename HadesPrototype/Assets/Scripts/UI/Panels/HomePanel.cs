﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HomePanel : BasePanel
{
    private const float switchTime = 10f;

    [SerializeField] private GameObject[] pages;
    [SerializeField] private GameObject instructionPage;
    [SerializeField] private GameObject nextButton;

    private float switchTimer;
    private int currentPage;
    private bool isSwitching;

    public static HomePanel Instance;

    protected override void OnAwake()
    {
        Instance = this;

        currentPage = 0;
        switchTimer = 0;
        isSwitching = false;

        for (var i = 1; i < pages.Length; i++)
        {
            pages[i].SetActive(false);
        }
        instructionPage.SetActive(false);

        base.OnAwake();
    }

    protected override void IdleProcess()
    {
        if (isSwitching)
        {
            return;
        }

        if (switchTimer < switchTime)
        {
            switchTimer += Time.deltaTime;
            return;
        }

        switchTimer -= switchTime;

        StartCoroutine(SwitchProcess());
    }

    public void OnStart()
    {
        MainController.Instance.GoToState(AppState.PlayGround);
    }

    public void OnNext()
    {
        for (var i = 0; i < pages.Length; i++)
        {
            pages[i].SetActive(false);
        }

        instructionPage.SetActive(true);
        nextButton.SetActive(false);
        isSwitching = true;
    }

    public void OnLanguageChanged(int index)
    {
        AppData.SelectedLanguageIndex = index;
    }

    private IEnumerator SwitchProcess()
    {
        isSwitching = true;

        var duration = 0.5f;
        var speed = 1f / duration;
        var color1 = Color.white;
        var color2 = Color.white;
        color2.a = 0;
        var alpha = 1f;
        var timer = 0f;
        var page1 = pages[currentPage];
        var pages1 = page1.GetComponentsInChildren<Image>();

        currentPage++;
        if (currentPage >= pages.Length)
        {
            currentPage = 0;
        }

        var page2 = pages[currentPage];
        var pages2 = page2.GetComponentsInChildren<Image>();
        page2.gameObject.SetActive(true);

        for (var i = 0; i < pages2.Length; i++)
        {
            pages2[i].color = color2;
        }

        while (timer < duration)
        {
            yield return null;

            timer += Time.deltaTime;
            alpha -= speed * Time.deltaTime;
            color1.a = alpha;
            color2.a = 1 - alpha;

            for (var i = 0; i < pages1.Length; i++)
            {
                pages1[i].color = color1;
            }

            for (var i = 0; i < pages2.Length; i++)
            {
                pages2[i].color = color2;
            }
        }

        page1.gameObject.SetActive(false);

        for (var i = 0; i < pages2.Length; i++)
        {
            pages2[i].color = Color.white;
        }

        isSwitching = false;
    }
}
