﻿using UnityEngine;
using UnityEngine.UI;

public class InputEmailPanel : MonoBehaviour
{
    [SerializeField] private InputField inputField;
    [SerializeField] private SharePanel sharePanel;

    private void OnEnable()
    {
        inputField.text = string.Empty;
    }

    private void Update()
    {
        var inputString = InputManager.Instance.InputString;

        if (inputString.Count < 1)
        {
            return;
        }

        while (inputString.Count > 0)
        {
            var newChar = inputString.Dequeue();

            inputField.text = string.Format("{0}{1}", inputField.text, newChar);
        }
    }

    public void OnClear()
    {
        inputField.text = string.Empty;
    }

    public void OnSend()
    {
        sharePanel.Send(inputField.text);
    }
}
