﻿
public class BaseAppDialog : BaseDialog
{
    protected override void OnClose()
    {
        base.OnClose();
        Destroy(gameObject);
    }
}
